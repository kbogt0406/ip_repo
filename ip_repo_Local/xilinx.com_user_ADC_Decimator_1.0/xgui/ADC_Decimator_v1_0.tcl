# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"

}

proc update_PARAM_VALUE.top_lvl_Deci_factor_width { PARAM_VALUE.top_lvl_Deci_factor_width } {
	# Procedure called to update top_lvl_Deci_factor_width when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.top_lvl_Deci_factor_width { PARAM_VALUE.top_lvl_Deci_factor_width } {
	# Procedure called to validate top_lvl_Deci_factor_width
	return true
}

proc update_PARAM_VALUE.top_lvl_Din_width { PARAM_VALUE.top_lvl_Din_width } {
	# Procedure called to update top_lvl_Din_width when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.top_lvl_Din_width { PARAM_VALUE.top_lvl_Din_width } {
	# Procedure called to validate top_lvl_Din_width
	return true
}

proc update_PARAM_VALUE.top_lvl_Dout_width { PARAM_VALUE.top_lvl_Dout_width } {
	# Procedure called to update top_lvl_Dout_width when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.top_lvl_Dout_width { PARAM_VALUE.top_lvl_Dout_width } {
	# Procedure called to validate top_lvl_Dout_width
	return true
}


proc update_MODELPARAM_VALUE.top_lvl_Din_width { MODELPARAM_VALUE.top_lvl_Din_width PARAM_VALUE.top_lvl_Din_width } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.top_lvl_Din_width}] ${MODELPARAM_VALUE.top_lvl_Din_width}
}

proc update_MODELPARAM_VALUE.top_lvl_Dout_width { MODELPARAM_VALUE.top_lvl_Dout_width PARAM_VALUE.top_lvl_Dout_width } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.top_lvl_Dout_width}] ${MODELPARAM_VALUE.top_lvl_Dout_width}
}

proc update_MODELPARAM_VALUE.top_lvl_Deci_factor_width { MODELPARAM_VALUE.top_lvl_Deci_factor_width PARAM_VALUE.top_lvl_Deci_factor_width } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.top_lvl_Deci_factor_width}] ${MODELPARAM_VALUE.top_lvl_Deci_factor_width}
}

