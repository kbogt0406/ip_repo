library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use IEEE.STD_LOGIC_unsigned.ALL;


entity Osc_trig is
    Generic(data_width : NATURAL := 16;             -- NATURAL or integer ???
            fifo_depth : NATURAL := 1024);          -- can generic parameters ues inside of the architecture?????
    Port ( clk : in STD_LOGIC;
           resetn : in STD_LOGIC;
           ack_arm : in STD_LOGIC;
           Din : in STD_LOGIC_VECTOR (data_width - 1 downto 0);
           din_valid : in STD_LOGIC;
           osc_fifo_we : out STD_LOGIC;
           osc_fifo_re : out STD_LOGIC;
           osc_fifo_rstn : out STD_LOGIC;
           com_fifo_rstn : out STD_LOGIC;
           com_fifo_we : out STD_LOGIC;
           trig_fpga : out STD_LOGIC;
           Dout : out STD_LOGIC_VECTOR (data_width - 1 downto 0);
           trig_edge : in STD_LOGIC;            -- from a register
           trig_auto : in STD_LOGIC;            -- to dump about 10 traces per second without trigger
           trig_level : in STD_LOGIC_VECTOR (data_width - 1 downto 0); -- from a register
           trig_delay : in unsigned (data_width - 1 downto 0);-- from a register -- since counters are unsigned
           
           -- debuging signals for state
           sig_stRST : out std_logic; 
           sig_stFIFOfilling : out std_logic; 
           sig_stTRIG1 : out std_logic; 
           sig_stTRIG2 : out std_logic; 
           sig_stDELAY : out std_logic; 
           sig_stDUMP : out std_logic; 
           sig_stINFORMandWAITack : out std_logic; 
           sig_stWAITendofACK : out std_logic;
           sig_cnt_init : out unsigned(data_width-1 downto 0);
           sig_cnt_auto : out unsigned(data_width-1 downto 0);
           sig_cnt_delay : out unsigned(data_width-1 downto 0);
           sig_cnt_dump : out unsigned(data_width-1 downto 0)
           );
end Osc_trig;

architecture Behavioral of Osc_trig is

    type OscStates is (stRST, stFIFOfilling, stTRIG1, stTRIG2, stDELAY, stDUMP, stINFORMandWAITack, stWAITendofACK) ;
    signal state, next_state : OscStates;
--    signal sig_stRST, sig_stFIFOfilling, sig_stTRIG1, sig_stTRIG2, sig_stDELAY, sig_stDUMP, sig_stINFORMandWAITack, sig_stWAITendofACK : std_logic;
    signal cnt_init, cnt_auto, cnt_delay, cnt_dump : unsigned(data_width-1 downto 0);           -- NATURAL or unsigned or std_logic_vector ?????
    signal cnt_en, cnt_clr : std_logic;
    constant auto_period : unsigned(data_width-1 downto 0):= x"03E8";          --redused for simulation (original value 50000000)
    
begin

    sig_cnt_init  <= cnt_init; 
    sig_cnt_auto  <= cnt_auto; 
    sig_cnt_delay <= cnt_delay; 
    sig_cnt_dump  <= cnt_dump;

SYNC_PROC: process (clk, resetn, din_valid)
   begin
      if (rising_edge(clk) and (din_valid = '1')) then
         if (resetn = '0') then
            state <= stRST;
         else
            state <= next_state;
         end if;
      end if;
   end process;


NEXT_STATE_DECODE: process(clk, resetn, state, din_valid, trig_edge, trig_level, ack_arm, cnt_init)
    begin

        case (state) is 
            when stRST      =>              -- newly added
                next_state <= stFIFOfilling;
                cnt_init <= (others => '0');              -- previously inside the SYNC_PROC
                cnt_auto <= (others => '0');              -- previously inside the SYNC_PROC
                cnt_delay <= (others => '0');             -- previously inside the SYNC_PROC
                cnt_dump <= (others => '0');              -- previously inside the SYNC_PROC
                
            when stFIFOfilling     =>                  -- fill the fifo
            if (rising_edge(clk) and (din_valid = '1')) then
                cnt_init <= cnt_init + 1;
                    if(cnt_init >= fifo_depth) then
                        next_state <= stTRIG1;
                        cnt_init <= (others => '0');     -- in case we come back
                    else
                        next_state <= stFIFOfilling;
                    end if; 
            end if; 
            
            when stTRIG1    =>
            if (rising_edge(clk) and (din_valid = '1')) then
                if (cnt_auto <= auto_period) then
                    cnt_auto <= cnt_auto + 1;
                    next_state <= stTRIG1;
                else
                    cnt_auto <= (others => '0');
                    next_state <= stDELAY;              -- skip the trig2 state
                end if;
            
                if(trig_edge = '1') then        --1st condition of the positive edge
                    if(din < trig_level ) then
                        next_state <= stTRIG2;
                    else
                        next_state <= stTRIG1;
                    end if;
                else                            --1st condition of the negative edge
                    if(din > trig_level) then
                        next_state <= stTRIG2;
                    else
                        next_state <= stTRIG1;
                    end if;
                end if;
            end if;
            
            when stTRIG2    =>
            if (rising_edge(clk) and (din_valid = '1')) then
                if (cnt_auto <= auto_period) then
                    cnt_auto <= cnt_auto + 1;
                    next_state <= stTRIG2;
                else
                    cnt_auto <= (others => '0');
                    next_state <= stDELAY;              -- 
                end if;
                
                if(trig_edge = '1') then        --2nd condition of the positive edge
                    if(din > trig_level or ((cnt_auto = 0) and (trig_auto='1'))) then
                        next_state <= stDELAY;
                    else
                        next_state <= stTRIG2;
                    end if;
                else                            --2nd condition of the negative edge
                    if(din < trig_level or ((cnt_auto = 0) and (trig_auto='1'))) then
                        next_state <= stDELAY;
                    else
                        next_state <= stTRIG2;
                    end if;
                end if;
            end if;
            
            when stDELAY    =>                  -- trigger delay
            if (rising_edge(clk) and (din_valid = '1')) then
                if (cnt_delay < trig_delay) then
                    cnt_delay <= cnt_delay + 1;
                    next_state <= stDELAY;
                else
                    next_state <= stDUMP;
                    cnt_delay <= (others => '0');
                end if;
            end if;
            when stDUMP     =>                  -- writing to communication FIFO
            if (rising_edge(clk) and (din_valid = '1')) then
                if(cnt_dump <= fifo_depth) then
                    cnt_dump <= cnt_dump + 1;
                    next_state <= stDUMP;
                else
                    next_state <= stINFORMandWAITack;
                    cnt_dump <= (others => '0');
                end if;
            end if;
            
            when stINFORMandWAITack   =>                  -- infrom to the ARM(PS)/ wait ack
                if( ack_arm = '1') then
                    next_state <= stWAITendofACK;
                else
                    next_state <= stINFORMandWAITack;
                end if;
            when stWAITendofACK      =>
                if( ack_arm = '0') then 
                    next_state <= stTRIG1;
                else
                    next_state <= stWAITendofACK;
                end if;
            when others     =>
                next_state <= stRST;
        end case;
    end process;
    
OUTPUT_DECODE: process(state)
    begin  
        if(state = stRST) then             -- fill the osc fifo
            osc_fifo_we <= '0';
            osc_fifo_re <= '0';
            com_fifo_we <= '0';
            osc_fifo_rstn <= '0';
            com_fifo_rstn <= '0';
            
            sig_stRST <= '1';           -- for debuging
        else
            osc_fifo_rstn <= '1';
            com_fifo_rstn <= '1';
            
            sig_stRST <= '0';           -- for debuging
        end if;
     
        if(state = stFIFOfilling) then             -- fill the osc fifo
            osc_fifo_we <= '1';
            osc_fifo_re <= '0';
            com_fifo_we <= '0';
            
            sig_stFIFOfilling <= '1';       -- for debuging
        else
            osc_fifo_we <= din_valid;
            osc_fifo_re <= din_valid;         -- enable the flow
            
            sig_stFIFOfilling <= '0';       -- for debuging
        end if;

        if(state = stTRIG1) then
            osc_fifo_we <= din_valid;
            osc_fifo_re <= din_valid;         -- enable the flow
            com_fifo_we <= '0';
            
            sig_stTRIG1 <= '1';         -- for debuging
        else
            sig_stTRIG1 <= '0';         -- for debuging
        end if;
        
        if(state = stTRIG2) then
            osc_fifo_we <= din_valid;
            osc_fifo_re <= din_valid;         -- enable the flow
            com_fifo_we <= '0';
            
            sig_stTRIG2 <= '1';         -- for debuging
        else
            sig_stTRIG2 <= '0';         -- for debuging
        end if;
        
       if(state = stDELAY) then
            osc_fifo_we <= din_valid;
            osc_fifo_re <= din_valid;         -- enable the flow
            com_fifo_we <= '0';
            
            sig_stDELAY <= '1';         -- for debuging
        else
            sig_stDELAY <= '0';         -- for debuging
        end if;
              
        if(state = stDUMP) then     -- dump data in the fifo to communication fifo in every clk cycle 
           osc_fifo_we <= din_valid;
           osc_fifo_re <= din_valid;         -- enable the flow
           com_fifo_we <= din_valid;
           
           sig_stDUMP <= '1';       -- for debuging
        else
           com_fifo_we <= '0'; 
           
           sig_stDUMP <= '0';       -- for debuging
        end if;
        
        if(state = stINFORMandWAITack) then
            trig_fpga <= '1';
            
            sig_stINFORMandWAITack <= '1';      -- for debuging
        else
            trig_fpga <= '0';
            
            sig_stINFORMandWAITack <= '0';      -- for debuging
        end if;
        
        if (state = stWAITendofACK) then
            sig_stWAITendofACK <= '1';          -- for debuging
        else
            sig_stWAITendofACK <= '0';          -- for debuging
        end if;
    end process;  
end Behavioral;
