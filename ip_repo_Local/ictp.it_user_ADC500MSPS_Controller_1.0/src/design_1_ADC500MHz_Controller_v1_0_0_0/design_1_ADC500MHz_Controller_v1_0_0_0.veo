// (c) Copyright 1995-2018 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// 
// DO NOT MODIFY THIS FILE.

// IP VLNV: ictp.it:user:ADC500MHz_Controller_v1_0:1.0
// IP Revision: 2

// The following must be inserted into your Verilog file for this
// core to be instantiated. Change the instance name and port connections
// (in parentheses) to your own signal names.

//----------- Begin Cut here for INSTANTIATION Template ---// INST_TAG
design_1_ADC500MHz_Controller_v1_0_0_0 your_instance_name (
  .ADC_clk(ADC_clk),                                            // input wire ADC_clk
  .ADC_reset(ADC_reset),                                        // input wire ADC_reset
  .ADC_Din(ADC_Din),                                            // input wire [15 : 0] ADC_Din
  .ADC_Dout(ADC_Dout),                                          // output wire [31 : 0] ADC_Dout
  .data_valid(data_valid),                                      // output wire data_valid
  .Ctrl_reg0_in(Ctrl_reg0_in),                                  // input wire [8 : 0] Ctrl_reg0_in
  .Ctrl_reg1_out(Ctrl_reg1_out),                                // output wire [1 : 0] Ctrl_reg1_out
  .to_adc_cal_fmc(to_adc_cal_fmc),                              // output wire to_adc_cal_fmc
  .to_adc_caldly_nscs_fmc(to_adc_caldly_nscs_fmc),              // output wire to_adc_caldly_nscs_fmc
  .to_adc_fsr_ece_fmc(to_adc_fsr_ece_fmc),                      // output wire to_adc_fsr_ece_fmc
  .to_adc_outv_slck_fmc(to_adc_outv_slck_fmc),                  // output wire to_adc_outv_slck_fmc
  .to_adc_outedge_ddr_sdata_fmc(to_adc_outedge_ddr_sdata_fmc),  // output wire to_adc_outedge_ddr_sdata_fmc
  .to_adc_dclk_rst_fmc(to_adc_dclk_rst_fmc),                    // output wire to_adc_dclk_rst_fmc
  .to_adc_pd_fmc(to_adc_pd_fmc),                                // output wire to_adc_pd_fmc
  .to_adc_led_0(to_adc_led_0),                                  // output wire to_adc_led_0
  .to_adc_led_1(to_adc_led_1),                                  // output wire to_adc_led_1
  .from_adc_calrun_fmc(from_adc_calrun_fmc),                    // input wire from_adc_calrun_fmc
  .from_adc_or(from_adc_or)                                    // input wire from_adc_or
);
// INST_TAG_END ------ End INSTANTIATION Template ---------

