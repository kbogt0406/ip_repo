--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   22:49:59 10/20/2017
-- Design Name:   
-- Module Name:   F:/Pvt/PhD/Tests/ISE_Projects/macro_mod/top_macromodule_with_hist_tb.vhd
-- Project Name:  macro_mod
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: top_macromodule
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY top_macromodule_with_hist_tb IS
END top_macromodule_with_hist_tb;
 
ARCHITECTURE behavior OF top_macromodule_with_hist_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT top_macromodule
    PORT(
         fifo_wr_en_mux_out : OUT  std_logic;
         clk : IN  std_logic;
         reset : IN  std_logic;
         ADC_data : IN  std_logic_vector(11 downto 0);
         param_peak_time : IN  std_logic_vector(15 downto 0);
         param_pulse_time : IN  std_logic_vector(15 downto 0);
         param_threshold_high : IN  std_logic_vector(15 downto 0);
         param_threshold_low : IN  std_logic_vector(15 downto 0);
         param_sirio_reset_threshold : IN  std_logic_vector(31 downto 0);
         param_sirio_reset_time : IN  std_logic_vector(31 downto 0);
         param_sirio_restoration_time : IN  std_logic_vector(31 downto 0);
         operation_mode : IN  std_logic_vector(2 downto 0);
         dma_transfer_size : IN  std_logic_vector(31 downto 0);
         reg_total_photon_counter : OUT  std_logic_vector(31 downto 0);
         reg_dead_time_main_FIR : OUT  std_logic_vector(31 downto 0);
         reg_sirio_resets_counter : OUT  std_logic_vector(31 downto 0);
         FIR_data_top : OUT  std_logic_vector(15 downto 0);
         FIR_Deriv_data_top : OUT  std_logic_vector(15 downto 0);
         Sirio_reset : OUT  std_logic;
--         endofrun : IN  std_logic;
         FIFO_full : OUT  std_logic;
         FIFO_empty : OUT  std_logic;
         fifo_out_tvalid : OUT  std_logic;
         fifo_out_tready : IN  std_logic;
         fifo_out_tdata : OUT  std_logic_vector(15 downto 0);
         fifo_out_tlast : OUT  std_logic;
         cf_1 : IN  std_logic_vector(4 downto 0);
         cf_2 : IN  std_logic_vector(4 downto 0);
         cf_3 : IN  std_logic_vector(4 downto 0);
         acq_period : IN  std_logic_vector(31 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal reset : std_logic := '0';             -- active low
   
   signal ADC_data : std_logic_vector(11 downto 0) := (others => '0');
   
   signal param_peak_time : std_logic_vector(15 downto 0) := x"002E";       -- PeakTime 	 = 46
   signal param_pulse_time : std_logic_vector(15 downto 0) := x"0050";      -- PulseTime      = 80
   signal param_threshold_high : std_logic_vector(15 downto 0) := x"0002";  -- ThresholdHigh = 2
   signal param_threshold_low : std_logic_vector(15 downto 0) := x"0000";   -- ThresholdLow     = 0
   
   signal param_sirio_reset_threshold : std_logic_vector(31 downto 0) := x"00000BB8";   -- sirio reset threshold = 3000
   signal param_sirio_reset_time : std_logic_vector(31 downto 0) := x"00000032";        -- sirio reset time = 50
   signal param_sirio_restoration_time : std_logic_vector(31 downto 0) := x"000000FA";  -- sirio restoration time = 250
   
   signal operation_mode : std_logic_vector(2 downto 0) := "111";         -- perationMode     = 0-Normal: Amplitudes Acquisition
   signal dma_transfer_size : std_logic_vector(31 downto 0) := x"00040000";     -- DMATransferSize = 262144
   
--   signal endofrun : std_logic := '0';
   signal fifo_out_tready : std_logic := '1';
   
   signal cf_1 : std_logic_vector(4 downto 0) := "00011";       -- coeff1 = 3
   signal cf_2 : std_logic_vector(4 downto 0) := "00101";       -- coeff2 = 5
   signal cf_3 : std_logic_vector(4 downto 0) := "01001";       -- coeff3 = 9
   
   signal acq_period : std_logic_vector(31 downto 0) := x"00000FFF";

 	--Outputs
   signal fifo_wr_en_mux_out : std_logic;
   signal reg_total_photon_counter : std_logic_vector(31 downto 0);
   signal reg_dead_time_main_FIR : std_logic_vector(31 downto 0);
   signal reg_sirio_resets_counter : std_logic_vector(31 downto 0);
   signal FIR_data_top : std_logic_vector(15 downto 0);
   signal FIR_Deriv_data_top : std_logic_vector(15 downto 0);
   signal Sirio_reset : std_logic;
   signal FIFO_full : std_logic;
   signal FIFO_empty : std_logic;
   signal fifo_out_tvalid : std_logic;
   signal fifo_out_tdata : std_logic_vector(15 downto 0);
   signal fifo_out_tlast : std_logic;

   -- Clock period definitions
   constant clk_period : time := 10 ns;
   
   type Ph_trace is array (0 to 1023) of std_logic_vector(11 downto 0);
   constant photons : Ph_trace :=(x"11D", x"11C", x"11E", x"11F", x"11E", x"11E", x"11F", x"11E", x"11E", x"11F", x"11E", x"11E", x"11F", x"11F", x"11F", x"120", x"120", x"121", x"121", x"121", x"122", x"122", x"122", x"121", x"122", x"121", x"121", x"122", x"120", x"122", x"122", x"121", x"121", x"123", x"124", x"121", x"124", x"123", x"123", x"123", x"124", x"124", x"125", x"124", x"124", x"123", x"121", x"122", x"122", x"121", x"121", x"122", x"122", x"121", x"123", x"121", x"122", x"123", x"122", x"123", x"123", x"123", x"121", x"123", x"123", x"123", x"123", x"121", x"123", x"124", x"122", x"122", x"122", x"122", x"122", x"121", x"120", x"120", x"120", x"121", x"120", x"121", x"122", x"122", x"122", x"122", x"123", x"123", x"123", x"123", x"123", x"123", x"123", x"123", x"123", x"122", x"121", x"120", x"120", x"11F", x"11D", x"11C", x"11E", x"11F", x"11E", x"11E", x"11F", x"11E", x"11E", x"11F", x"11E", x"11E", x"11F", x"11F", x"11F", x"120", x"120", x"121", x"121", x"121", x"122", x"122", x"122", x"121", x"122", x"121", x"121", x"122", x"120", x"122", x"122", x"121", x"121", x"123", x"124", x"121", x"124", x"123", x"123", x"123", x"124", x"124", x"125", x"124", x"124", x"123", x"121", x"122", x"122", x"121", x"121", x"122", x"122", x"121", x"123", x"121", x"122", x"123", x"122", x"123", x"123", x"123", x"121", x"123", x"123", x"123", x"123", x"121", x"123", x"124", x"122", x"122", x"122", x"122", x"122", x"121", x"120", x"120", x"120", x"121", x"120", x"121", x"122", x"122", x"122", x"122", x"123", x"123", x"123", x"123", x"123", x"123", x"123", x"123", x"123", x"122", x"121", x"120", x"120", x"11F", x"11D", x"11C", x"11E", x"11F", x"11E", x"11E", x"11F", x"11E", x"11E", x"11F", x"11E", x"11E", x"11F", x"11F", x"11F", x"120", x"120", x"121", x"121", x"121", x"122", x"122", x"122", x"121", x"122", x"121", x"121", x"122", x"120", x"122", x"122", x"121", x"121", x"123", x"124", x"121", x"124", x"123", x"123", x"123", x"124", x"124", x"125", x"124", x"124", x"123", x"121", x"122", x"122", x"121", x"121", x"122", x"122", x"121", x"123", x"121", x"122", x"123", x"122", x"123", x"123", x"123", x"121", x"123", x"123", x"123", x"123", x"121", x"123", x"124", x"122", x"122", x"122", x"122", x"122", x"121", x"120", x"120", x"120", x"121", x"120", x"121", x"122", x"122", x"122", x"122", x"123", x"123", x"123", x"123", x"123", x"123", x"123", x"123", x"123", x"122", x"121", x"120", x"120", x"11F", x"11E", x"11C", x"11C", x"11B", x"11A", x"11B", x"11C", x"11C", x"11C", x"11C", x"11B", x"11C", x"11E", x"11C", x"11D", x"11D", x"11D", x"11D", x"11E", x"11D", x"11D", x"11C", x"11E", x"11D", x"11F", x"11F", x"11E", x"11E", x"120", x"121", x"121", x"122", x"122", x"123", x"123", x"123", x"123", x"123", x"123", x"122", x"124", x"124", x"123", x"124", x"124", x"125", x"123", x"123", x"123", x"122", x"123", x"120", x"120", x"121", x"120", x"121", x"121", x"120", x"120", x"120", x"120", x"120", x"120", x"120", x"120", x"120", x"11F", x"11F", x"11F", x"11F", x"120", x"11F", x"120", x"120", x"11F", x"120", x"121", x"121", x"121", x"122", x"123", x"122", x"123", x"123", x"123", x"122", x"124", x"124", x"123", x"124", x"124", x"125", x"125", x"126", x"126", x"126", x"127", x"126", x"127", x"128", x"128", x"129", x"129", x"12A", x"12C", x"12B", x"12E", x"130", x"130", x"132", x"134", x"137", x"139", x"13C", x"13D", x"140", x"141", x"145", x"147", x"14B", x"14E", x"152", x"157", x"15B", x"15F", x"164", x"168", x"16D", x"174", x"178", x"180", x"186", x"18D", x"195", x"19B", x"1A4", x"1AA", x"1B3", x"1BB", x"1C2", x"1C9", x"1CF", x"1D5", x"1DA", x"1DF", x"1E3", x"1E5", x"1E6", x"1E4", x"1E1", x"1DD", x"1D7", x"1CF", x"1C5", x"1B7", x"1A8", x"197", x"185", x"172", x"15E", x"14D", x"13C", x"12F", x"126", x"122", x"121", x"120", x"121", x"120", x"11F", x"120", x"11F", x"120", x"120", x"120", x"11F", x"11D", x"11E", x"11D", x"11D", x"11D", x"11D", x"11C", x"11B", x"11C", x"11D", x"11C", x"11D", x"11E", x"11E", x"11E", x"11F", x"11E", x"120", x"11F", x"121", x"121", x"121", x"122", x"122", x"123", x"124", x"124", x"123", x"123", x"124", x"123", x"123", x"124", x"122", x"123", x"122", x"121", x"120", x"123", x"122", x"122", x"121", x"121", x"120", x"121", x"121", x"122", x"121", x"122", x"123", x"124", x"123", x"123", x"121", x"122", x"122", x"123", x"121", x"122", x"122", x"121", x"121", x"121", x"120", x"121", x"122", x"121", x"122", x"122", x"121", x"121", x"121", x"121", x"121", x"11F", x"120", x"120", x"120", x"11F", x"11D", x"11E", x"11F", x"11F", x"11D", x"11D", x"11F", x"11E", x"11D", x"11C", x"11D", x"11F", x"11D", x"11F", x"11F", x"11F", x"120", x"120", x"121", x"120", x"121", x"122", x"122", x"123", x"123", x"123", x"123", x"124", x"124", x"123", x"124", x"125", x"123", x"123", x"123", x"123", x"124", x"123", x"123", x"122", x"121", x"123", x"121", x"124", x"123", x"124", x"125", x"126", x"125", x"125", x"125", x"125", x"125", x"125", x"125", x"123", x"123", x"123", x"122", x"123", x"122", x"121", x"121", x"120", x"120", x"120", x"11F", x"11E", x"121", x"11F", x"11E", x"11D", x"11D", x"11E", x"11D", x"11D", x"11D", x"11D", x"11D", x"11E", x"11F", x"11E", x"11F", x"11F", x"121", x"121", x"121", x"121", x"120", x"121", x"121", x"121", x"121", x"120", x"11F", x"120", x"11F", x"11E", x"11E", x"11D", x"11C", x"11B", x"11B", x"11C", x"11A", x"11A", x"11C", x"11B", x"11B", x"11B", x"11C", x"11D", x"11F", x"11D", x"11F", x"11E", x"11F", x"11E", x"11D", x"11E", x"11F", x"11E", x"11F", x"11F", x"120", x"120", x"121", x"121", x"122", x"121", x"122", x"123", x"124", x"123", x"125", x"124", x"125", x"125", x"125", x"126", x"127", x"125", x"126", x"127", x"125", x"125", x"125", x"124", x"125", x"122", x"123", x"123", x"124", x"124", x"124", x"124", x"124", x"123", x"123", x"122", x"123", x"122", x"121", x"121", x"120", x"120", x"120", x"11F", x"11E", x"121", x"11F", x"11E", x"11D", x"11D", x"11E", x"11D", x"11D", x"11D", x"11D", x"11D", x"11E", x"11F", x"11E", x"11F", x"11F", x"121", x"121", x"121", x"121", x"120", x"121", x"121", x"121", x"121", x"120", x"11F", x"120", x"11F", x"11E", x"11E", x"11D", x"11C", x"11B", x"11B", x"11C", x"11A", x"11A", x"11C", x"11B", x"11B", x"11B", x"11C", x"11D", x"11F", x"11D", x"11F", x"11E", x"11F", x"11E", x"11D", x"11E", x"11F", x"11E", x"11F", x"11F", x"120", x"120", x"121", x"121", x"122", x"121", x"122", x"123", x"124", x"123", x"125", x"124", x"125", x"125", x"125", x"126", x"127", x"125", x"126", x"127", x"125", x"125", x"125", x"124", x"125", x"122", x"123", x"123", x"124", x"124", x"124", x"124", x"124", x"123", x"123", x"122", x"123", x"122", x"121", x"121", x"120", x"120", x"120", x"11F", x"11E", x"121", x"11F", x"11E", x"11D", x"11D", x"11E", x"11D", x"11D", x"11D", x"11D", x"11D", x"11E", x"11F", x"11E", x"11F", x"11F", x"121", x"121", x"121", x"121", x"120", x"121", x"121", x"121", x"121", x"120", x"11F", x"120", x"11F", x"11E", x"11E", x"11D", x"11C", x"11B", x"11B", x"11C", x"11A", x"11A", x"11C", x"11B", x"11B", x"11B", x"11C", x"11D", x"11F", x"11D", x"11F", x"11E", x"11F", x"11E", x"11D", x"11E", x"11F", x"11E", x"11F", x"11F", x"120", x"120", x"121", x"121", x"122", x"121", x"122", x"123", x"124", x"123", x"125", x"124", x"125", x"125", x"125", x"126", x"127", x"125", x"126", x"127", x"125", x"125", x"125", x"124", x"125", x"122", x"123", x"123", x"124", x"124", x"124", x"124", x"124", x"123", x"123", x"122", x"123", x"122", x"121", x"121", x"120", x"120", x"120", x"11F", x"11E", x"121", x"11F", x"11E", x"11D", x"11D", x"11E", x"11D", x"11D", x"11D", x"11D", x"11D", x"11E", x"11F", x"11E", x"11F", x"11F", x"121", x"121", x"121", x"121", x"120", x"121", x"121", x"121", x"121", x"120", x"11F", x"120", x"11F", x"11E", x"11E", x"11D", x"11C", x"11B", x"11B", x"11C", x"11A", x"11A", x"11C", x"11B", x"11B", x"11B", x"11C", x"11D", x"11F", x"11D", x"11F", x"11E", x"11F", x"11E", x"11D", x"11E", x"11F", x"11E", x"11F", x"11F", x"120", x"120", x"121", x"121", x"122", x"121", x"122", x"123", x"124", x"123", x"125", x"124", x"125", x"125", x"125", x"126", x"127", x"125", x"126", x"127", x"125", x"125", x"125", x"124", x"125", x"122", x"123", x"123", x"124", x"124", x"124", x"124", x"124", x"123", x"123", x"124", x"124", x"124", x"124", x"124", x"124", x"124");
   
   signal cnt : integer range 0 to 1024;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: top_macromodule PORT MAP (
          fifo_wr_en_mux_out => fifo_wr_en_mux_out,
          clk => clk,
          reset => reset,
          ADC_data => ADC_data,
          param_peak_time => param_peak_time,
          param_pulse_time => param_pulse_time,
          param_threshold_high => param_threshold_high,
          param_threshold_low => param_threshold_low,
          param_sirio_reset_threshold => param_sirio_reset_threshold,
          param_sirio_reset_time => param_sirio_reset_time,
          param_sirio_restoration_time => param_sirio_restoration_time,
          operation_mode => operation_mode,
          dma_transfer_size => dma_transfer_size,
          reg_total_photon_counter => reg_total_photon_counter,
          reg_dead_time_main_FIR => reg_dead_time_main_FIR,
          reg_sirio_resets_counter => reg_sirio_resets_counter,
          FIR_data_top => FIR_data_top,
          FIR_Deriv_data_top => FIR_Deriv_data_top,
          Sirio_reset => Sirio_reset,
--          endofrun => endofrun,
          FIFO_full => FIFO_full,
          FIFO_empty => FIFO_empty,
          fifo_out_tvalid => fifo_out_tvalid,
          fifo_out_tready => fifo_out_tready,
          fifo_out_tdata => fifo_out_tdata,
          fifo_out_tlast => fifo_out_tlast,
          cf_1 => cf_1,
          cf_2 => cf_2,
          cf_3 => cf_3,
          acq_period => acq_period
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 
 
    -- data process
  data_process : process
  begin
    wait for clk_period/2;
    if(reset = '1') then
        if(cnt = 0) then
            cnt <= 1024;
        else
            ADC_data <= photons(cnt-1);
            cnt <= cnt - 1;       
        end if;
    else
        cnt <= 1024;
        ADC_data <= (others => '0');
    end if;
    wait for clk_period/2;  
  end process;

   -- Stimulus process
   stim_proc: process
   begin		
   wait for clk_period*10;
   reset <= '1';        
      -- hold reset state for 100 ns.
    wait for 20 us;
    
  --  endofrun <= '1';
      -- insert stimulus here

      wait;
   end process;

END;
