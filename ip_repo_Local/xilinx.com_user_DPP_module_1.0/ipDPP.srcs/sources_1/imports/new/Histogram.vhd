----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/13/2017 01:54:31 PM
-- Design Name: 
-- Module Name: Histogram - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Histogram is
    Port ( clk : in STD_LOGIC;
           resetn : in STD_LOGIC;
           din : in STD_LOGIC_VECTOR (11 downto 0);
           dvalid : in STD_LOGIC;
           acq_T : in STD_LOGIC_VECTOR (31 downto 0);
           dout : out STD_LOGIC_VECTOR (31 downto 0);
           tready : in STD_LOGIC;
           tvalid : out STD_LOGIC;
           endof_acq : out std_logic
           );
end Histogram;

architecture Behavioral of Histogram is

    COMPONENT TDPR32x4096
      PORT (
        clka : IN STD_LOGIC;
        wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        addra : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
        dina : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        douta : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        clkb : IN STD_LOGIC;
        web : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        addrb : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
        dinb : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        doutb : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
      );
    END COMPONENT;

    -- state machine varibles
    type state is (stReset, stClear, stStore, stIpDelay, stIncr, stWaitRead, stTx);
    signal HistState : state;
    
    signal Din_A, Din_B, Dout_A : std_logic_vector(31 downto 0);
    signal WEn_portA, WEn_portB : std_logic_vector(0 downto 0);
    signal Addr_portA, Addr_portB : std_logic_vector(11 downto 0);
    signal cnt, sDelay : std_logic_vector(31 downto 0);
    
 --   signal endof_acq : std_logic;

begin

    Hist_BRAM : TDPR32x4096
      PORT MAP (
        clka => clk,
        wea => WEn_portA,
        addra => Addr_PortA,
        dina => Din_A,
        douta => Dout_A,
        
        clkb => clk,
        web => WEn_portB,
        addrb => Addr_portB,
        dinb => Din_B,
        doutb => dout
      );

    Din_A <= Dout_A + 1;

SateMachine: process(clk, resetn)
    begin
    if( resetn = '0') then
        HistState <= stReset;
    else
        if(rising_edge(clk)) then
            case HistState is
                when stReset =>                         -- reset memory
                    tvalid <= '0';
                    WEn_portA <= "0";
                    WEn_portB <= "0";
                    cnt <= (others => '0');
                    endof_acq <= '1';
                    HistState <= stClear;
                    
                when stClear =>                         -- Clear the memory
                    endof_acq <= '1';
                    WEn_portB <= "0";
                    if(cnt < 4096) then
                        Din_B <= (others => '0');
                        WEn_portB <= "1";
                        Addr_portB <= cnt(11 downto 0);
                        cnt <= cnt + '1';
                        
                        HistState <= stClear;
                    else
                        endof_acq <= '0';
                        cnt <= (others => '0');
                        HistState <= stStore;
                    end if;
                
                when stStore =>                             -- acquire data for acq_T events
                    WEn_portA <= "0";
                        if(cnt < acq_T) then
                                if(dvalid = '1') then
                                    --WEn_portA <= "1";
                                    Addr_PortA <= din;
                                    --cnt <= cnt + '1';
                                    sDelay <= (others => '0');
                                    HistState <= stIpDelay;
                                end if;
                            cnt <= cnt + '1';
                        else
                            endof_acq <= '1';
                            HistState <= stWaitRead;
                        end if;
                
                when stIpDelay =>
                    if( sDelay < 1) then
                        sDelay <= sDelay + '1';
                    else
                        WEn_portA <= "1";
                        HistState <= stIncr;
                    end if;
                    
                when stIncr =>
                    WEn_portA <= "0";
                    HistState <= stStore;
                    
                when stWaitRead =>                      -- wait for fifo read
                    if(tready = '0') then
                        HistState <= stWaitRead;
                    else
                        cnt <= (others => '0');
                        HistState <= stTx;
                    end if;
                    
                when stTx =>                            -- write to fifo
                    tvalid <= '0';
                    WEn_portB <= "0";
                        if(cnt < 4096) then
                                if(tready = '1') then
                                Addr_portB <= cnt(11 downto 0);
                                cnt <= cnt + '1';
                                tvalid <= '1';
                                --WEn_portB <= "1";
                                --Din_B <= (others => '0');
                                end if;
                            HistState <= stTx;
                        else
                            cnt <= (others => '0');
                            HistState <= stClear;
                        end if;
                    
                when others =>
                    HistState <= stClear;
            end case;
        end if;
    end if;
    end process;
end Behavioral;
