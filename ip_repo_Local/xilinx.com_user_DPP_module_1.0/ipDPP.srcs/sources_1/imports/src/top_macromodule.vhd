----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    05:04:38 09/11/2015 
-- Design Name: 
-- Module Name:    top_macromodule - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
use IEEE.std_logic_signed.all;

entity DPP_module is
    Port (         
    
--              Main_fir_out : out STD_LOGIC_VECTOR(15 DOWNTO 0); --Main_fir data out
--              data_out : out  STD_LOGIC_VECTOR (13 downto 0); -- four bit is gained with the average derivative
              fifo_wr_en_mux_out: out std_logic;
              
			 --Global signals
			  clk : in  STD_LOGIC;
           reset : in  STD_LOGIC;
			  
			 -- Famu Interface
				ADC_data : in  STD_LOGIC_VECTOR (11 downto 0); --From FAMU/others ADC
			 -- reset_to_famu : out  STD_LOGIC; -- To be issued from the PS
			 -- clk_to_famu : out  STD_LOGIC;   -- To be issued from the PS

			--Input parameters
				param_peak_time : in  STD_LOGIC_VECTOR (15 downto 0);     --20
				param_pulse_time : in  STD_LOGIC_VECTOR (15 downto 0);		--40
				param_threshold_high : in  STD_LOGIC_VECTOR (15 downto 0); --?
				param_threshold_low : in  STD_LOGIC_VECTOR (15 downto 0);  --?
				param_sirio_reset_threshold : in  STD_LOGIC_VECTOR (31 downto 0);  --3000?
				param_sirio_reset_time : in  STD_LOGIC_VECTOR (31 downto 0);  -- Time to keep sirio reset active
				param_sirio_restoration_time : in  STD_LOGIC_VECTOR (31 downto 0); -- Restoration Time after sirio reset 
				operation_mode : in  STD_LOGIC_VECTOR (2 downto 0); --20(adc_reading, FIR_reading)
				dma_transfer_size : in  STD_LOGIC_VECTOR (31 downto 0);
			
			--Output data registers and counter
--         total_photon_counter : inout  STD_LOGIC_VECTOR (31 downto 0); --total detected photon in run
--			  dead_time_main_FIR: inout  STD_LOGIC_VECTOR (31 downto 0);
--			  sirio_resets_counter: out  STD_LOGIC_VECTOR (31 downto 0);
				
				reg_total_photon_counter : out  STD_LOGIC_VECTOR (31 downto 0); --total detected photon in run
				reg_dead_time_main_FIR: out  STD_LOGIC_VECTOR (31 downto 0);
				reg_sirio_resets_counter: out  STD_LOGIC_VECTOR (31 downto 0);
				
				-- for debugging
			    FIR_data_top : out std_logic_vector(15 downto 0);
                FIR_Deriv_data_top : out std_logic_vector(15 downto 0);   
				
			-- Data and handshaking signal to the PS	
           --FIFO_amplitudes_out : out  STD_LOGIC_VECTOR (15 downto 0);
          -- time_stamps : out  STD_LOGIC_VECTOR (15 downto 0);
          --           FIFO_empty : out  STD_LOGIC;
--			  FIFO_prog_full : out  STD_LOGIC;
--           FIFO_read_clock : in  STD_LOGIC;
--           FIFO_read_clock_enable : in  STD_LOGIC;

			-- Interface with detector board	
				Sirio_reset : out  STD_LOGIC;
--				Sirio_mask_clk : out  STD_LOGIC; 				--To Be Executed By The PS
--				Sirio_mask_data : out  STD_LOGIC; 				--To Be Executed By The PS
--				Sirio_stimul_test: out   STD_LOGIC; 			--To Be Executed By The PS
				
			-- Interface with EXAFS Experiment
--           start_of_run : in  STD_LOGIC; -- It is equivalent to remove the reset
--            endofrun : in  STD_LOGIC;
            
            --
            FIFO_full : out  STD_LOGIC;
            FIFO_empty : out  STD_LOGIC;
            fifo_out_tvalid : out STD_LOGIC;
            fifo_out_tready : in STD_LOGIC;
            fifo_out_tdata : out STD_LOGIC_VECTOR(15 DOWNTO 0);
            fifo_out_tlast : out STD_LOGIC;   -- To be generated by the PS. 
            
            cf_1: in std_logic_vector(4 downto 0);
            cf_2: in std_logic_vector(4 downto 0);
            cf_3: in std_logic_vector(4 downto 0);
            
            acq_period : in STD_LOGIC_VECTOR(31 DOWNTO 0)
            );
			  
end DPP_module;

architecture Behavioral of DPP_module is

	COMPONENT Operation_mode_decoder
	PORT(
		op_mode_in : IN std_logic_vector(2 downto 0);
		FIFO_wr_en_in : IN std_logic;
		sirio_rst_restor : IN std_logic;
		FIR_data : IN std_logic_vector(15 downto 0);
		ADC_data_in : IN std_logic_vector(11 downto 0);
		FIR_Deriv_data : IN std_logic_vector(15 downto 0);          
		FIFO_wr_en_valid : OUT std_logic;
		FIFO_DATA_IN : OUT std_logic_vector(15 downto 0);
		hist_dout    : in STD_LOGIC_VECTOR (15 downto 0);
        hist_tvalid  : in STD_LOGIC
		);
	END COMPONENT;

COMPONENT Photon_counters
    Port ( clk : in STD_LOGIC;
           reset : in STD_LOGIC;
           photon_arrival : in STD_LOGIC;
           long_rejection_flag : in STD_LOGIC;
           EndOfRun : in STD_LOGIC;
           reg_total_photon_counter : out  STD_LOGIC_VECTOR (31 downto 0); --total detected photon in run
           reg_dead_time_main_FIR: out  STD_LOGIC_VECTOR (31 downto 0);
           reg_sirio_resets_counter: out  STD_LOGIC_VECTOR (31 downto 0)
           );
end COMPONENT;


COMPONENT fir_derivative_0 
	PORT (
			data_in : in  STD_LOGIC_VECTOR (11 downto 0);
         data_out : out  STD_LOGIC_VECTOR (15 downto 0); -- four bit is gained with the average derivative
         clk : in  STD_LOGIC;
         reset : in  STD_LOGIC;
         cf_1: in std_logic_vector(4 downto 0);
         cf_2: in std_logic_vector(4 downto 0);
         cf_3: in std_logic_vector(4 downto 0));
	END COMPONENT;
	 
COMPONENT edge_detector_0
   PORT(
         data_in : IN  std_logic_vector(15 downto 0);
         clk : IN  std_logic;
         reset : IN  std_logic;
         threshold_high : IN  std_logic_vector(15 downto 0);
         threshold_low : IN  std_logic_vector(15 downto 0);
         arrival_flag : OUT  std_logic;
         sirio_reset_restoration : IN  std_logic;
         rejection_flag : OUT  std_logic);
	END COMPONENT;
	 
COMPONENT rejection_logic_0
   PORT(
         clk : IN  std_logic;
         reset : IN  std_logic;
         photon_arrival : IN  std_logic;
         peak_time : IN  std_logic_vector(15 downto 0);
         pulse_time : IN  std_logic_vector(15 downto 0);
         rejection_flag : OUT  std_logic;
         ampl_FIFO_we : OUT  std_logic;
         dead_time_counter : INOUT  std_logic_vector(31 downto 0));
	END COMPONENT;
  
COMPONENT Main_FIR
	PORT (
--	clk: in std_logic;                           aclk
--	rfd: out std_logic;                          --
--	rdy: out std_logic;                          --
--	din: in std_logic_vector(11 downto 0);       s_axis_data_tdata
--	dout: out std_logic_vector(15 downto 0));    m_axis_data_tdata
	
	aclk : IN STD_LOGIC;
    s_axis_data_tvalid : IN STD_LOGIC;
    s_axis_data_tready : OUT STD_LOGIC;
    s_axis_data_tdata : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    m_axis_data_tvalid : OUT STD_LOGIC;
    m_axis_data_tdata : OUT STD_LOGIC_VECTOR(15 DOWNTO 0));
	END COMPONENT;

COMPONENT FIFO_to_PS
  PORT (
--    rst : IN STD_LOGIC;                       s_aresetn
--    wr_clk : IN STD_LOGIC;                    s_aclk
--    rd_clk : IN STD_LOGIC;                    m_aclk
--    din : IN STD_LOGIC_VECTOR(15 DOWNTO 0);   s_axis_tdata
--    wr_en : IN STD_LOGIC;                     s_axis_tvalid
--    rd_en : IN STD_LOGIC;                     m_axis_tready
--    dout : OUT STD_LOGIC_VECTOR(15 DOWNTO 0); m_axis_tdata
--    full : OUT STD_LOGIC;                     --
--    empty : OUT STD_LOGIC;                    --
--    prog_full : OUT STD_LOGIC                 axis_overflow
    
    m_aclk : IN STD_LOGIC;
    s_aclk : IN STD_LOGIC;
    s_aresetn : IN STD_LOGIC;
    s_axis_tvalid : IN STD_LOGIC;
    s_axis_tready : OUT STD_LOGIC;
    s_axis_tdata : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    s_axis_tlast : IN STD_LOGIC;
    m_axis_tvalid : OUT STD_LOGIC;
    m_axis_tready : IN STD_LOGIC;
    m_axis_tdata : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    m_axis_tlast : OUT STD_LOGIC;
    axis_prog_full : OUT STD_LOGIC;
    axis_prog_empty : OUT STD_LOGIC
  );
END COMPONENT;

COMPONENT sirio_reset_block 
  PORT ( 
	ADC_data : in  STD_LOGIC_VECTOR (11 downto 0);
	sirio_reset : out  STD_LOGIC;
	sirio_reset_restoration: out  STD_LOGIC; --to avoid false photon detection du to sirio reset restoration (spurious positive pulse)
	param_sirio_reset_threshold : in  STD_LOGIC_VECTOR (31 downto 0);  --3000?
	param_sirio_reset_time  : in  STD_LOGIC_VECTOR (31 downto 0); --waiting time to complete reset
	param_sirio_restoration_time : in  STD_LOGIC_VECTOR (31 downto 0); --waiting time for complete restoration
   sirio_resets_counter_out : out  STD_LOGIC_VECTOR (31 downto 0);
   clock : in  STD_LOGIC;
   reset : in  STD_LOGIC);
END COMPONENT;

COMPONENT tlast_generator is
    Port ( clk : in STD_LOGIC;
           rst : in STD_LOGIC;
           wr_en  : in STD_LOGIC;
           packet_size : in STD_LOGIC_VECTOR (31 downto 0);
           fifo_tready : in STD_LOGIC;
           tlast : out STD_LOGIC);
end COMPONENT tlast_generator;

	COMPONENT Histogram
	PORT(
		clk : IN std_logic;
		resetn : IN std_logic;
		din : IN std_logic_vector(11 downto 0);
		dvalid : IN std_logic;
		acq_T : IN std_logic_vector(31 downto 0);
		tready : IN std_logic;          
		dout : OUT std_logic_vector(31 downto 0);
		tvalid : OUT std_logic;
		endof_acq : out std_logic
		);
	END COMPONENT;

--Signals


signal FIR_Derivative_data: std_logic_vector(15 downto 0) ; --
signal photon_arrival: std_logic; --
signal short_rejection_flag: std_logic; -- 
signal long_rejection_flag: std_logic; --
signal FIFO_wr_en: std_logic; --
signal FIR_data_out: std_logic_vector(15 downto 0) ; ----Main_fir data out
signal dead_time_counter: std_logic_vector(31 downto 0) ; --


signal total_photon_counter :   STD_LOGIC_VECTOR (31 downto 0); --total detected photon in run 
signal dead_time_main_FIR:   STD_LOGIC_VECTOR (31 downto 0);
signal sirio_resets_counter:   STD_LOGIC_VECTOR (31 downto 0);
signal sirio_reset_restoration: STD_LOGIC;
signal fifo_wr_en_mux: std_logic; --
signal FIFO_DATA_IN_mux:    STD_LOGIC_VECTOR (15 downto 0);

signal fifo_tready : STD_LOGIC;
signal tlast : STD_LOGIC;

signal his_dvalid : STD_LOGIC;
signal his_dout : std_logic_vector(31 downto 0);
signal his_tvalid : std_logic;

signal EndOfRun : STD_LOGIC;

begin


---- Definition of Operation modes

--OPERATION_MODE_DECODING: process (operation_mode)
--   begin
--      case (operation_mode) is
--         when "000" =>          	-- Normal: Amplitudes Acquisition 
--                FIFO_wr_en_mux <= FIFO_wr_en AND NOT sirio_reset_restoration;
--			    FIFO_DATA_IN_mux <= FIR_data_out; 

--         when "001" =>				-- ADC debbuging: Continuous ADC Data Acquisition 
--                FIFO_wr_en_mux <= '1';
-- 				FIFO_DATA_IN_mux <= "0000"&ADC_data;
					
--         when "010" =>				-- FIR debbuging: Continuous output FIR Acquisition 
--                FIFO_wr_en_mux <= '1';
--				FIFO_DATA_IN_mux <= FIR_data_out;
			
--         when "100" =>
--                FIFO_wr_en_mux <= '1';
--				FIFO_DATA_IN_mux <= FIR_Derivative_data;  
					
--         when others =>
--               FIFO_wr_en_mux <= FIFO_wr_en AND NOT sirio_reset_restoration;
--               FIFO_DATA_IN_mux <= FIR_data_out;					
--      end case;      
--   end process;


----Updating Counters
----Total detected photons
--SYNC_total_photon_counter : process (clk, reset, photon_arrival)
--   begin
--      if (clk'event and clk = '1') then
--         if (reset = '0') then
--          total_photon_counter <= (others => '0');
--         elsif (photon_arrival = '1') then
--          total_photon_counter <= total_photon_counter + 1;
--         end if; 
--		end if;
--end process;

----Total dead time due to main FIR output (in system clock cycles) (The same as the one calculated by the logic_rejection_block ("dead_time_counter"))
--SYNC_dead_time_main_FIR : process (clk,reset)
--   begin
--      if (clk'event and clk = '1') then
--         if (reset = '0') then
--          dead_time_main_FIR <=(others => '0');
--         elsif (long_rejection_flag = '1') then
--          dead_time_main_FIR <= dead_time_main_FIR + 1;
--         end if; 
--		end if;
--end process;	


---- Saving value of counters (before reset!) (Assuming FIFO doesn't become FULL)
--SYNC_Saving_Counters : process (clk,EndOfRun)
--   begin
--      if (clk'event and clk = '1') then
--         if (EndOfRun ='1' ) then   -- EndOfRun must be put to '1' then to '0' (to save counters), and immediately after send a reset to the Macroblock
--          reg_total_photon_counter <= total_photon_counter ; --Acquired photons must be read from FIFO
--			 reg_dead_time_main_FIR   <= dead_time_main_FIR ;
--			 reg_sirio_resets_counter <= sirio_resets_counter ; 
--         end if; 
--		end if;
--end process;

FIR_data_top <= FIR_data_out;
FIR_Deriv_data_top <= FIR_Derivative_data;

-- COMPONENTS INSTANTIATION SECTION
my_op_mode_dec: Operation_mode_decoder PORT MAP(
    op_mode_in => operation_mode,
    FIFO_wr_en_in => FIFO_wr_en,
    sirio_rst_restor => sirio_reset_restoration,
    FIR_data => FIR_data_out,
    ADC_data_in => ADC_data,
    FIR_Deriv_data => FIR_Derivative_data,
    FIFO_wr_en_valid => FIFO_wr_en_mux,
    FIFO_DATA_IN => FIFO_DATA_IN_mux,
    hist_dout    => his_dout(15 downto 0),
    hist_tvalid  => his_tvalid
    );

my_photon_counter : Photon_counters
    Port map( clk => clk,
           reset => reset,
           photon_arrival => photon_arrival,
           long_rejection_flag => long_rejection_flag,
           EndOfRun => EndOfRun,
           reg_total_photon_counter => reg_total_photon_counter,
           reg_dead_time_main_FIR => reg_dead_time_main_FIR,
           reg_sirio_resets_counter => reg_sirio_resets_counter
           );

my_fir_derivative_0 : fir_derivative_0
		port map (
		data_in => ADC_data, 
      data_out  => FIR_Derivative_data,  
      clk  => clk, 
      reset =>  reset,
      cf_1 => cf_1,
      cf_2 => cf_2,
      cf_3 => cf_3);
	
my_edge_detector_0 : edge_detector_0
		port map (
		 data_in => FIR_Derivative_data, 
         clk  => clk, 
         reset => reset, 
         threshold_high  =>  param_threshold_high, 
         threshold_low  =>  param_threshold_low, 
         arrival_flag => photon_arrival, 
         sirio_reset_restoration => sirio_reset_restoration, --input from sirio reset block
         rejection_flag  => short_rejection_flag);

my_rejection_logic_0 : rejection_logic_0
  PORT MAP (
	clk  => clk , 
	reset => reset , 
	photon_arrival  => photon_arrival , 
   peak_time   =>  param_peak_time , 
   pulse_time   =>  param_pulse_time, 
   rejection_flag   =>  long_rejection_flag, 
   ampl_FIFO_we   => FIFO_wr_en , 
   dead_time_counter   =>  dead_time_counter);					

my_main_FIR : Main_FIR
  PORT MAP (
--	clk => clk,
--	rfd => open, --rfd,
--	rdy => open, --rdy,
--	din => ADC_data,
--	dout => FIR_data_out);
	
	--	clk: in std_logic;                           aclk
    --    rfd: out std_logic;                          --
    --    rdy: out std_logic;                          --
    --    din: in std_logic_vector(11 downto 0);       s_axis_data_tdata
    --    dout: out std_logic_vector(15 downto 0));    m_axis_data_tdata
        
    aclk => clk,
    s_axis_data_tvalid => '1',
    s_axis_data_tready => open,
    s_axis_data_tdata => "0000"&ADC_data,
    m_axis_data_tvalid => open,
    m_axis_data_tdata => FIR_data_out
    );

my_FIFO : FIFO_to_PS
  PORT MAP (
--    rst => reset,
--    wr_clk => clk,
--    rd_clk => FIFO_read_clock,
--    din => 	FIFO_DATA_IN_mux,			--Depending on operation mode
--    wr_en => FIFO_wr_en_mux,			--Depending on operation mode
--    rd_en => FIFO_read_clock_enable,
--    dout => FIFO_amplitudes_out,
--    full => FIFO_full,
--    empty => FIFO_empty,
--    prog_full => FIFO_prog_full);
    
   
    m_aclk => clk,
    s_aclk => clk,
    s_aresetn => reset,
    s_axis_tvalid => FIFO_wr_en_mux,
    s_axis_tready => fifo_tready,
    s_axis_tdata => FIFO_DATA_IN_mux,
    s_axis_tlast => tlast,                           --AQUI VA EL TLAST QUE VOY A GENERAR                   
    m_axis_tvalid => fifo_out_tvalid,
    m_axis_tready => fifo_out_tready,        -- m_axis_tready => FIFO_read_clock_enable,
    m_axis_tdata => fifo_out_tdata,          -- m_axis_tdata => FIFO_amplitudes_out,   
    m_axis_tlast => fifo_out_tlast,
    axis_prog_full => FIFO_full,
    axis_prog_empty => FIFO_empty
    );
	 
my_sirio_reset_block : sirio_reset_block 
  PORT MAP( 
	ADC_data =>  ADC_data,
	sirio_reset => Sirio_reset, 
	sirio_reset_restoration => sirio_reset_restoration, --output for the edge detector
	param_sirio_reset_threshold => param_sirio_reset_threshold, 
	param_sirio_reset_time => param_sirio_reset_time,  			--waiting time to complete reset
	param_sirio_restoration_time =>  param_sirio_restoration_time, --waiting time for complete restoration
   sirio_resets_counter_out =>  sirio_resets_counter, 
   clock => clk, 
   reset => reset);
   
my_tlast_generator : tlast_generator
  Port Map(
   clk => clk,
   rst => reset,
   wr_en => FIFO_wr_en_mux,
   packet_size => dma_transfer_size,   --131072    luego lo cargare de registro
   fifo_tready => fifo_tready,
   tlast => tlast);
  

--Main_fir_out <= FIR_data_out;
--data_out <= FIR_Derivative_data;
fifo_wr_en_mux_out <= fifo_wr_en_mux;

his_dvalid <= FIFO_wr_en and not sirio_reset_restoration;
my_Histogram: Histogram PORT MAP(
		clk => clk,
		resetn => reset,
		din => FIR_data_out(11 downto 0),
		dvalid => his_dvalid,
		acq_T => acq_period,
		dout => his_dout,
		tready => fifo_tready,
		tvalid => his_tvalid,
		endof_acq => EndOfRun
	);

end Behavioral;

