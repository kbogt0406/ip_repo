----------------------------------------------------------------------------------
-- Company: MLAB (ICTP-INFN)
-- Engineers: A. Cicuttin, M.L. Crespo
-- 
-- Create Date:    10:38:36 08/27/2015 
-- Design Name: 
-- Module Name:    fir_edge_detector_0 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
-- 		This module uses the detection output flag from module "edge_detector_0.vhd" to 
--       enable pulse amplitude measurment (wirte enabling a FIFO input port) and at the 
--			same time rejecting pile-up due to close incoming photons according to the parameters:
--			"peak_time" and "reject_time" (expressed in clock cycles) 
--       There are two different time resolutions: 
--			one for edge detection:
--			and another for amplitude measurement: 
-- 		 
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created 
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use IEEE.std_logic_signed.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity rejection_logic_0 is
		generic (
					width_param: natural:=16;
					width_ph_counter: natural:=24;
					width_dt_counter: natural:=32); --generic declaration 
    Port ( 
			--Inputs
           clk : in  STD_LOGIC;
           reset : in  STD_LOGIC;
			  photon_arrival: in  STD_LOGIC; --coming from the edge detector (when "din" surpasses the "threshold")

			--Parameters
           peak_time : in  STD_LOGIC_VECTOR (width_param-1 downto 0); --time from pulse arrival detection and pulsepeack (in clock cycles)
           pulse_time: in  STD_LOGIC_VECTOR (width_param-1 downto 0);
			  
			--Outputs
           rejection_flag : out  STD_LOGIC;   -- it is kept high during "rejection time" cycles
           ampl_FIFO_we : out  STD_LOGIC; -- it goes high only one clock cycle to enable "amplitude" recording 
			  
	--lost_photons_counter: STD_LOGIC_VECTOR (15 downto 0); --A Meally State Machine necessary to implement his counter
			  dead_time_counter: inout STD_LOGIC_VECTOR (31 downto 0) );  --Four bytes

end rejection_logic_0;

architecture Behavioral of rejection_logic_0 is

 --   Description of the finite state machine
 -- st_reset				: 	while active reset 
 -- st_waiting_photon	: 	normal state without photons
 -- st_waiting_peak		:	from arrival to midle of flat top
 -- st_FIFO_wr_en			:  last only one clock cycle to enable FIFO writing measured amplitude
 -- st_waiting_endof_pulse : to count main FIR out pulse duration from last photon arrival (rejection time)
	 

  type state_type is (st_reset, st_waiting_photon, st_waiting_peak, st_FIFO_wr_en, st_waiting_endof_pulse); 
  signal state, next_state : state_type; 
 
 --Declare internal signals for all outputs of the state-machine

	signal count_PeakT: std_logic_vector(7 downto 0); --counter "Peak_Time"
	signal count_RejT: std_logic_vector(7 downto 0); --counter "Pulse_Time"

begin 
 
--Updating states
   SYNC_PROC: process (clk,reset)
   begin
      if (clk'event and clk = '1') then
         if (reset = '0') then
            state <= st_reset;
         else
            state <= next_state;
         end if;        

      if state = st_reset then
				count_PeakT <= (others => '0');
 				count_RejT <= (others => '0');
			   dead_time_counter <= (others => '0');
								
		elsif state = st_waiting_photon then  
				count_PeakT <= (others => '0');
				count_RejT <= (others => '0');
				
		elsif state = st_waiting_peak then      
				count_PeakT <= count_PeakT + 1;
 				count_RejT  <= count_RejT + 1;
			if Photon_arrival = '1' then 
				count_RejT <=(others => '0');
			else
 				count_RejT <= count_RejT + 1;
			end if;
			   dead_time_counter <= dead_time_counter + 1;
				
		elsif state = st_FIFO_wr_en then    
				count_PeakT <=(others => '0');
 				count_RejT <= count_RejT + 1;
			   dead_time_counter <= dead_time_counter + 1;
				
		elsif state = st_waiting_endof_pulse then     

				count_PeakT <=(others => '0');
			if Photon_arrival = '1' then 
				count_RejT <=(others => '0');
			else
 				count_RejT <= count_RejT + 1;
			end if;
			   dead_time_counter <= dead_time_counter + 1;			
      end if;

   end if;
 end process;
 
-- Outputs based on state only
DECODING_OUTPUTS: process (state, photon_arrival)
   begin
      --insert statements to decode internal output signals
      --below is simple example
      if state = st_reset then
				rejection_flag  <= '0';  
				ampl_FIFO_we <= '0'; 
								
		elsif state = st_waiting_photon then  
				rejection_flag  <= '0';  
				ampl_FIFO_we <= '0';
				
		elsif state = st_waiting_peak then      
				rejection_flag  <= '1';  
				ampl_FIFO_we <= '0';
				
		elsif state = st_FIFO_wr_en then    
				rejection_flag  <= '1';  
				ampl_FIFO_we <= '1';
				
		elsif state = st_waiting_endof_pulse then     
				rejection_flag  <= '1';  
				ampl_FIFO_we <= '0';	
      end if;
end process;
 
 
NEXT_STATES_DECODING: process (state,photon_arrival,count_PeakT,count_RejT, peak_time, pulse_time)
   begin

      case (state) is
         when st_reset =>
               next_state <= st_waiting_photon;

         when st_waiting_photon =>      
            if photon_arrival = '1' then
               next_state <= st_waiting_peak; 
				else
					next_state <= st_waiting_photon; 
            end if;
 
         when st_waiting_peak =>
            if (count_PeakT >= peak_time) then
				   next_state <= st_FIFO_wr_en;
				else 
					next_state <= st_waiting_peak;
            end if;
				
         when st_FIFO_wr_en =>
               next_state <= st_waiting_endof_pulse;

         when st_waiting_endof_pulse =>
				if count_RejT >= pulse_time then
               next_state <= st_waiting_photon;
				else
					next_state <= st_waiting_endof_pulse;
            end if;
 				
         when others =>
            next_state <= st_reset;
      end case;      
   end process;

end Behavioral;

