----------------------------------------------------------------------------------
-- Company: MLAB (ICTP-INFN)
-- Engineers: A. Cicuttin, M.L. Crespo
-- 
-- Create Date:    10:38:36 08/27/2015 
-- Design Name: 
-- Module Name:    fir_edge_detector_0 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
-- 		This module takes the derivative of a synchronous input data stream 
--			provided by the module "fir_derivative_0" and generates: 
--			1) the "arrival_flag" lasting on clock cycle each time data goes beyond "threshold_high"
--			2) the "rejection_flag" starting when data_in goes beyond "threshold_high" and ending when 
--				data_in goes below "threshold_low"
--			Two different threshold to provide some histeresis (kind off Schmidt triggering)
--      	If another photon arrives before going below threshold_low then it is not detected
--       3) The arrival_flag is finally delayed according to the latency of the Main FIR (93 cycles)
--			
-- Dependencies: Uses module "fir_derivative_0.vhd"
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use IEEE.std_logic_signed.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
-- library UNISIM;
-- use UNISIM.VComponents.all;

entity edge_detector_0 is
    Port ( 
			--Inputs
			  data_in : in  STD_LOGIC_VECTOR (15 downto 0); 
           clk : in  STD_LOGIC;
           reset : in  STD_LOGIC;
			--Parameters
           threshold_high : in  STD_LOGIC_VECTOR (15 downto 0); -- Absolute threshold to detect pulse arrival
			  threshold_low  : in  STD_LOGIC_VECTOR (15 downto 0); -- Absolute threshold to go to the "waiting_state" (it should be slightly lower than "threshold_high")
			--Outputs
           arrival_flag : out  STD_LOGIC;     -- it is rised only one clock pulse when "data_in" cross upwards "threshold" 
           sirio_reset_restoration: in  STD_LOGIC; --to avoid spurious photon detection due to sirio restoration positive slope
           rejection_flag : out  STD_LOGIC);  -- it is kept high while over threshold 

end edge_detector_0; 

architecture Behavioral of edge_detector_0 is

  -- For the finite state machine
  type state_type is (reset_state, waiting_state, output_pulse_state, over_threshold_state); 
  signal state, next_state : state_type; 
  signal shiftreg94 : std_logic_vector(137 downto 0);
  signal arrival_flag_int : std_logic;
--Declare internal signals for all outputs of the state-machine

--	signal Photon_arrival: std_logic;


begin 
 
--Updating states
   SYNC_PROC: process (clk,reset)
   begin
      if (clk'event and clk = '1') then
         if (reset = '0') then
            state <= reset_state;			
         else
            state <= next_state;
         end if;        
      end if;
   end process;
 
   --MOORE State-Machine - Outputs based on state only
   OUTPUT_DECODE: process (state)
   begin
      --insert statements to decode internal output signals
      --below is simple example
	
      if state = reset_state then
 				arrival_flag_int  <= '0';      
				rejection_flag  <= '0';  
			
		elsif state = waiting_state then
 				arrival_flag_int  <= '0';      
				rejection_flag  <= '0';  
					
		elsif state = output_pulse_state  then
 				arrival_flag_int  <= NOT sirio_reset_restoration;      
				rejection_flag  <= '1';  
				
		elsif state = over_threshold_state then
 				arrival_flag_int  <= '0';      
				rejection_flag  <= '1';  
      end if;
end process;
 
   NEXT_STATE_DECODE: process (state, data_in, threshold_low, threshold_high )
   begin
      --declare default state for next_state to avoid latches
      next_state <= state;  --default is to stay in current state
      --insert statements to decode next_state
      --below is a simple example

      case (state) is
         when reset_state =>
               next_state <= waiting_state;

         when waiting_state =>
            if data_in > threshold_high then
               next_state <= output_pulse_state; 
				else
					next_state <= waiting_state; 
            end if;
 
         when output_pulse_state =>
  				   next_state <= over_threshold_state; 
				
         when over_threshold_state =>
            if data_in < threshold_low then
               next_state <= waiting_state;
				else
					next_state <= over_threshold_state;
            end if;
				
         when others =>
            next_state <= reset_state;
      end case;      
   end process;


--Delaying  the "arrival_flag_int" the latency of the main FIR (138)
   Dealying_PROC: process (clk,reset)
   begin
      if (clk'event and clk = '1') then
         if (reset = '0') then
            shiftreg94 <= (others => '0');			
         else
            shiftreg94 <= shiftreg94(136 downto 0) & arrival_flag_int ;
         end if;        
      end if;
   end process; 
	
arrival_flag <= shiftreg94(137);

end Behavioral;

