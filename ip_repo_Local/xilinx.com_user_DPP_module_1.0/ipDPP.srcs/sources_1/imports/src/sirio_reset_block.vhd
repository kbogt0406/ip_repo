----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    18:38:11 09/13/2015 
-- Design Name: 
-- Module Name:    sirio_resets_counter - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
use IEEE.std_logic_unsigned.all;

entity sirio_reset_block is
    Port ( ADC_data : in  STD_LOGIC_VECTOR (11 downto 0);
           sirio_reset : out  STD_LOGIC;
           sirio_reset_restoration: out  STD_LOGIC; --to avoid false photon detection du to sirio reset restoration (spurious positive pulse)
           param_sirio_reset_threshold : in  STD_LOGIC_VECTOR (31 downto 0);
			  param_sirio_reset_time  : in  STD_LOGIC_VECTOR (31 downto 0); --waiting time to complete reset
			  param_sirio_restoration_time : in  STD_LOGIC_VECTOR (31 downto 0); --waiting time for complete restoration
           sirio_resets_counter_out : out  STD_LOGIC_VECTOR (31 downto 0);
           clock : in  STD_LOGIC;
           reset : in  STD_LOGIC);
end sirio_reset_block;

architecture Behavioral of sirio_reset_block is

type state_type is (st_reset, st_ini, st_under_treshold, st_increment, st_reseting, st_waiting_restoration); 
signal state, next_state : state_type; 

signal sirio_reseting_time_counter: std_logic_vector(31 downto 0); -- Sirio "Reseting Time" counter
signal sirio_restoration_time_counter: std_logic_vector(31 downto 0); -- Sirio "Restoration Time" counter
signal sirio_resets_counter: std_logic_vector(31 downto 0); 

signal overthreshold, ovt_1, ovt_2, ovt_3: std_logic ;

begin

sirio_resets_counter_out <= sirio_resets_counter; -- to avoid inout or buffer

SYNC_PROC: process (clock, reset)
   begin
      if (clock'event and clock = '1') then
         if (reset = '0') then
            state <= st_reset;
         else
            state <= next_state;
         end if;    
      end if;
   end process;
	
-- Updating Counters 

SYNC_PROC_COUNTERS: process (clock, reset)
	begin
      if (clock'event and clock = '1') then
         if (reset = '0') then
				sirio_resets_counter <= (others => '0'); --
				sirio_reseting_time_counter <= (others => '0'); -- To count up to "param_sirio_reset_time"
				sirio_restoration_time_counter <= (others => '0'); -- To count up to "param_sirio_restoration_time"
 
			elsif state = st_reset then
                    sirio_reseting_time_counter <= (others => '0'); -- To count up to "param_sirio_reset_time"
                    sirio_restoration_time_counter <= (others => '0'); -- To count up to "param_sirio_restoration_time"    

			elsif state = st_ini then
				sirio_reseting_time_counter <= (others => '0'); -- To count up to "param_sirio_reset_time"
				sirio_restoration_time_counter <= (others => '0'); -- To count up to "param_sirio_restoration_time"				

			elsif state = st_under_treshold then
				sirio_reseting_time_counter <= (others => '0'); -- To count up to "param_sirio_reset_time"
				sirio_restoration_time_counter <= (others => '0'); -- To count up to "param_sirio_restoration_time"

			elsif state = st_increment then
				sirio_resets_counter <= sirio_resets_counter + 1;
		  		sirio_reseting_time_counter <= (others => '0'); -- To count up to "param_sirio_reset_time"
				sirio_restoration_time_counter <= (others => '0'); -- To count up to "param_sirio_restoration_time"				

			elsif state = st_reseting then
				sirio_reseting_time_counter <= sirio_reseting_time_counter + 1;	
			
			elsif state = st_waiting_restoration then
				sirio_restoration_time_counter <= sirio_restoration_time_counter + 1;
			end if;
      end if;    
   end process;


   OUTPUT_DECODE: process (state) 
   begin
       
		if 	state = st_reset then
			sirio_reset <= '1';		
			sirio_reset_restoration <= '0';
		elsif state = st_ini then
			sirio_reset <= '0'; 
			sirio_reset_restoration <= '0';
		elsif state = st_under_treshold then
			sirio_reset <= '0';	
			sirio_reset_restoration <= '0';			
		elsif state = st_increment then
			sirio_reset <= '0';	  
			sirio_reset_restoration <= '0';			
		elsif state = st_reseting then
			sirio_reset <= '1';		
			sirio_reset_restoration <= '1';			
		elsif state = st_waiting_restoration then
			sirio_reset <= '0';
			sirio_reset_restoration <= '1';			
		end if;	
		
   end process;
 
-- 
NEXT_STATE_DECODE: process (state, ADC_data, param_sirio_reset_threshold, sirio_reseting_time_counter, 
	overthreshold, param_sirio_reset_time, sirio_restoration_time_counter, param_sirio_restoration_time)
   begin
      case (state) is

         when st_reset=>
               next_state <= st_ini;

         when st_ini=>
               next_state <= st_under_treshold;

         when st_under_treshold =>
           -- if ( ADC_data > param_sirio_reset_threshold(11 downto 0) ) then
			   if ( overthreshold = '1') then --Redundant Over Threshold Condition			
               next_state <= st_increment;
				else
	            next_state <= st_under_treshold;				
            end if;

         when st_increment =>
					next_state <= st_reseting;
			
			when st_reseting =>		
				if (sirio_reseting_time_counter = param_sirio_reset_time) then
					next_state <= st_waiting_restoration;
				else
	            next_state <= st_reseting;	
            end if;

			when st_waiting_restoration =>		
				if (sirio_restoration_time_counter = param_sirio_restoration_time) then
					next_state <= st_ini;
				else
	            next_state <= st_waiting_restoration;	
            end if;
				
         when others =>
            next_state <=st_ini;      
				
		end case;      
   end process;


OVER_THRESHOLD_DETECTION: process (clock, reset, ADC_data, param_sirio_reset_threshold)
   begin
      if (clock'event and clock = '1') then
         if (reset = '0') then
            overthreshold <= '0';
				ovt_1 <= '0';
				ovt_2 <= '0';
				ovt_3 <= '0';
         else
				if (ADC_data > param_sirio_reset_threshold(11 downto 0)) then 
					ovt_1 <= '1';
				else 
					ovt_1 <= '0';
				end if;
				
				ovt_2 <= ovt_1;
				ovt_3 <= ovt_2;
            overthreshold <= ovt_1 AND ovt_2 AND ovt_2 ;
         end if;    
      end if;
   end process;

end Behavioral;

