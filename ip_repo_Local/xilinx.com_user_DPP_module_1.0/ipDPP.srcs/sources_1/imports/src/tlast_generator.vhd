----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/22/2015 10:42:16 AM
-- Design Name: 
-- Module Name: tlast_generator - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity tlast_generator is
    Port ( clk : in STD_LOGIC;
           rst : in STD_LOGIC;
           packet_size : in STD_LOGIC_VECTOR (31 downto 0);
           fifo_tready : in STD_LOGIC;
           wr_en : in STD_LOGIC;
           tlast : out STD_LOGIC);
end tlast_generator;

architecture Behavioral of tlast_generator is

signal packet_counter : unsigned(31 downto 0);

begin

process( clk,rst,fifo_tready,wr_en ) is
        begin
         if (rising_edge (clk)) then
            if ( rst = '0' ) then
                packet_counter  <= x"00000000";
            else
              if (fifo_tready = '1' and wr_en = '1') then
                if (packet_counter = unsigned('0'&packet_size(31 downto 1))) then       -- logical shift right 1 bit (divide by 2)
                    packet_counter  <= x"00000001";
                else    
                    packet_counter <= packet_counter+1;
                end if;
              end if;   
            end if;
         end if;
end process;

tlast <= '1' when (packet_counter = unsigned('0'&packet_size(31 downto 1))-1) else '0';

end Behavioral;
