----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/02/2018 02:48:38 AM
-- Design Name: 
-- Module Name: Top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Top is
    Generic( addr_bits : integer :=12;
             latency : integer :=2;
             data_bits : integer :=16;
             en_cond : std_logic:='1');
    Port ( clk : in std_logic;
           rst : in std_logic;
           en : in std_logic;
    
           in_addr : in STD_LOGIC_VECTOR (addr_bits-1 downto 0);
--           last_addr : in STD_LOGIC_VECTOR (addr_bits-1 downto 0);

           adda : out std_logic_vector (addr_bits -1 downto 0);
           addb : out std_logic_vector (addr_bits -1 downto 0);
           
           douta : in std_logic_vector (data_bits-1 downto 0);
           doutb : in std_logic_vector (data_bits-1 downto 0);
           
           dina : out std_logic_vector (data_bits-1 downto 0);
           dinb : out std_logic_vector (data_bits-1 downto 0);
                      
           out_addr : out std_logic_vector (addr_bits -1 downto 0);
           count : out std_logic_vector(data_bits -1 downto 0);
           rdy : out std_logic;
           
           clk_out : out std_logic;
           
           
           ena : out STD_LOGIC;
           enb : out STD_LOGIC;
           wea : out STD_LOGIC;
           web : out STD_LOGIC);
end Top;

architecture Behavioral of Top is
signal out_cond : std_logic;
signal rd_count : integer range 0 to latency;
signal c0, c1, c2 : std_logic_vector(data_bits -1 downto 0);

type addr_buffer is array (latency downto 0) of std_logic_vector(addr_bits -1 downto 0);
signal adbuf : addr_buffer;


type states is (not_enable, normal, colision, avoidance);
signal curr_state, next_state : states;
begin


process(clk, rst)
begin
--    if en /= en_cond then
   if rst='1' then
          adbuf <= (others=>(others=>'1'));
          rd_count<=0;
          curr_state<=not_enable;
    elsif rising_edge(clk) then
          adbuf(0)<=in_addr;
          if rd_count<latency then
            rd_count<=rd_count+1;
          end if;
          for i in 1 to latency loop
            adbuf(i)<=adbuf(i-1);          
          end loop;
         curr_state<=next_state;
    end if;
end process;


process(curr_state, adbuf, en)
begin
    case curr_state is 
        when not_enable =>
            ena <='0';
            enb <='0';
            wea <= '0';
            web <= '0';
            
            count<=c0;
            dinb<=c0;
            if en=en_cond then
                next_state<=normal;
            else
                next_state<=not_enable;
            end if;
        when normal =>
            ena <='1';
            enb <='1';
            wea <= '0';
            web <= '1';
            
            count<=c1;
            dinb<=c1;
            if en = not en_cond then
                next_state<=not_enable;
            elsif adbuf(0)=adbuf(1) then
                next_state<=colision;
            else    
                next_state<=normal;
            end if;        
            
        when colision=> 
            ena <='1';
            enb <='1';
            wea <= '0';
            web <= '1';
            
            count<=c1;
            dinb<=c2;
            
            next_state<=avoidance;
        when avoidance=>
            ena <='1';
            enb <='1';
            wea <= '0';
            web <= '0';
            
            count<=c2;
            dinb<=(others=>'0');
            if en = not en_cond then
                next_state<=not_enable;
            elsif adbuf(0)=adbuf(1) then
                next_state<=colision;
            else    
                next_state<=normal;
            end if;    
        when others => 
            next_state<=not_enable;

    end case;
end process;


c0<=std_logic_vector(unsigned(douta));
c1<=std_logic_vector(unsigned(douta)+1);
c2<=std_logic_vector(unsigned(douta)+2);

dina<=c1;

out_addr<=adbuf(latency);

adda<=adbuf(0);
addb<=adbuf(latency);


clk_out<= clk;

rdy<='1' when rd_count=latency else '0';
 
end Behavioral;
