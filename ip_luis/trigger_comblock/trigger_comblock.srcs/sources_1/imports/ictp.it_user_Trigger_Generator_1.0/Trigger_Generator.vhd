----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/08/2017 01:43:17 PM
-- Design Name: 
-- Module Name: Trigger_Generator - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code. PRIMITIVES INSTANTIATION LIBRARY
--library UNISIM;
--use UNISIM.VComponents.all;


entity Trigger_Generator is
    Generic( Din_WIDTH : integer := 16);
    Port (  pl_clk     : in STD_LOGIC;
--            DMA_clk    : in STD_LOGIC;
            
--            m_clk      : out STD_LOGIC;
            
            resetn  : in STD_LOGIC;
            
            din        : in STD_LOGIC_VECTOR (Din_WIDTH - 1 downto 0);
            din_valid  : in STD_LOGIC;
                                                    ----- 8/06/2017
            fifo_full   : in STD_LOGIC;
            fifo_empty  : in STD_LOGIC;
            fifo_we             : out STD_LOGIC;
            fifo_re  : out STD_LOGIC;
            
            --- registers
            trig_edge_in    : in STD_LOGIC;                -- 1- rising edge , 0- falling edge
            auto_trigger    : in STD_LOGIC;
            trig_level_in   : in STD_LOGIC_VECTOR (Din_WIDTH - 1 downto 0);
            trig_delay_in   : in std_logic_vector(Din_WIDTH-1 downto 0);
            trig_out        : out STD_LOGIC
            
            -- 24/05/2018

           );
end Trigger_Generator;

architecture Behavioral of Trigger_Generator is

        component Trigger_m4 is
        generic ( Trig_data_width : integer    := 16);
        Port ( clk : in STD_LOGIC;
               resetn : in STD_LOGIC;
               din : in STD_LOGIC_VECTOR (Trig_data_width - 1 downto 0);
               data_valid   : in STD_LOGIC;                                        ----- 8/06/2017
               trig_level : in STD_LOGIC_VECTOR (Trig_data_width - 1 downto 0);
               trig_edge_rising : in STD_LOGIC;
               trig : out STD_LOGIC;
               delta      : in STD_LOGIC_VECTOR (Trig_data_width - 1 downto 0);
               fifo_full   : in STD_LOGIC;
               fifo_empty   : in STD_LOGIC;
               fifo_re  : out STD_LOGIC;
               count_val  : out STD_LOGIC_VECTOR (Trig_data_width - 1 downto 0);
               trig_real : out STD_LOGIC;
               
               auto_trigger : in STD_LOGIC; --31/05/2018
               
               fifo_we : out STD_LOGIC);
    end component;

--    signal dma_tready   : STD_LOGIC;
signal trigger :STD_LOGIC;

begin

    Trig_Gen : Trigger_m4
    generic map( Trig_data_width => Din_WIDTH)
    Port map( 
        auto_trigger => auto_trigger,
        clk => pl_clk,
        resetn => resetn,
        din => din,
        data_valid => din_valid,                                       ----- 8/06/2017
        trig_level => trig_level_in,
        trig_edge_rising => trig_edge_in,
        delta => trig_delay_in,
        fifo_full => fifo_full,
        fifo_empty => fifo_empty,
        fifo_re => fifo_re,
        trig => trigger,
        count_val => open,
        trig_real => open,
        fifo_we => fifo_we
    );


trig_out<=trigger;

--BUFGMUX_inst : BUFGMUX
--port map (
--O => m_clk, -- 1-bit output: Clock output
--I0 => pl_clk, -- 1-bit input: Clock input (S=0)
--I1 => DMA_clk, -- 1-bit input: Clock input (S=1)
--S => trigger -- 1-bit input: Clock select
--);

end Behavioral;
