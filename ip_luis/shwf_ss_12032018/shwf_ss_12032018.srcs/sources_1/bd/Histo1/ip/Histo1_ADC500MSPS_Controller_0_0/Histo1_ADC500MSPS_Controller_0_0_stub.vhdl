-- Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2017.4 (win64) Build 2086221 Fri Dec 15 20:55:39 MST 2017
-- Date        : Wed Feb  7 13:10:30 2018
-- Host        : HP6-MLAB-9 running 64-bit Service Pack 1  (build 7601)
-- Command     : write_vhdl -force -mode synth_stub
--               c:/Users/infolab/Desktop/my_verilog/histo_w_comblock/project_1/project_1.srcs/sources_1/bd/Histo1/ip/Histo1_ADC500MSPS_Controller_0_0/Histo1_ADC500MSPS_Controller_0_0_stub.vhdl
-- Design      : Histo1_ADC500MSPS_Controller_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Histo1_ADC500MSPS_Controller_0_0 is
  Port ( 
    ADC_Dout_2Sample : out STD_LOGIC_VECTOR ( 15 downto 0 );
    ADC_Dout_for_PL : out STD_LOGIC_VECTOR ( 31 downto 0 );
    ADC_Dout_valid : out STD_LOGIC;
    ADC_clk_for_PL : out STD_LOGIC_VECTOR ( 0 to 0 );
    Ctrl_reg0_in : in STD_LOGIC_VECTOR ( 8 downto 0 );
    Ctrl_reg1_out : out STD_LOGIC_VECTOR ( 1 downto 0 );
    Data_from_ADC_N : in STD_LOGIC_VECTOR ( 15 downto 0 );
    Data_from_ADC_P : in STD_LOGIC_VECTOR ( 15 downto 0 );
    clk_from_ADC_N : in STD_LOGIC_VECTOR ( 0 to 0 );
    clk_from_ADC_P : in STD_LOGIC_VECTOR ( 0 to 0 );
    clk_to_ADC_N : out STD_LOGIC_VECTOR ( 0 to 0 );
    clk_to_ADC_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    clock : in STD_LOGIC;
    from_ADC_OR_N : in STD_LOGIC_VECTOR ( 0 to 0 );
    from_ADC_OR_P : in STD_LOGIC_VECTOR ( 0 to 0 );
    from_adc_calrun_fmc : in STD_LOGIC;
    reset : in STD_LOGIC;
    to_adc_cal_fmc : out STD_LOGIC;
    to_adc_caldly_nscs_fmc : out STD_LOGIC;
    to_adc_dclk_rst_fmc : out STD_LOGIC;
    to_adc_fsr_ece_fmc : out STD_LOGIC;
    to_adc_led_0 : out STD_LOGIC;
    to_adc_led_1 : out STD_LOGIC;
    to_adc_outedge_ddr_sdata_fmc : out STD_LOGIC;
    to_adc_outv_slck_fmc : out STD_LOGIC;
    to_adc_pd_fmc : out STD_LOGIC
  );

end Histo1_ADC500MSPS_Controller_0_0;

architecture stub of Histo1_ADC500MSPS_Controller_0_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "ADC_Dout_2Sample[15:0],ADC_Dout_for_PL[31:0],ADC_Dout_valid,ADC_clk_for_PL[0:0],Ctrl_reg0_in[8:0],Ctrl_reg1_out[1:0],Data_from_ADC_N[15:0],Data_from_ADC_P[15:0],clk_from_ADC_N[0:0],clk_from_ADC_P[0:0],clk_to_ADC_N[0:0],clk_to_ADC_P[0:0],clock,from_ADC_OR_N[0:0],from_ADC_OR_P[0:0],from_adc_calrun_fmc,reset,to_adc_cal_fmc,to_adc_caldly_nscs_fmc,to_adc_dclk_rst_fmc,to_adc_fsr_ece_fmc,to_adc_led_0,to_adc_led_1,to_adc_outedge_ddr_sdata_fmc,to_adc_outv_slck_fmc,to_adc_pd_fmc";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "design_1_adc,Vivado 2017.4";
begin
end;
