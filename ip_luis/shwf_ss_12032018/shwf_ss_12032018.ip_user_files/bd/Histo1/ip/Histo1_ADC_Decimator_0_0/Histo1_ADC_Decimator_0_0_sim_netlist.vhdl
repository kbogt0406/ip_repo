-- Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2017.4 (win64) Build 2086221 Fri Dec 15 20:55:39 MST 2017
-- Date        : Wed Feb  7 13:10:30 2018
-- Host        : HP6-MLAB-9 running 64-bit Service Pack 1  (build 7601)
-- Command     : write_vhdl -force -mode funcsim
--               c:/Users/infolab/Desktop/my_verilog/histo_w_comblock/project_1/project_1.srcs/sources_1/bd/Histo1/ip/Histo1_ADC_Decimator_0_0/Histo1_ADC_Decimator_0_0_sim_netlist.vhdl
-- Design      : Histo1_ADC_Decimator_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity Histo1_ADC_Decimator_0_0_Decimate_N is
  port (
    Decim_data_valid : out STD_LOGIC;
    Dout : out STD_LOGIC_VECTOR ( 15 downto 0 );
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    D_sum : out STD_LOGIC_VECTOR ( 15 downto 0 );
    clk_in : in STD_LOGIC;
    resetn : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 8 downto 0 );
    N : in STD_LOGIC_VECTOR ( 14 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of Histo1_ADC_Decimator_0_0_Decimate_N : entity is "Decimate_N";
end Histo1_ADC_Decimator_0_0_Decimate_N;

architecture STRUCTURE of Histo1_ADC_Decimator_0_0_Decimate_N is
  signal \D_sum[15]_i_2_n_0\ : STD_LOGIC;
  signal \^decim_data_valid\ : STD_LOGIC;
  signal Decim_data_valid_i_1_n_0 : STD_LOGIC;
  signal \^dout\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \^sr\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \cnt[0]_i_1_n_0\ : STD_LOGIC;
  signal \cnt[0]_i_3_n_0\ : STD_LOGIC;
  signal cnt_reg : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \cnt_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \cnt_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \cnt_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \cnt_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \cnt_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \cnt_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \cnt_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \cnt_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \cnt_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \cnt_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \cnt_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal eqOp : STD_LOGIC;
  signal \eqOp_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \eqOp_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \eqOp_carry__0_n_2\ : STD_LOGIC;
  signal \eqOp_carry__0_n_3\ : STD_LOGIC;
  signal eqOp_carry_i_1_n_0 : STD_LOGIC;
  signal eqOp_carry_i_2_n_0 : STD_LOGIC;
  signal eqOp_carry_i_3_n_0 : STD_LOGIC;
  signal eqOp_carry_i_4_n_0 : STD_LOGIC;
  signal eqOp_carry_n_0 : STD_LOGIC;
  signal eqOp_carry_n_1 : STD_LOGIC;
  signal eqOp_carry_n_2 : STD_LOGIC;
  signal eqOp_carry_n_3 : STD_LOGIC;
  signal minusOp : STD_LOGIC_VECTOR ( 14 downto 1 );
  signal \minusOp_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \minusOp_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \minusOp_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \minusOp_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \minusOp_carry__0_n_0\ : STD_LOGIC;
  signal \minusOp_carry__0_n_1\ : STD_LOGIC;
  signal \minusOp_carry__0_n_2\ : STD_LOGIC;
  signal \minusOp_carry__0_n_3\ : STD_LOGIC;
  signal \minusOp_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \minusOp_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \minusOp_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \minusOp_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \minusOp_carry__1_n_0\ : STD_LOGIC;
  signal \minusOp_carry__1_n_1\ : STD_LOGIC;
  signal \minusOp_carry__1_n_2\ : STD_LOGIC;
  signal \minusOp_carry__1_n_3\ : STD_LOGIC;
  signal \minusOp_carry__2_i_1_n_0\ : STD_LOGIC;
  signal \minusOp_carry__2_i_2_n_0\ : STD_LOGIC;
  signal \minusOp_carry__2_n_1\ : STD_LOGIC;
  signal \minusOp_carry__2_n_3\ : STD_LOGIC;
  signal minusOp_carry_i_1_n_0 : STD_LOGIC;
  signal minusOp_carry_i_2_n_0 : STD_LOGIC;
  signal minusOp_carry_i_3_n_0 : STD_LOGIC;
  signal minusOp_carry_i_4_n_0 : STD_LOGIC;
  signal minusOp_carry_n_0 : STD_LOGIC;
  signal minusOp_carry_n_1 : STD_LOGIC;
  signal minusOp_carry_n_2 : STD_LOGIC;
  signal minusOp_carry_n_3 : STD_LOGIC;
  signal \tmp_sum[11]_i_2_n_0\ : STD_LOGIC;
  signal \tmp_sum[11]_i_3_n_0\ : STD_LOGIC;
  signal \tmp_sum[11]_i_4_n_0\ : STD_LOGIC;
  signal \tmp_sum[11]_i_5_n_0\ : STD_LOGIC;
  signal \tmp_sum[11]_i_6_n_0\ : STD_LOGIC;
  signal \tmp_sum[15]_i_10_n_0\ : STD_LOGIC;
  signal \tmp_sum[15]_i_1_n_0\ : STD_LOGIC;
  signal \tmp_sum[15]_i_3_n_0\ : STD_LOGIC;
  signal \tmp_sum[15]_i_4_n_0\ : STD_LOGIC;
  signal \tmp_sum[15]_i_5_n_0\ : STD_LOGIC;
  signal \tmp_sum[15]_i_6_n_0\ : STD_LOGIC;
  signal \tmp_sum[15]_i_7_n_0\ : STD_LOGIC;
  signal \tmp_sum[15]_i_8_n_0\ : STD_LOGIC;
  signal \tmp_sum[15]_i_9_n_0\ : STD_LOGIC;
  signal \tmp_sum[3]_i_2_n_0\ : STD_LOGIC;
  signal \tmp_sum[3]_i_3_n_0\ : STD_LOGIC;
  signal \tmp_sum[3]_i_4_n_0\ : STD_LOGIC;
  signal \tmp_sum[3]_i_5_n_0\ : STD_LOGIC;
  signal \tmp_sum[3]_i_6_n_0\ : STD_LOGIC;
  signal \tmp_sum[3]_i_7_n_0\ : STD_LOGIC;
  signal \tmp_sum[3]_i_8_n_0\ : STD_LOGIC;
  signal \tmp_sum[3]_i_9_n_0\ : STD_LOGIC;
  signal \tmp_sum[7]_i_2_n_0\ : STD_LOGIC;
  signal \tmp_sum[7]_i_3_n_0\ : STD_LOGIC;
  signal \tmp_sum[7]_i_4_n_0\ : STD_LOGIC;
  signal \tmp_sum[7]_i_5_n_0\ : STD_LOGIC;
  signal \tmp_sum[7]_i_6_n_0\ : STD_LOGIC;
  signal \tmp_sum[7]_i_7_n_0\ : STD_LOGIC;
  signal \tmp_sum[7]_i_8_n_0\ : STD_LOGIC;
  signal \tmp_sum[7]_i_9_n_0\ : STD_LOGIC;
  signal \tmp_sum_reg[11]_i_1_n_0\ : STD_LOGIC;
  signal \tmp_sum_reg[11]_i_1_n_1\ : STD_LOGIC;
  signal \tmp_sum_reg[11]_i_1_n_2\ : STD_LOGIC;
  signal \tmp_sum_reg[11]_i_1_n_3\ : STD_LOGIC;
  signal \tmp_sum_reg[11]_i_1_n_4\ : STD_LOGIC;
  signal \tmp_sum_reg[11]_i_1_n_5\ : STD_LOGIC;
  signal \tmp_sum_reg[11]_i_1_n_6\ : STD_LOGIC;
  signal \tmp_sum_reg[11]_i_1_n_7\ : STD_LOGIC;
  signal \tmp_sum_reg[15]_i_2_n_1\ : STD_LOGIC;
  signal \tmp_sum_reg[15]_i_2_n_2\ : STD_LOGIC;
  signal \tmp_sum_reg[15]_i_2_n_3\ : STD_LOGIC;
  signal \tmp_sum_reg[15]_i_2_n_4\ : STD_LOGIC;
  signal \tmp_sum_reg[15]_i_2_n_5\ : STD_LOGIC;
  signal \tmp_sum_reg[15]_i_2_n_6\ : STD_LOGIC;
  signal \tmp_sum_reg[15]_i_2_n_7\ : STD_LOGIC;
  signal \tmp_sum_reg[3]_i_1_n_0\ : STD_LOGIC;
  signal \tmp_sum_reg[3]_i_1_n_1\ : STD_LOGIC;
  signal \tmp_sum_reg[3]_i_1_n_2\ : STD_LOGIC;
  signal \tmp_sum_reg[3]_i_1_n_3\ : STD_LOGIC;
  signal \tmp_sum_reg[3]_i_1_n_4\ : STD_LOGIC;
  signal \tmp_sum_reg[3]_i_1_n_5\ : STD_LOGIC;
  signal \tmp_sum_reg[3]_i_1_n_6\ : STD_LOGIC;
  signal \tmp_sum_reg[3]_i_1_n_7\ : STD_LOGIC;
  signal \tmp_sum_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal \tmp_sum_reg[7]_i_1_n_1\ : STD_LOGIC;
  signal \tmp_sum_reg[7]_i_1_n_2\ : STD_LOGIC;
  signal \tmp_sum_reg[7]_i_1_n_3\ : STD_LOGIC;
  signal \tmp_sum_reg[7]_i_1_n_4\ : STD_LOGIC;
  signal \tmp_sum_reg[7]_i_1_n_5\ : STD_LOGIC;
  signal \tmp_sum_reg[7]_i_1_n_6\ : STD_LOGIC;
  signal \tmp_sum_reg[7]_i_1_n_7\ : STD_LOGIC;
  signal \NLW_cnt_reg[12]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal NLW_eqOp_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_eqOp_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_eqOp_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_minusOp_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_minusOp_carry__2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_tmp_sum_reg[15]_i_2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \D_sum[15]_i_2\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \tmp_sum[15]_i_3\ : label is "soft_lutpair0";
begin
  Decim_data_valid <= \^decim_data_valid\;
  Dout(15 downto 0) <= \^dout\(15 downto 0);
  SR(0) <= \^sr\(0);
\D_sum[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => \tmp_sum[15]_i_6_n_0\,
      I1 => cnt_reg(14),
      I2 => cnt_reg(15),
      I3 => cnt_reg(13),
      I4 => cnt_reg(12),
      I5 => \D_sum[15]_i_2_n_0\,
      O => eqOp
    );
\D_sum[15]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFEFFFF"
    )
        port map (
      I0 => cnt_reg(0),
      I1 => cnt_reg(1),
      I2 => cnt_reg(2),
      I3 => cnt_reg(3),
      I4 => \tmp_sum[15]_i_4_n_0\,
      O => \D_sum[15]_i_2_n_0\
    );
\D_sum_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk_in,
      CE => eqOp,
      D => \^dout\(0),
      Q => D_sum(0),
      R => \^sr\(0)
    );
\D_sum_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk_in,
      CE => eqOp,
      D => \^dout\(10),
      Q => D_sum(10),
      R => \^sr\(0)
    );
\D_sum_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk_in,
      CE => eqOp,
      D => \^dout\(11),
      Q => D_sum(11),
      R => \^sr\(0)
    );
\D_sum_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk_in,
      CE => eqOp,
      D => \^dout\(12),
      Q => D_sum(12),
      R => \^sr\(0)
    );
\D_sum_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk_in,
      CE => eqOp,
      D => \^dout\(13),
      Q => D_sum(13),
      R => \^sr\(0)
    );
\D_sum_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk_in,
      CE => eqOp,
      D => \^dout\(14),
      Q => D_sum(14),
      R => \^sr\(0)
    );
\D_sum_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk_in,
      CE => eqOp,
      D => \^dout\(15),
      Q => D_sum(15),
      R => \^sr\(0)
    );
\D_sum_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk_in,
      CE => eqOp,
      D => \^dout\(1),
      Q => D_sum(1),
      R => \^sr\(0)
    );
\D_sum_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk_in,
      CE => eqOp,
      D => \^dout\(2),
      Q => D_sum(2),
      R => \^sr\(0)
    );
\D_sum_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk_in,
      CE => eqOp,
      D => \^dout\(3),
      Q => D_sum(3),
      R => \^sr\(0)
    );
\D_sum_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk_in,
      CE => eqOp,
      D => \^dout\(4),
      Q => D_sum(4),
      R => \^sr\(0)
    );
\D_sum_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk_in,
      CE => eqOp,
      D => \^dout\(5),
      Q => D_sum(5),
      R => \^sr\(0)
    );
\D_sum_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk_in,
      CE => eqOp,
      D => \^dout\(6),
      Q => D_sum(6),
      R => \^sr\(0)
    );
\D_sum_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk_in,
      CE => eqOp,
      D => \^dout\(7),
      Q => D_sum(7),
      R => \^sr\(0)
    );
\D_sum_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk_in,
      CE => eqOp,
      D => \^dout\(8),
      Q => D_sum(8),
      R => \^sr\(0)
    );
\D_sum_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk_in,
      CE => eqOp,
      D => \^dout\(9),
      Q => D_sum(9),
      R => \^sr\(0)
    );
Decim_data_valid_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4000FFFF40000000"
    )
        port map (
      I0 => \tmp_sum[15]_i_6_n_0\,
      I1 => \tmp_sum[15]_i_5_n_0\,
      I2 => \tmp_sum[15]_i_4_n_0\,
      I3 => \tmp_sum[15]_i_3_n_0\,
      I4 => resetn,
      I5 => \^decim_data_valid\,
      O => Decim_data_valid_i_1_n_0
    );
Decim_data_valid_reg: unisim.vcomponents.FDRE
     port map (
      C => clk_in,
      CE => '1',
      D => Decim_data_valid_i_1_n_0,
      Q => \^decim_data_valid\,
      R => '0'
    );
\cnt[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \eqOp_carry__0_n_2\,
      I1 => resetn,
      O => \cnt[0]_i_1_n_0\
    );
\cnt[0]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_reg(0),
      O => \cnt[0]_i_3_n_0\
    );
\cnt_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => \cnt_reg[0]_i_2_n_7\,
      Q => cnt_reg(0),
      R => \cnt[0]_i_1_n_0\
    );
\cnt_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \cnt_reg[0]_i_2_n_0\,
      CO(2) => \cnt_reg[0]_i_2_n_1\,
      CO(1) => \cnt_reg[0]_i_2_n_2\,
      CO(0) => \cnt_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \cnt_reg[0]_i_2_n_4\,
      O(2) => \cnt_reg[0]_i_2_n_5\,
      O(1) => \cnt_reg[0]_i_2_n_6\,
      O(0) => \cnt_reg[0]_i_2_n_7\,
      S(3 downto 1) => cnt_reg(3 downto 1),
      S(0) => \cnt[0]_i_3_n_0\
    );
\cnt_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => \cnt_reg[8]_i_1_n_5\,
      Q => cnt_reg(10),
      R => \cnt[0]_i_1_n_0\
    );
\cnt_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => \cnt_reg[8]_i_1_n_4\,
      Q => cnt_reg(11),
      R => \cnt[0]_i_1_n_0\
    );
\cnt_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => \cnt_reg[12]_i_1_n_7\,
      Q => cnt_reg(12),
      R => \cnt[0]_i_1_n_0\
    );
\cnt_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_reg[8]_i_1_n_0\,
      CO(3) => \NLW_cnt_reg[12]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \cnt_reg[12]_i_1_n_1\,
      CO(1) => \cnt_reg[12]_i_1_n_2\,
      CO(0) => \cnt_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_reg[12]_i_1_n_4\,
      O(2) => \cnt_reg[12]_i_1_n_5\,
      O(1) => \cnt_reg[12]_i_1_n_6\,
      O(0) => \cnt_reg[12]_i_1_n_7\,
      S(3 downto 0) => cnt_reg(15 downto 12)
    );
\cnt_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => \cnt_reg[12]_i_1_n_6\,
      Q => cnt_reg(13),
      R => \cnt[0]_i_1_n_0\
    );
\cnt_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => \cnt_reg[12]_i_1_n_5\,
      Q => cnt_reg(14),
      R => \cnt[0]_i_1_n_0\
    );
\cnt_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => \cnt_reg[12]_i_1_n_4\,
      Q => cnt_reg(15),
      R => \cnt[0]_i_1_n_0\
    );
\cnt_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => \cnt_reg[0]_i_2_n_6\,
      Q => cnt_reg(1),
      R => \cnt[0]_i_1_n_0\
    );
\cnt_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => \cnt_reg[0]_i_2_n_5\,
      Q => cnt_reg(2),
      R => \cnt[0]_i_1_n_0\
    );
\cnt_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => \cnt_reg[0]_i_2_n_4\,
      Q => cnt_reg(3),
      R => \cnt[0]_i_1_n_0\
    );
\cnt_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => \cnt_reg[4]_i_1_n_7\,
      Q => cnt_reg(4),
      R => \cnt[0]_i_1_n_0\
    );
\cnt_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_reg[0]_i_2_n_0\,
      CO(3) => \cnt_reg[4]_i_1_n_0\,
      CO(2) => \cnt_reg[4]_i_1_n_1\,
      CO(1) => \cnt_reg[4]_i_1_n_2\,
      CO(0) => \cnt_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_reg[4]_i_1_n_4\,
      O(2) => \cnt_reg[4]_i_1_n_5\,
      O(1) => \cnt_reg[4]_i_1_n_6\,
      O(0) => \cnt_reg[4]_i_1_n_7\,
      S(3 downto 0) => cnt_reg(7 downto 4)
    );
\cnt_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => \cnt_reg[4]_i_1_n_6\,
      Q => cnt_reg(5),
      R => \cnt[0]_i_1_n_0\
    );
\cnt_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => \cnt_reg[4]_i_1_n_5\,
      Q => cnt_reg(6),
      R => \cnt[0]_i_1_n_0\
    );
\cnt_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => \cnt_reg[4]_i_1_n_4\,
      Q => cnt_reg(7),
      R => \cnt[0]_i_1_n_0\
    );
\cnt_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => \cnt_reg[8]_i_1_n_7\,
      Q => cnt_reg(8),
      R => \cnt[0]_i_1_n_0\
    );
\cnt_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_reg[4]_i_1_n_0\,
      CO(3) => \cnt_reg[8]_i_1_n_0\,
      CO(2) => \cnt_reg[8]_i_1_n_1\,
      CO(1) => \cnt_reg[8]_i_1_n_2\,
      CO(0) => \cnt_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_reg[8]_i_1_n_4\,
      O(2) => \cnt_reg[8]_i_1_n_5\,
      O(1) => \cnt_reg[8]_i_1_n_6\,
      O(0) => \cnt_reg[8]_i_1_n_7\,
      S(3 downto 0) => cnt_reg(11 downto 8)
    );
\cnt_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => \cnt_reg[8]_i_1_n_6\,
      Q => cnt_reg(9),
      R => \cnt[0]_i_1_n_0\
    );
eqOp_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => eqOp_carry_n_0,
      CO(2) => eqOp_carry_n_1,
      CO(1) => eqOp_carry_n_2,
      CO(0) => eqOp_carry_n_3,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => NLW_eqOp_carry_O_UNCONNECTED(3 downto 0),
      S(3) => eqOp_carry_i_1_n_0,
      S(2) => eqOp_carry_i_2_n_0,
      S(1) => eqOp_carry_i_3_n_0,
      S(0) => eqOp_carry_i_4_n_0
    );
\eqOp_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => eqOp_carry_n_0,
      CO(3 downto 2) => \NLW_eqOp_carry__0_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \eqOp_carry__0_n_2\,
      CO(0) => \eqOp_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_eqOp_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 2) => B"00",
      S(1) => \eqOp_carry__0_i_1_n_0\,
      S(0) => \eqOp_carry__0_i_2_n_0\
    );
\eqOp_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \minusOp_carry__2_n_1\,
      I1 => cnt_reg(15),
      O => \eqOp_carry__0_i_1_n_0\
    );
\eqOp_carry__0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => cnt_reg(12),
      I1 => minusOp(12),
      I2 => minusOp(14),
      I3 => cnt_reg(14),
      I4 => minusOp(13),
      I5 => cnt_reg(13),
      O => \eqOp_carry__0_i_2_n_0\
    );
eqOp_carry_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => cnt_reg(9),
      I1 => minusOp(9),
      I2 => minusOp(11),
      I3 => cnt_reg(11),
      I4 => minusOp(10),
      I5 => cnt_reg(10),
      O => eqOp_carry_i_1_n_0
    );
eqOp_carry_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => cnt_reg(6),
      I1 => minusOp(6),
      I2 => minusOp(8),
      I3 => cnt_reg(8),
      I4 => minusOp(7),
      I5 => cnt_reg(7),
      O => eqOp_carry_i_2_n_0
    );
eqOp_carry_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => cnt_reg(3),
      I1 => minusOp(3),
      I2 => minusOp(5),
      I3 => cnt_reg(5),
      I4 => minusOp(4),
      I5 => cnt_reg(4),
      O => eqOp_carry_i_3_n_0
    );
eqOp_carry_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6006000000006006"
    )
        port map (
      I0 => cnt_reg(0),
      I1 => N(0),
      I2 => minusOp(2),
      I3 => cnt_reg(2),
      I4 => minusOp(1),
      I5 => cnt_reg(1),
      O => eqOp_carry_i_4_n_0
    );
minusOp_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => minusOp_carry_n_0,
      CO(2) => minusOp_carry_n_1,
      CO(1) => minusOp_carry_n_2,
      CO(0) => minusOp_carry_n_3,
      CYINIT => N(0),
      DI(3 downto 0) => N(4 downto 1),
      O(3 downto 0) => minusOp(4 downto 1),
      S(3) => minusOp_carry_i_1_n_0,
      S(2) => minusOp_carry_i_2_n_0,
      S(1) => minusOp_carry_i_3_n_0,
      S(0) => minusOp_carry_i_4_n_0
    );
\minusOp_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => minusOp_carry_n_0,
      CO(3) => \minusOp_carry__0_n_0\,
      CO(2) => \minusOp_carry__0_n_1\,
      CO(1) => \minusOp_carry__0_n_2\,
      CO(0) => \minusOp_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => N(8 downto 5),
      O(3 downto 0) => minusOp(8 downto 5),
      S(3) => \minusOp_carry__0_i_1_n_0\,
      S(2) => \minusOp_carry__0_i_2_n_0\,
      S(1) => \minusOp_carry__0_i_3_n_0\,
      S(0) => \minusOp_carry__0_i_4_n_0\
    );
\minusOp_carry__0_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => N(8),
      O => \minusOp_carry__0_i_1_n_0\
    );
\minusOp_carry__0_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => N(7),
      O => \minusOp_carry__0_i_2_n_0\
    );
\minusOp_carry__0_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => N(6),
      O => \minusOp_carry__0_i_3_n_0\
    );
\minusOp_carry__0_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => N(5),
      O => \minusOp_carry__0_i_4_n_0\
    );
\minusOp_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \minusOp_carry__0_n_0\,
      CO(3) => \minusOp_carry__1_n_0\,
      CO(2) => \minusOp_carry__1_n_1\,
      CO(1) => \minusOp_carry__1_n_2\,
      CO(0) => \minusOp_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => N(12 downto 9),
      O(3 downto 0) => minusOp(12 downto 9),
      S(3) => \minusOp_carry__1_i_1_n_0\,
      S(2) => \minusOp_carry__1_i_2_n_0\,
      S(1) => \minusOp_carry__1_i_3_n_0\,
      S(0) => \minusOp_carry__1_i_4_n_0\
    );
\minusOp_carry__1_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => N(12),
      O => \minusOp_carry__1_i_1_n_0\
    );
\minusOp_carry__1_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => N(11),
      O => \minusOp_carry__1_i_2_n_0\
    );
\minusOp_carry__1_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => N(10),
      O => \minusOp_carry__1_i_3_n_0\
    );
\minusOp_carry__1_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => N(9),
      O => \minusOp_carry__1_i_4_n_0\
    );
\minusOp_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \minusOp_carry__1_n_0\,
      CO(3) => \NLW_minusOp_carry__2_CO_UNCONNECTED\(3),
      CO(2) => \minusOp_carry__2_n_1\,
      CO(1) => \NLW_minusOp_carry__2_CO_UNCONNECTED\(1),
      CO(0) => \minusOp_carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1 downto 0) => N(14 downto 13),
      O(3 downto 2) => \NLW_minusOp_carry__2_O_UNCONNECTED\(3 downto 2),
      O(1 downto 0) => minusOp(14 downto 13),
      S(3 downto 2) => B"01",
      S(1) => \minusOp_carry__2_i_1_n_0\,
      S(0) => \minusOp_carry__2_i_2_n_0\
    );
\minusOp_carry__2_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => N(14),
      O => \minusOp_carry__2_i_1_n_0\
    );
\minusOp_carry__2_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => N(13),
      O => \minusOp_carry__2_i_2_n_0\
    );
minusOp_carry_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => N(4),
      O => minusOp_carry_i_1_n_0
    );
minusOp_carry_i_2: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => N(3),
      O => minusOp_carry_i_2_n_0
    );
minusOp_carry_i_3: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => N(2),
      O => minusOp_carry_i_3_n_0
    );
minusOp_carry_i_4: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => N(1),
      O => minusOp_carry_i_4_n_0
    );
\tmp_sum[11]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAA2AAA"
    )
        port map (
      I0 => Q(8),
      I1 => \tmp_sum[15]_i_3_n_0\,
      I2 => \tmp_sum[15]_i_4_n_0\,
      I3 => \tmp_sum[15]_i_5_n_0\,
      I4 => \tmp_sum[15]_i_6_n_0\,
      O => \tmp_sum[11]_i_2_n_0\
    );
\tmp_sum[11]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAA2AAA"
    )
        port map (
      I0 => \^dout\(11),
      I1 => \tmp_sum[15]_i_3_n_0\,
      I2 => \tmp_sum[15]_i_4_n_0\,
      I3 => \tmp_sum[15]_i_5_n_0\,
      I4 => \tmp_sum[15]_i_6_n_0\,
      O => \tmp_sum[11]_i_3_n_0\
    );
\tmp_sum[11]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAA2AAA"
    )
        port map (
      I0 => \^dout\(10),
      I1 => \tmp_sum[15]_i_3_n_0\,
      I2 => \tmp_sum[15]_i_4_n_0\,
      I3 => \tmp_sum[15]_i_5_n_0\,
      I4 => \tmp_sum[15]_i_6_n_0\,
      O => \tmp_sum[11]_i_4_n_0\
    );
\tmp_sum[11]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAA2AAA"
    )
        port map (
      I0 => \^dout\(9),
      I1 => \tmp_sum[15]_i_3_n_0\,
      I2 => \tmp_sum[15]_i_4_n_0\,
      I3 => \tmp_sum[15]_i_5_n_0\,
      I4 => \tmp_sum[15]_i_6_n_0\,
      O => \tmp_sum[11]_i_5_n_0\
    );
\tmp_sum[11]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5555D555AAAA2AAA"
    )
        port map (
      I0 => \^dout\(8),
      I1 => \tmp_sum[15]_i_3_n_0\,
      I2 => \tmp_sum[15]_i_4_n_0\,
      I3 => \tmp_sum[15]_i_5_n_0\,
      I4 => \tmp_sum[15]_i_6_n_0\,
      I5 => Q(8),
      O => \tmp_sum[11]_i_6_n_0\
    );
\tmp_sum[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0080FFFF"
    )
        port map (
      I0 => \tmp_sum[15]_i_3_n_0\,
      I1 => \tmp_sum[15]_i_4_n_0\,
      I2 => \tmp_sum[15]_i_5_n_0\,
      I3 => \tmp_sum[15]_i_6_n_0\,
      I4 => resetn,
      O => \tmp_sum[15]_i_1_n_0\
    );
\tmp_sum[15]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAA2AAA"
    )
        port map (
      I0 => \^dout\(12),
      I1 => \tmp_sum[15]_i_3_n_0\,
      I2 => \tmp_sum[15]_i_4_n_0\,
      I3 => \tmp_sum[15]_i_5_n_0\,
      I4 => \tmp_sum[15]_i_6_n_0\,
      O => \tmp_sum[15]_i_10_n_0\
    );
\tmp_sum[15]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => cnt_reg(3),
      I1 => cnt_reg(2),
      I2 => cnt_reg(1),
      I3 => cnt_reg(0),
      O => \tmp_sum[15]_i_3_n_0\
    );
\tmp_sum[15]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => cnt_reg(7),
      I1 => cnt_reg(6),
      I2 => cnt_reg(5),
      I3 => cnt_reg(4),
      O => \tmp_sum[15]_i_4_n_0\
    );
\tmp_sum[15]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => cnt_reg(14),
      I1 => cnt_reg(15),
      I2 => cnt_reg(13),
      I3 => cnt_reg(12),
      O => \tmp_sum[15]_i_5_n_0\
    );
\tmp_sum[15]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => cnt_reg(9),
      I1 => cnt_reg(8),
      I2 => cnt_reg(11),
      I3 => cnt_reg(10),
      O => \tmp_sum[15]_i_6_n_0\
    );
\tmp_sum[15]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAA2AAA"
    )
        port map (
      I0 => \^dout\(15),
      I1 => \tmp_sum[15]_i_3_n_0\,
      I2 => \tmp_sum[15]_i_4_n_0\,
      I3 => \tmp_sum[15]_i_5_n_0\,
      I4 => \tmp_sum[15]_i_6_n_0\,
      O => \tmp_sum[15]_i_7_n_0\
    );
\tmp_sum[15]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAA2AAA"
    )
        port map (
      I0 => \^dout\(14),
      I1 => \tmp_sum[15]_i_3_n_0\,
      I2 => \tmp_sum[15]_i_4_n_0\,
      I3 => \tmp_sum[15]_i_5_n_0\,
      I4 => \tmp_sum[15]_i_6_n_0\,
      O => \tmp_sum[15]_i_8_n_0\
    );
\tmp_sum[15]_i_9\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAA2AAA"
    )
        port map (
      I0 => \^dout\(13),
      I1 => \tmp_sum[15]_i_3_n_0\,
      I2 => \tmp_sum[15]_i_4_n_0\,
      I3 => \tmp_sum[15]_i_5_n_0\,
      I4 => \tmp_sum[15]_i_6_n_0\,
      O => \tmp_sum[15]_i_9_n_0\
    );
\tmp_sum[3]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAA2AAA"
    )
        port map (
      I0 => Q(3),
      I1 => \tmp_sum[15]_i_3_n_0\,
      I2 => \tmp_sum[15]_i_4_n_0\,
      I3 => \tmp_sum[15]_i_5_n_0\,
      I4 => \tmp_sum[15]_i_6_n_0\,
      O => \tmp_sum[3]_i_2_n_0\
    );
\tmp_sum[3]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAA2AAA"
    )
        port map (
      I0 => Q(2),
      I1 => \tmp_sum[15]_i_3_n_0\,
      I2 => \tmp_sum[15]_i_4_n_0\,
      I3 => \tmp_sum[15]_i_5_n_0\,
      I4 => \tmp_sum[15]_i_6_n_0\,
      O => \tmp_sum[3]_i_3_n_0\
    );
\tmp_sum[3]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAA2AAA"
    )
        port map (
      I0 => Q(1),
      I1 => \tmp_sum[15]_i_3_n_0\,
      I2 => \tmp_sum[15]_i_4_n_0\,
      I3 => \tmp_sum[15]_i_5_n_0\,
      I4 => \tmp_sum[15]_i_6_n_0\,
      O => \tmp_sum[3]_i_4_n_0\
    );
\tmp_sum[3]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAA2AAA"
    )
        port map (
      I0 => Q(0),
      I1 => \tmp_sum[15]_i_3_n_0\,
      I2 => \tmp_sum[15]_i_4_n_0\,
      I3 => \tmp_sum[15]_i_5_n_0\,
      I4 => \tmp_sum[15]_i_6_n_0\,
      O => \tmp_sum[3]_i_5_n_0\
    );
\tmp_sum[3]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5555D555AAAA2AAA"
    )
        port map (
      I0 => \^dout\(3),
      I1 => \tmp_sum[15]_i_3_n_0\,
      I2 => \tmp_sum[15]_i_4_n_0\,
      I3 => \tmp_sum[15]_i_5_n_0\,
      I4 => \tmp_sum[15]_i_6_n_0\,
      I5 => Q(3),
      O => \tmp_sum[3]_i_6_n_0\
    );
\tmp_sum[3]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5555D555AAAA2AAA"
    )
        port map (
      I0 => \^dout\(2),
      I1 => \tmp_sum[15]_i_3_n_0\,
      I2 => \tmp_sum[15]_i_4_n_0\,
      I3 => \tmp_sum[15]_i_5_n_0\,
      I4 => \tmp_sum[15]_i_6_n_0\,
      I5 => Q(2),
      O => \tmp_sum[3]_i_7_n_0\
    );
\tmp_sum[3]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5555D555AAAA2AAA"
    )
        port map (
      I0 => \^dout\(1),
      I1 => \tmp_sum[15]_i_3_n_0\,
      I2 => \tmp_sum[15]_i_4_n_0\,
      I3 => \tmp_sum[15]_i_5_n_0\,
      I4 => \tmp_sum[15]_i_6_n_0\,
      I5 => Q(1),
      O => \tmp_sum[3]_i_8_n_0\
    );
\tmp_sum[3]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5555D555AAAA2AAA"
    )
        port map (
      I0 => \^dout\(0),
      I1 => \tmp_sum[15]_i_3_n_0\,
      I2 => \tmp_sum[15]_i_4_n_0\,
      I3 => \tmp_sum[15]_i_5_n_0\,
      I4 => \tmp_sum[15]_i_6_n_0\,
      I5 => Q(0),
      O => \tmp_sum[3]_i_9_n_0\
    );
\tmp_sum[7]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAA2AAA"
    )
        port map (
      I0 => Q(7),
      I1 => \tmp_sum[15]_i_3_n_0\,
      I2 => \tmp_sum[15]_i_4_n_0\,
      I3 => \tmp_sum[15]_i_5_n_0\,
      I4 => \tmp_sum[15]_i_6_n_0\,
      O => \tmp_sum[7]_i_2_n_0\
    );
\tmp_sum[7]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAA2AAA"
    )
        port map (
      I0 => Q(6),
      I1 => \tmp_sum[15]_i_3_n_0\,
      I2 => \tmp_sum[15]_i_4_n_0\,
      I3 => \tmp_sum[15]_i_5_n_0\,
      I4 => \tmp_sum[15]_i_6_n_0\,
      O => \tmp_sum[7]_i_3_n_0\
    );
\tmp_sum[7]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAA2AAA"
    )
        port map (
      I0 => Q(5),
      I1 => \tmp_sum[15]_i_3_n_0\,
      I2 => \tmp_sum[15]_i_4_n_0\,
      I3 => \tmp_sum[15]_i_5_n_0\,
      I4 => \tmp_sum[15]_i_6_n_0\,
      O => \tmp_sum[7]_i_4_n_0\
    );
\tmp_sum[7]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAA2AAA"
    )
        port map (
      I0 => Q(4),
      I1 => \tmp_sum[15]_i_3_n_0\,
      I2 => \tmp_sum[15]_i_4_n_0\,
      I3 => \tmp_sum[15]_i_5_n_0\,
      I4 => \tmp_sum[15]_i_6_n_0\,
      O => \tmp_sum[7]_i_5_n_0\
    );
\tmp_sum[7]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5555D555AAAA2AAA"
    )
        port map (
      I0 => \^dout\(7),
      I1 => \tmp_sum[15]_i_3_n_0\,
      I2 => \tmp_sum[15]_i_4_n_0\,
      I3 => \tmp_sum[15]_i_5_n_0\,
      I4 => \tmp_sum[15]_i_6_n_0\,
      I5 => Q(7),
      O => \tmp_sum[7]_i_6_n_0\
    );
\tmp_sum[7]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5555D555AAAA2AAA"
    )
        port map (
      I0 => \^dout\(6),
      I1 => \tmp_sum[15]_i_3_n_0\,
      I2 => \tmp_sum[15]_i_4_n_0\,
      I3 => \tmp_sum[15]_i_5_n_0\,
      I4 => \tmp_sum[15]_i_6_n_0\,
      I5 => Q(6),
      O => \tmp_sum[7]_i_7_n_0\
    );
\tmp_sum[7]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5555D555AAAA2AAA"
    )
        port map (
      I0 => \^dout\(5),
      I1 => \tmp_sum[15]_i_3_n_0\,
      I2 => \tmp_sum[15]_i_4_n_0\,
      I3 => \tmp_sum[15]_i_5_n_0\,
      I4 => \tmp_sum[15]_i_6_n_0\,
      I5 => Q(5),
      O => \tmp_sum[7]_i_8_n_0\
    );
\tmp_sum[7]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5555D555AAAA2AAA"
    )
        port map (
      I0 => \^dout\(4),
      I1 => \tmp_sum[15]_i_3_n_0\,
      I2 => \tmp_sum[15]_i_4_n_0\,
      I3 => \tmp_sum[15]_i_5_n_0\,
      I4 => \tmp_sum[15]_i_6_n_0\,
      I5 => Q(4),
      O => \tmp_sum[7]_i_9_n_0\
    );
\tmp_sum[8]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => resetn,
      O => \^sr\(0)
    );
\tmp_sum_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => \tmp_sum_reg[3]_i_1_n_7\,
      Q => \^dout\(0),
      R => \^sr\(0)
    );
\tmp_sum_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => \tmp_sum_reg[11]_i_1_n_5\,
      Q => \^dout\(10),
      R => \tmp_sum[15]_i_1_n_0\
    );
\tmp_sum_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => \tmp_sum_reg[11]_i_1_n_4\,
      Q => \^dout\(11),
      R => \tmp_sum[15]_i_1_n_0\
    );
\tmp_sum_reg[11]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \tmp_sum_reg[7]_i_1_n_0\,
      CO(3) => \tmp_sum_reg[11]_i_1_n_0\,
      CO(2) => \tmp_sum_reg[11]_i_1_n_1\,
      CO(1) => \tmp_sum_reg[11]_i_1_n_2\,
      CO(0) => \tmp_sum_reg[11]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \tmp_sum[11]_i_2_n_0\,
      O(3) => \tmp_sum_reg[11]_i_1_n_4\,
      O(2) => \tmp_sum_reg[11]_i_1_n_5\,
      O(1) => \tmp_sum_reg[11]_i_1_n_6\,
      O(0) => \tmp_sum_reg[11]_i_1_n_7\,
      S(3) => \tmp_sum[11]_i_3_n_0\,
      S(2) => \tmp_sum[11]_i_4_n_0\,
      S(1) => \tmp_sum[11]_i_5_n_0\,
      S(0) => \tmp_sum[11]_i_6_n_0\
    );
\tmp_sum_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => \tmp_sum_reg[15]_i_2_n_7\,
      Q => \^dout\(12),
      R => \tmp_sum[15]_i_1_n_0\
    );
\tmp_sum_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => \tmp_sum_reg[15]_i_2_n_6\,
      Q => \^dout\(13),
      R => \tmp_sum[15]_i_1_n_0\
    );
\tmp_sum_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => \tmp_sum_reg[15]_i_2_n_5\,
      Q => \^dout\(14),
      R => \tmp_sum[15]_i_1_n_0\
    );
\tmp_sum_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => \tmp_sum_reg[15]_i_2_n_4\,
      Q => \^dout\(15),
      R => \tmp_sum[15]_i_1_n_0\
    );
\tmp_sum_reg[15]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \tmp_sum_reg[11]_i_1_n_0\,
      CO(3) => \NLW_tmp_sum_reg[15]_i_2_CO_UNCONNECTED\(3),
      CO(2) => \tmp_sum_reg[15]_i_2_n_1\,
      CO(1) => \tmp_sum_reg[15]_i_2_n_2\,
      CO(0) => \tmp_sum_reg[15]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tmp_sum_reg[15]_i_2_n_4\,
      O(2) => \tmp_sum_reg[15]_i_2_n_5\,
      O(1) => \tmp_sum_reg[15]_i_2_n_6\,
      O(0) => \tmp_sum_reg[15]_i_2_n_7\,
      S(3) => \tmp_sum[15]_i_7_n_0\,
      S(2) => \tmp_sum[15]_i_8_n_0\,
      S(1) => \tmp_sum[15]_i_9_n_0\,
      S(0) => \tmp_sum[15]_i_10_n_0\
    );
\tmp_sum_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => \tmp_sum_reg[3]_i_1_n_6\,
      Q => \^dout\(1),
      R => \^sr\(0)
    );
\tmp_sum_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => \tmp_sum_reg[3]_i_1_n_5\,
      Q => \^dout\(2),
      R => \^sr\(0)
    );
\tmp_sum_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => \tmp_sum_reg[3]_i_1_n_4\,
      Q => \^dout\(3),
      R => \^sr\(0)
    );
\tmp_sum_reg[3]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \tmp_sum_reg[3]_i_1_n_0\,
      CO(2) => \tmp_sum_reg[3]_i_1_n_1\,
      CO(1) => \tmp_sum_reg[3]_i_1_n_2\,
      CO(0) => \tmp_sum_reg[3]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => \tmp_sum[3]_i_2_n_0\,
      DI(2) => \tmp_sum[3]_i_3_n_0\,
      DI(1) => \tmp_sum[3]_i_4_n_0\,
      DI(0) => \tmp_sum[3]_i_5_n_0\,
      O(3) => \tmp_sum_reg[3]_i_1_n_4\,
      O(2) => \tmp_sum_reg[3]_i_1_n_5\,
      O(1) => \tmp_sum_reg[3]_i_1_n_6\,
      O(0) => \tmp_sum_reg[3]_i_1_n_7\,
      S(3) => \tmp_sum[3]_i_6_n_0\,
      S(2) => \tmp_sum[3]_i_7_n_0\,
      S(1) => \tmp_sum[3]_i_8_n_0\,
      S(0) => \tmp_sum[3]_i_9_n_0\
    );
\tmp_sum_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => \tmp_sum_reg[7]_i_1_n_7\,
      Q => \^dout\(4),
      R => \^sr\(0)
    );
\tmp_sum_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => \tmp_sum_reg[7]_i_1_n_6\,
      Q => \^dout\(5),
      R => \^sr\(0)
    );
\tmp_sum_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => \tmp_sum_reg[7]_i_1_n_5\,
      Q => \^dout\(6),
      R => \^sr\(0)
    );
\tmp_sum_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => \tmp_sum_reg[7]_i_1_n_4\,
      Q => \^dout\(7),
      R => \^sr\(0)
    );
\tmp_sum_reg[7]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \tmp_sum_reg[3]_i_1_n_0\,
      CO(3) => \tmp_sum_reg[7]_i_1_n_0\,
      CO(2) => \tmp_sum_reg[7]_i_1_n_1\,
      CO(1) => \tmp_sum_reg[7]_i_1_n_2\,
      CO(0) => \tmp_sum_reg[7]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => \tmp_sum[7]_i_2_n_0\,
      DI(2) => \tmp_sum[7]_i_3_n_0\,
      DI(1) => \tmp_sum[7]_i_4_n_0\,
      DI(0) => \tmp_sum[7]_i_5_n_0\,
      O(3) => \tmp_sum_reg[7]_i_1_n_4\,
      O(2) => \tmp_sum_reg[7]_i_1_n_5\,
      O(1) => \tmp_sum_reg[7]_i_1_n_6\,
      O(0) => \tmp_sum_reg[7]_i_1_n_7\,
      S(3) => \tmp_sum[7]_i_6_n_0\,
      S(2) => \tmp_sum[7]_i_7_n_0\,
      S(1) => \tmp_sum[7]_i_8_n_0\,
      S(0) => \tmp_sum[7]_i_9_n_0\
    );
\tmp_sum_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => \tmp_sum_reg[11]_i_1_n_7\,
      Q => \^dout\(8),
      R => \^sr\(0)
    );
\tmp_sum_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_in,
      CE => '1',
      D => \tmp_sum_reg[11]_i_1_n_6\,
      Q => \^dout\(9),
      R => \tmp_sum[15]_i_1_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity Histo1_ADC_Decimator_0_0_Decimate_by2 is
  port (
    Q : out STD_LOGIC_VECTOR ( 8 downto 0 );
    Din : in STD_LOGIC_VECTOR ( 15 downto 0 );
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    clk_in : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of Histo1_ADC_Decimator_0_0_Decimate_by2 : entity is "Decimate_by2";
end Histo1_ADC_Decimator_0_0_Decimate_by2;

architecture STRUCTURE of Histo1_ADC_Decimator_0_0_Decimate_by2 is
  signal \dout[3]_i_2_n_0\ : STD_LOGIC;
  signal \dout[3]_i_3_n_0\ : STD_LOGIC;
  signal \dout[3]_i_4_n_0\ : STD_LOGIC;
  signal \dout[3]_i_5_n_0\ : STD_LOGIC;
  signal \dout[7]_i_2_n_0\ : STD_LOGIC;
  signal \dout[7]_i_3_n_0\ : STD_LOGIC;
  signal \dout[7]_i_4_n_0\ : STD_LOGIC;
  signal \dout[7]_i_5_n_0\ : STD_LOGIC;
  signal \dout_reg[3]_i_1_n_0\ : STD_LOGIC;
  signal \dout_reg[3]_i_1_n_1\ : STD_LOGIC;
  signal \dout_reg[3]_i_1_n_2\ : STD_LOGIC;
  signal \dout_reg[3]_i_1_n_3\ : STD_LOGIC;
  signal \dout_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal \dout_reg[7]_i_1_n_1\ : STD_LOGIC;
  signal \dout_reg[7]_i_1_n_2\ : STD_LOGIC;
  signal \dout_reg[7]_i_1_n_3\ : STD_LOGIC;
  signal plusOp : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal \NLW_dout_reg[8]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_dout_reg[8]_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
begin
\dout[3]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Din(11),
      I1 => Din(3),
      O => \dout[3]_i_2_n_0\
    );
\dout[3]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Din(10),
      I1 => Din(2),
      O => \dout[3]_i_3_n_0\
    );
\dout[3]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Din(9),
      I1 => Din(1),
      O => \dout[3]_i_4_n_0\
    );
\dout[3]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Din(8),
      I1 => Din(0),
      O => \dout[3]_i_5_n_0\
    );
\dout[7]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Din(15),
      I1 => Din(7),
      O => \dout[7]_i_2_n_0\
    );
\dout[7]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Din(14),
      I1 => Din(6),
      O => \dout[7]_i_3_n_0\
    );
\dout[7]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Din(13),
      I1 => Din(5),
      O => \dout[7]_i_4_n_0\
    );
\dout[7]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Din(12),
      I1 => Din(4),
      O => \dout[7]_i_5_n_0\
    );
\dout_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk_in,
      CE => '1',
      D => plusOp(0),
      Q => Q(0),
      R => SR(0)
    );
\dout_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk_in,
      CE => '1',
      D => plusOp(1),
      Q => Q(1),
      R => SR(0)
    );
\dout_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk_in,
      CE => '1',
      D => plusOp(2),
      Q => Q(2),
      R => SR(0)
    );
\dout_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk_in,
      CE => '1',
      D => plusOp(3),
      Q => Q(3),
      R => SR(0)
    );
\dout_reg[3]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \dout_reg[3]_i_1_n_0\,
      CO(2) => \dout_reg[3]_i_1_n_1\,
      CO(1) => \dout_reg[3]_i_1_n_2\,
      CO(0) => \dout_reg[3]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Din(11 downto 8),
      O(3 downto 0) => plusOp(3 downto 0),
      S(3) => \dout[3]_i_2_n_0\,
      S(2) => \dout[3]_i_3_n_0\,
      S(1) => \dout[3]_i_4_n_0\,
      S(0) => \dout[3]_i_5_n_0\
    );
\dout_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk_in,
      CE => '1',
      D => plusOp(4),
      Q => Q(4),
      R => SR(0)
    );
\dout_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk_in,
      CE => '1',
      D => plusOp(5),
      Q => Q(5),
      R => SR(0)
    );
\dout_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk_in,
      CE => '1',
      D => plusOp(6),
      Q => Q(6),
      R => SR(0)
    );
\dout_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk_in,
      CE => '1',
      D => plusOp(7),
      Q => Q(7),
      R => SR(0)
    );
\dout_reg[7]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \dout_reg[3]_i_1_n_0\,
      CO(3) => \dout_reg[7]_i_1_n_0\,
      CO(2) => \dout_reg[7]_i_1_n_1\,
      CO(1) => \dout_reg[7]_i_1_n_2\,
      CO(0) => \dout_reg[7]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Din(15 downto 12),
      O(3 downto 0) => plusOp(7 downto 4),
      S(3) => \dout[7]_i_2_n_0\,
      S(2) => \dout[7]_i_3_n_0\,
      S(1) => \dout[7]_i_4_n_0\,
      S(0) => \dout[7]_i_5_n_0\
    );
\dout_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk_in,
      CE => '1',
      D => plusOp(8),
      Q => Q(8),
      R => SR(0)
    );
\dout_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \dout_reg[7]_i_1_n_0\,
      CO(3 downto 1) => \NLW_dout_reg[8]_i_1_CO_UNCONNECTED\(3 downto 1),
      CO(0) => plusOp(8),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_dout_reg[8]_i_1_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"0001"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity Histo1_ADC_Decimator_0_0_ADC_Decimator is
  port (
    Dout : out STD_LOGIC_VECTOR ( 15 downto 0 );
    D_sum : out STD_LOGIC_VECTOR ( 15 downto 0 );
    Decim_data_valid : out STD_LOGIC;
    resetn : in STD_LOGIC;
    clk_in : in STD_LOGIC;
    Din : in STD_LOGIC_VECTOR ( 15 downto 0 );
    N : in STD_LOGIC_VECTOR ( 14 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of Histo1_ADC_Decimator_0_0_ADC_Decimator : entity is "ADC_Decimator";
end Histo1_ADC_Decimator_0_0_ADC_Decimator;

architecture STRUCTURE of Histo1_ADC_Decimator_0_0_ADC_Decimator is
  signal Din_sig : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal p_0_in : STD_LOGIC;
begin
Decimate_N_init: entity work.Histo1_ADC_Decimator_0_0_Decimate_N
     port map (
      D_sum(15 downto 0) => D_sum(15 downto 0),
      Decim_data_valid => Decim_data_valid,
      Dout(15 downto 0) => Dout(15 downto 0),
      N(14 downto 0) => N(14 downto 0),
      Q(8 downto 0) => Din_sig(8 downto 0),
      SR(0) => p_0_in,
      clk_in => clk_in,
      resetn => resetn
    );
Decimate_by2_init: entity work.Histo1_ADC_Decimator_0_0_Decimate_by2
     port map (
      Din(15 downto 0) => Din(15 downto 0),
      Q(8 downto 0) => Din_sig(8 downto 0),
      SR(0) => p_0_in,
      clk_in => clk_in
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity Histo1_ADC_Decimator_0_0 is
  port (
    clk_in : in STD_LOGIC;
    resetn : in STD_LOGIC;
    Din : in STD_LOGIC_VECTOR ( 15 downto 0 );
    N : in STD_LOGIC_VECTOR ( 15 downto 0 );
    clk_out : out STD_LOGIC;
    Dout : out STD_LOGIC_VECTOR ( 15 downto 0 );
    Decim_data_valid : out STD_LOGIC;
    D_sum : out STD_LOGIC_VECTOR ( 15 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of Histo1_ADC_Decimator_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of Histo1_ADC_Decimator_0_0 : entity is "Histo1_ADC_Decimator_0_0,ADC_Decimator,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of Histo1_ADC_Decimator_0_0 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of Histo1_ADC_Decimator_0_0 : entity is "ADC_Decimator,Vivado 2017.4";
end Histo1_ADC_Decimator_0_0;

architecture STRUCTURE of Histo1_ADC_Decimator_0_0 is
  signal \<const0>\ : STD_LOGIC;
  attribute x_interface_info : string;
  attribute x_interface_info of resetn : signal is "xilinx.com:signal:reset:1.0 reset RST";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of resetn : signal is "XIL_INTERFACENAME reset, POLARITY ACTIVE_LOW";
begin
  clk_out <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
U0: entity work.Histo1_ADC_Decimator_0_0_ADC_Decimator
     port map (
      D_sum(15 downto 0) => D_sum(15 downto 0),
      Decim_data_valid => Decim_data_valid,
      Din(15 downto 0) => Din(15 downto 0),
      Dout(15 downto 0) => Dout(15 downto 0),
      N(14 downto 0) => N(15 downto 1),
      clk_in => clk_in,
      resetn => resetn
    );
end STRUCTURE;
