// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.4 (win64) Build 2086221 Fri Dec 15 20:55:39 MST 2017
// Date        : Wed Feb  7 13:10:30 2018
// Host        : HP6-MLAB-9 running 64-bit Service Pack 1  (build 7601)
// Command     : write_verilog -force -mode funcsim
//               c:/Users/infolab/Desktop/my_verilog/histo_w_comblock/project_1/project_1.srcs/sources_1/bd/Histo1/ip/Histo1_ADC500MSPS_Controller_0_0/Histo1_ADC500MSPS_Controller_0_0_sim_netlist.v
// Design      : Histo1_ADC500MSPS_Controller_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "Histo1_ADC500MSPS_Controller_0_0,design_1_adc,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "design_1_adc,Vivado 2017.4" *) 
(* NotValidForBitStream *)
module Histo1_ADC500MSPS_Controller_0_0
   (ADC_Dout_2Sample,
    ADC_Dout_for_PL,
    ADC_Dout_valid,
    ADC_clk_for_PL,
    Ctrl_reg0_in,
    Ctrl_reg1_out,
    Data_from_ADC_N,
    Data_from_ADC_P,
    clk_from_ADC_N,
    clk_from_ADC_P,
    clk_to_ADC_N,
    clk_to_ADC_P,
    clock,
    from_ADC_OR_N,
    from_ADC_OR_P,
    from_adc_calrun_fmc,
    reset,
    to_adc_cal_fmc,
    to_adc_caldly_nscs_fmc,
    to_adc_dclk_rst_fmc,
    to_adc_fsr_ece_fmc,
    to_adc_led_0,
    to_adc_led_1,
    to_adc_outedge_ddr_sdata_fmc,
    to_adc_outv_slck_fmc,
    to_adc_pd_fmc);
  output [15:0]ADC_Dout_2Sample;
  output [31:0]ADC_Dout_for_PL;
  output ADC_Dout_valid;
  output [0:0]ADC_clk_for_PL;
  input [8:0]Ctrl_reg0_in;
  output [1:0]Ctrl_reg1_out;
  input [15:0]Data_from_ADC_N;
  input [15:0]Data_from_ADC_P;
  input [0:0]clk_from_ADC_N;
  input [0:0]clk_from_ADC_P;
  output [0:0]clk_to_ADC_N;
  output [0:0]clk_to_ADC_P;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 CLK.CLOCK CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME CLK.CLOCK, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN Histo1_clock_0" *) input clock;
  input [0:0]from_ADC_OR_N;
  input [0:0]from_ADC_OR_P;
  input from_adc_calrun_fmc;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 RST.RESET RST" *) (* x_interface_parameter = "XIL_INTERFACENAME RST.RESET, POLARITY ACTIVE_LOW" *) input reset;
  output to_adc_cal_fmc;
  output to_adc_caldly_nscs_fmc;
  output to_adc_dclk_rst_fmc;
  output to_adc_fsr_ece_fmc;
  output to_adc_led_0;
  output to_adc_led_1;
  output to_adc_outedge_ddr_sdata_fmc;
  output to_adc_outv_slck_fmc;
  output to_adc_pd_fmc;

  (* DIFF_TERM = 0 *) (* IBUF_LOW_PWR *) wire [15:0]ADC_Dout_2Sample;
  wire [31:0]ADC_Dout_for_PL;
  wire ADC_Dout_valid;
  wire [0:0]ADC_clk_for_PL;
  wire [8:0]Ctrl_reg0_in;
  wire [1:0]Ctrl_reg1_out;
  (* DIFF_TERM = 0 *) (* IBUF_LOW_PWR *) wire [15:0]Data_from_ADC_N;
  (* DIFF_TERM = 0 *) (* IBUF_LOW_PWR *) wire [15:0]Data_from_ADC_P;
  (* DIFF_TERM = 0 *) (* IBUF_LOW_PWR *) wire [0:0]clk_from_ADC_N;
  (* DIFF_TERM = 0 *) (* IBUF_LOW_PWR *) wire [0:0]clk_from_ADC_P;
  (* SLEW = "SLOW" *) wire [0:0]clk_to_ADC_N;
  (* SLEW = "SLOW" *) wire [0:0]clk_to_ADC_P;
  (* IBUF_LOW_PWR *) wire clock;
  (* DIFF_TERM = 0 *) (* IBUF_LOW_PWR *) wire [0:0]from_ADC_OR_N;
  (* DIFF_TERM = 0 *) (* IBUF_LOW_PWR *) wire [0:0]from_ADC_OR_P;
  wire from_adc_calrun_fmc;
  wire reset;
  wire to_adc_cal_fmc;
  wire to_adc_caldly_nscs_fmc;
  wire to_adc_dclk_rst_fmc;
  wire to_adc_fsr_ece_fmc;
  wire to_adc_led_0;
  wire to_adc_led_1;
  wire to_adc_outedge_ddr_sdata_fmc;
  wire to_adc_outv_slck_fmc;
  wire to_adc_pd_fmc;

  Histo1_ADC500MSPS_Controller_0_0_design_1_adc U0
       (.ADC_Dout_2Sample(ADC_Dout_2Sample),
        .ADC_Dout_for_PL(ADC_Dout_for_PL),
        .ADC_Dout_valid(ADC_Dout_valid),
        .ADC_clk_for_PL(ADC_clk_for_PL),
        .Ctrl_reg0_in(Ctrl_reg0_in),
        .Ctrl_reg1_out(Ctrl_reg1_out),
        .Data_from_ADC_N(Data_from_ADC_N),
        .Data_from_ADC_P(Data_from_ADC_P),
        .clk_from_ADC_N(clk_from_ADC_N),
        .clk_from_ADC_P(clk_from_ADC_P),
        .clk_to_ADC_N(clk_to_ADC_N),
        .clk_to_ADC_P(clk_to_ADC_P),
        .clock(clock),
        .from_ADC_OR_N(from_ADC_OR_N),
        .from_ADC_OR_P(from_ADC_OR_P),
        .from_adc_calrun_fmc(from_adc_calrun_fmc),
        .reset(reset),
        .to_adc_cal_fmc(to_adc_cal_fmc),
        .to_adc_caldly_nscs_fmc(to_adc_caldly_nscs_fmc),
        .to_adc_dclk_rst_fmc(to_adc_dclk_rst_fmc),
        .to_adc_fsr_ece_fmc(to_adc_fsr_ece_fmc),
        .to_adc_led_0(to_adc_led_0),
        .to_adc_led_1(to_adc_led_1),
        .to_adc_outedge_ddr_sdata_fmc(to_adc_outedge_ddr_sdata_fmc),
        .to_adc_outv_slck_fmc(to_adc_outv_slck_fmc),
        .to_adc_pd_fmc(to_adc_pd_fmc));
endmodule

(* ORIG_REF_NAME = "ADC500MHz_Controller_v1_0" *) 
module Histo1_ADC500MSPS_Controller_0_0_ADC500MHz_Controller_v1_0
   (data_valid,
    ADC_Dout,
    ADC_clk,
    ADC_Din,
    ADC_reset);
  output data_valid;
  output [31:0]ADC_Dout;
  input ADC_clk;
  input [15:0]ADC_Din;
  input ADC_reset;

  wire [15:0]ADC_Din;
  wire [31:0]ADC_Dout;
  wire ADC_clk;
  wire ADC_reset;
  wire [15:0]data_int_16b_1;
  wire [15:0]data_int_16b_2;
  wire data_valid;
  wire p_0_in;
  wire p_1_in;

  FDRE \data_int_16b_1_reg[0] 
       (.C(ADC_clk),
        .CE(1'b1),
        .D(ADC_Din[0]),
        .Q(data_int_16b_1[0]),
        .R(p_0_in));
  FDRE \data_int_16b_1_reg[10] 
       (.C(ADC_clk),
        .CE(1'b1),
        .D(ADC_Din[10]),
        .Q(data_int_16b_1[10]),
        .R(p_0_in));
  FDRE \data_int_16b_1_reg[11] 
       (.C(ADC_clk),
        .CE(1'b1),
        .D(ADC_Din[11]),
        .Q(data_int_16b_1[11]),
        .R(p_0_in));
  FDRE \data_int_16b_1_reg[12] 
       (.C(ADC_clk),
        .CE(1'b1),
        .D(ADC_Din[12]),
        .Q(data_int_16b_1[12]),
        .R(p_0_in));
  FDRE \data_int_16b_1_reg[13] 
       (.C(ADC_clk),
        .CE(1'b1),
        .D(ADC_Din[13]),
        .Q(data_int_16b_1[13]),
        .R(p_0_in));
  FDRE \data_int_16b_1_reg[14] 
       (.C(ADC_clk),
        .CE(1'b1),
        .D(ADC_Din[14]),
        .Q(data_int_16b_1[14]),
        .R(p_0_in));
  FDRE \data_int_16b_1_reg[15] 
       (.C(ADC_clk),
        .CE(1'b1),
        .D(ADC_Din[15]),
        .Q(data_int_16b_1[15]),
        .R(p_0_in));
  FDRE \data_int_16b_1_reg[1] 
       (.C(ADC_clk),
        .CE(1'b1),
        .D(ADC_Din[1]),
        .Q(data_int_16b_1[1]),
        .R(p_0_in));
  FDRE \data_int_16b_1_reg[2] 
       (.C(ADC_clk),
        .CE(1'b1),
        .D(ADC_Din[2]),
        .Q(data_int_16b_1[2]),
        .R(p_0_in));
  FDRE \data_int_16b_1_reg[3] 
       (.C(ADC_clk),
        .CE(1'b1),
        .D(ADC_Din[3]),
        .Q(data_int_16b_1[3]),
        .R(p_0_in));
  FDRE \data_int_16b_1_reg[4] 
       (.C(ADC_clk),
        .CE(1'b1),
        .D(ADC_Din[4]),
        .Q(data_int_16b_1[4]),
        .R(p_0_in));
  FDRE \data_int_16b_1_reg[5] 
       (.C(ADC_clk),
        .CE(1'b1),
        .D(ADC_Din[5]),
        .Q(data_int_16b_1[5]),
        .R(p_0_in));
  FDRE \data_int_16b_1_reg[6] 
       (.C(ADC_clk),
        .CE(1'b1),
        .D(ADC_Din[6]),
        .Q(data_int_16b_1[6]),
        .R(p_0_in));
  FDRE \data_int_16b_1_reg[7] 
       (.C(ADC_clk),
        .CE(1'b1),
        .D(ADC_Din[7]),
        .Q(data_int_16b_1[7]),
        .R(p_0_in));
  FDRE \data_int_16b_1_reg[8] 
       (.C(ADC_clk),
        .CE(1'b1),
        .D(ADC_Din[8]),
        .Q(data_int_16b_1[8]),
        .R(p_0_in));
  FDRE \data_int_16b_1_reg[9] 
       (.C(ADC_clk),
        .CE(1'b1),
        .D(ADC_Din[9]),
        .Q(data_int_16b_1[9]),
        .R(p_0_in));
  FDRE \data_int_16b_2_reg[0] 
       (.C(ADC_clk),
        .CE(1'b1),
        .D(data_int_16b_1[0]),
        .Q(data_int_16b_2[0]),
        .R(p_0_in));
  FDRE \data_int_16b_2_reg[10] 
       (.C(ADC_clk),
        .CE(1'b1),
        .D(data_int_16b_1[10]),
        .Q(data_int_16b_2[10]),
        .R(p_0_in));
  FDRE \data_int_16b_2_reg[11] 
       (.C(ADC_clk),
        .CE(1'b1),
        .D(data_int_16b_1[11]),
        .Q(data_int_16b_2[11]),
        .R(p_0_in));
  FDRE \data_int_16b_2_reg[12] 
       (.C(ADC_clk),
        .CE(1'b1),
        .D(data_int_16b_1[12]),
        .Q(data_int_16b_2[12]),
        .R(p_0_in));
  FDRE \data_int_16b_2_reg[13] 
       (.C(ADC_clk),
        .CE(1'b1),
        .D(data_int_16b_1[13]),
        .Q(data_int_16b_2[13]),
        .R(p_0_in));
  FDRE \data_int_16b_2_reg[14] 
       (.C(ADC_clk),
        .CE(1'b1),
        .D(data_int_16b_1[14]),
        .Q(data_int_16b_2[14]),
        .R(p_0_in));
  FDRE \data_int_16b_2_reg[15] 
       (.C(ADC_clk),
        .CE(1'b1),
        .D(data_int_16b_1[15]),
        .Q(data_int_16b_2[15]),
        .R(p_0_in));
  FDRE \data_int_16b_2_reg[1] 
       (.C(ADC_clk),
        .CE(1'b1),
        .D(data_int_16b_1[1]),
        .Q(data_int_16b_2[1]),
        .R(p_0_in));
  FDRE \data_int_16b_2_reg[2] 
       (.C(ADC_clk),
        .CE(1'b1),
        .D(data_int_16b_1[2]),
        .Q(data_int_16b_2[2]),
        .R(p_0_in));
  FDRE \data_int_16b_2_reg[3] 
       (.C(ADC_clk),
        .CE(1'b1),
        .D(data_int_16b_1[3]),
        .Q(data_int_16b_2[3]),
        .R(p_0_in));
  FDRE \data_int_16b_2_reg[4] 
       (.C(ADC_clk),
        .CE(1'b1),
        .D(data_int_16b_1[4]),
        .Q(data_int_16b_2[4]),
        .R(p_0_in));
  FDRE \data_int_16b_2_reg[5] 
       (.C(ADC_clk),
        .CE(1'b1),
        .D(data_int_16b_1[5]),
        .Q(data_int_16b_2[5]),
        .R(p_0_in));
  FDRE \data_int_16b_2_reg[6] 
       (.C(ADC_clk),
        .CE(1'b1),
        .D(data_int_16b_1[6]),
        .Q(data_int_16b_2[6]),
        .R(p_0_in));
  FDRE \data_int_16b_2_reg[7] 
       (.C(ADC_clk),
        .CE(1'b1),
        .D(data_int_16b_1[7]),
        .Q(data_int_16b_2[7]),
        .R(p_0_in));
  FDRE \data_int_16b_2_reg[8] 
       (.C(ADC_clk),
        .CE(1'b1),
        .D(data_int_16b_1[8]),
        .Q(data_int_16b_2[8]),
        .R(p_0_in));
  FDRE \data_int_16b_2_reg[9] 
       (.C(ADC_clk),
        .CE(1'b1),
        .D(data_int_16b_1[9]),
        .Q(data_int_16b_2[9]),
        .R(p_0_in));
  LUT1 #(
    .INIT(2'h1)) 
    \data_int_32b[31]_i_1 
       (.I0(ADC_reset),
        .O(p_0_in));
  FDRE \data_int_32b_reg[0] 
       (.C(ADC_clk),
        .CE(data_valid),
        .D(data_int_16b_1[0]),
        .Q(ADC_Dout[0]),
        .R(p_0_in));
  FDRE \data_int_32b_reg[10] 
       (.C(ADC_clk),
        .CE(data_valid),
        .D(data_int_16b_1[10]),
        .Q(ADC_Dout[10]),
        .R(p_0_in));
  FDRE \data_int_32b_reg[11] 
       (.C(ADC_clk),
        .CE(data_valid),
        .D(data_int_16b_1[11]),
        .Q(ADC_Dout[11]),
        .R(p_0_in));
  FDRE \data_int_32b_reg[12] 
       (.C(ADC_clk),
        .CE(data_valid),
        .D(data_int_16b_1[12]),
        .Q(ADC_Dout[12]),
        .R(p_0_in));
  FDRE \data_int_32b_reg[13] 
       (.C(ADC_clk),
        .CE(data_valid),
        .D(data_int_16b_1[13]),
        .Q(ADC_Dout[13]),
        .R(p_0_in));
  FDRE \data_int_32b_reg[14] 
       (.C(ADC_clk),
        .CE(data_valid),
        .D(data_int_16b_1[14]),
        .Q(ADC_Dout[14]),
        .R(p_0_in));
  FDRE \data_int_32b_reg[15] 
       (.C(ADC_clk),
        .CE(data_valid),
        .D(data_int_16b_1[15]),
        .Q(ADC_Dout[15]),
        .R(p_0_in));
  FDRE \data_int_32b_reg[16] 
       (.C(ADC_clk),
        .CE(data_valid),
        .D(data_int_16b_2[0]),
        .Q(ADC_Dout[16]),
        .R(p_0_in));
  FDRE \data_int_32b_reg[17] 
       (.C(ADC_clk),
        .CE(data_valid),
        .D(data_int_16b_2[1]),
        .Q(ADC_Dout[17]),
        .R(p_0_in));
  FDRE \data_int_32b_reg[18] 
       (.C(ADC_clk),
        .CE(data_valid),
        .D(data_int_16b_2[2]),
        .Q(ADC_Dout[18]),
        .R(p_0_in));
  FDRE \data_int_32b_reg[19] 
       (.C(ADC_clk),
        .CE(data_valid),
        .D(data_int_16b_2[3]),
        .Q(ADC_Dout[19]),
        .R(p_0_in));
  FDRE \data_int_32b_reg[1] 
       (.C(ADC_clk),
        .CE(data_valid),
        .D(data_int_16b_1[1]),
        .Q(ADC_Dout[1]),
        .R(p_0_in));
  FDRE \data_int_32b_reg[20] 
       (.C(ADC_clk),
        .CE(data_valid),
        .D(data_int_16b_2[4]),
        .Q(ADC_Dout[20]),
        .R(p_0_in));
  FDRE \data_int_32b_reg[21] 
       (.C(ADC_clk),
        .CE(data_valid),
        .D(data_int_16b_2[5]),
        .Q(ADC_Dout[21]),
        .R(p_0_in));
  FDRE \data_int_32b_reg[22] 
       (.C(ADC_clk),
        .CE(data_valid),
        .D(data_int_16b_2[6]),
        .Q(ADC_Dout[22]),
        .R(p_0_in));
  FDRE \data_int_32b_reg[23] 
       (.C(ADC_clk),
        .CE(data_valid),
        .D(data_int_16b_2[7]),
        .Q(ADC_Dout[23]),
        .R(p_0_in));
  FDRE \data_int_32b_reg[24] 
       (.C(ADC_clk),
        .CE(data_valid),
        .D(data_int_16b_2[8]),
        .Q(ADC_Dout[24]),
        .R(p_0_in));
  FDRE \data_int_32b_reg[25] 
       (.C(ADC_clk),
        .CE(data_valid),
        .D(data_int_16b_2[9]),
        .Q(ADC_Dout[25]),
        .R(p_0_in));
  FDRE \data_int_32b_reg[26] 
       (.C(ADC_clk),
        .CE(data_valid),
        .D(data_int_16b_2[10]),
        .Q(ADC_Dout[26]),
        .R(p_0_in));
  FDRE \data_int_32b_reg[27] 
       (.C(ADC_clk),
        .CE(data_valid),
        .D(data_int_16b_2[11]),
        .Q(ADC_Dout[27]),
        .R(p_0_in));
  FDRE \data_int_32b_reg[28] 
       (.C(ADC_clk),
        .CE(data_valid),
        .D(data_int_16b_2[12]),
        .Q(ADC_Dout[28]),
        .R(p_0_in));
  FDRE \data_int_32b_reg[29] 
       (.C(ADC_clk),
        .CE(data_valid),
        .D(data_int_16b_2[13]),
        .Q(ADC_Dout[29]),
        .R(p_0_in));
  FDRE \data_int_32b_reg[2] 
       (.C(ADC_clk),
        .CE(data_valid),
        .D(data_int_16b_1[2]),
        .Q(ADC_Dout[2]),
        .R(p_0_in));
  FDRE \data_int_32b_reg[30] 
       (.C(ADC_clk),
        .CE(data_valid),
        .D(data_int_16b_2[14]),
        .Q(ADC_Dout[30]),
        .R(p_0_in));
  FDRE \data_int_32b_reg[31] 
       (.C(ADC_clk),
        .CE(data_valid),
        .D(data_int_16b_2[15]),
        .Q(ADC_Dout[31]),
        .R(p_0_in));
  FDRE \data_int_32b_reg[3] 
       (.C(ADC_clk),
        .CE(data_valid),
        .D(data_int_16b_1[3]),
        .Q(ADC_Dout[3]),
        .R(p_0_in));
  FDRE \data_int_32b_reg[4] 
       (.C(ADC_clk),
        .CE(data_valid),
        .D(data_int_16b_1[4]),
        .Q(ADC_Dout[4]),
        .R(p_0_in));
  FDRE \data_int_32b_reg[5] 
       (.C(ADC_clk),
        .CE(data_valid),
        .D(data_int_16b_1[5]),
        .Q(ADC_Dout[5]),
        .R(p_0_in));
  FDRE \data_int_32b_reg[6] 
       (.C(ADC_clk),
        .CE(data_valid),
        .D(data_int_16b_1[6]),
        .Q(ADC_Dout[6]),
        .R(p_0_in));
  FDRE \data_int_32b_reg[7] 
       (.C(ADC_clk),
        .CE(data_valid),
        .D(data_int_16b_1[7]),
        .Q(ADC_Dout[7]),
        .R(p_0_in));
  FDRE \data_int_32b_reg[8] 
       (.C(ADC_clk),
        .CE(data_valid),
        .D(data_int_16b_1[8]),
        .Q(ADC_Dout[8]),
        .R(p_0_in));
  FDRE \data_int_32b_reg[9] 
       (.C(ADC_clk),
        .CE(data_valid),
        .D(data_int_16b_1[9]),
        .Q(ADC_Dout[9]),
        .R(p_0_in));
  LUT1 #(
    .INIT(2'h1)) 
    data_to_fifo_clock_enable_i_1
       (.I0(data_valid),
        .O(p_1_in));
  FDSE data_to_fifo_clock_enable_reg
       (.C(ADC_clk),
        .CE(1'b1),
        .D(p_1_in),
        .Q(data_valid),
        .S(p_0_in));
endmodule

(* ORIG_REF_NAME = "cdc_sync" *) 
module Histo1_ADC500MSPS_Controller_0_0_cdc_sync
   (lpf_asr_reg,
    scndry_out,
    lpf_asr,
    asr_lpf,
    p_1_in,
    p_2_in,
    aux_reset_in,
    slowest_sync_clk);
  output lpf_asr_reg;
  output scndry_out;
  input lpf_asr;
  input [0:0]asr_lpf;
  input p_1_in;
  input p_2_in;
  input aux_reset_in;
  input slowest_sync_clk;

  wire asr_d1;
  wire [0:0]asr_lpf;
  wire aux_reset_in;
  wire lpf_asr;
  wire lpf_asr_reg;
  wire p_1_in;
  wire p_2_in;
  wire s_level_out_d1_cdc_to;
  wire s_level_out_d2;
  wire s_level_out_d3;
  wire scndry_out;
  wire slowest_sync_clk;

  (* ASYNC_REG *) 
  (* XILINX_LEGACY_PRIM = "FDR" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_IN_cdc_to 
       (.C(slowest_sync_clk),
        .CE(1'b1),
        .D(asr_d1),
        .Q(s_level_out_d1_cdc_to),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_IN_cdc_to_i_1 
       (.I0(aux_reset_in),
        .O(asr_d1));
  (* ASYNC_REG *) 
  (* XILINX_LEGACY_PRIM = "FDR" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_s_level_out_d2 
       (.C(slowest_sync_clk),
        .CE(1'b1),
        .D(s_level_out_d1_cdc_to),
        .Q(s_level_out_d2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XILINX_LEGACY_PRIM = "FDR" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_s_level_out_d3 
       (.C(slowest_sync_clk),
        .CE(1'b1),
        .D(s_level_out_d2),
        .Q(s_level_out_d3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XILINX_LEGACY_PRIM = "FDR" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_s_level_out_d4 
       (.C(slowest_sync_clk),
        .CE(1'b1),
        .D(s_level_out_d3),
        .Q(scndry_out),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hEAAAAAA8)) 
    lpf_asr_i_1
       (.I0(lpf_asr),
        .I1(asr_lpf),
        .I2(scndry_out),
        .I3(p_1_in),
        .I4(p_2_in),
        .O(lpf_asr_reg));
endmodule

(* ORIG_REF_NAME = "cdc_sync" *) 
module Histo1_ADC500MSPS_Controller_0_0_cdc_sync_0
   (lpf_exr_reg,
    scndry_out,
    lpf_exr,
    p_3_out,
    mb_debug_sys_rst,
    ext_reset_in,
    slowest_sync_clk);
  output lpf_exr_reg;
  output scndry_out;
  input lpf_exr;
  input [2:0]p_3_out;
  input mb_debug_sys_rst;
  input ext_reset_in;
  input slowest_sync_clk;

  wire \GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_IN_cdc_to_i_1__0_n_0 ;
  wire ext_reset_in;
  wire lpf_exr;
  wire lpf_exr_reg;
  wire mb_debug_sys_rst;
  wire [2:0]p_3_out;
  wire s_level_out_d1_cdc_to;
  wire s_level_out_d2;
  wire s_level_out_d3;
  wire scndry_out;
  wire slowest_sync_clk;

  (* ASYNC_REG *) 
  (* XILINX_LEGACY_PRIM = "FDR" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_IN_cdc_to 
       (.C(slowest_sync_clk),
        .CE(1'b1),
        .D(\GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_IN_cdc_to_i_1__0_n_0 ),
        .Q(s_level_out_d1_cdc_to),
        .R(1'b0));
  LUT2 #(
    .INIT(4'hB)) 
    \GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_IN_cdc_to_i_1__0 
       (.I0(mb_debug_sys_rst),
        .I1(ext_reset_in),
        .O(\GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_IN_cdc_to_i_1__0_n_0 ));
  (* ASYNC_REG *) 
  (* XILINX_LEGACY_PRIM = "FDR" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_s_level_out_d2 
       (.C(slowest_sync_clk),
        .CE(1'b1),
        .D(s_level_out_d1_cdc_to),
        .Q(s_level_out_d2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XILINX_LEGACY_PRIM = "FDR" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_s_level_out_d3 
       (.C(slowest_sync_clk),
        .CE(1'b1),
        .D(s_level_out_d2),
        .Q(s_level_out_d3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* XILINX_LEGACY_PRIM = "FDR" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_s_level_out_d4 
       (.C(slowest_sync_clk),
        .CE(1'b1),
        .D(s_level_out_d3),
        .Q(scndry_out),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hEAAAAAA8)) 
    lpf_exr_i_1
       (.I0(lpf_exr),
        .I1(p_3_out[0]),
        .I2(scndry_out),
        .I3(p_3_out[1]),
        .I4(p_3_out[2]),
        .O(lpf_exr_reg));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_ADC500MHz_Controller_v1_0_0_0,ADC500MHz_Controller_v1_0,{}" *) (* ORIG_REF_NAME = "design_1_ADC500MHz_Controller_v1_0_0_0" *) (* downgradeipidentifiedwarnings = "yes" *) 
(* x_core_info = "ADC500MHz_Controller_v1_0,Vivado 2017.4" *) 
module Histo1_ADC500MSPS_Controller_0_0_design_1_ADC500MHz_Controller_v1_0_0_0
   (ADC_clk,
    ADC_reset,
    ADC_Din,
    ADC_Dout,
    data_valid,
    Ctrl_reg0_in,
    Ctrl_reg1_out,
    to_adc_cal_fmc,
    to_adc_caldly_nscs_fmc,
    to_adc_fsr_ece_fmc,
    to_adc_outv_slck_fmc,
    to_adc_outedge_ddr_sdata_fmc,
    to_adc_dclk_rst_fmc,
    to_adc_pd_fmc,
    to_adc_led_0,
    to_adc_led_1,
    from_adc_calrun_fmc,
    from_adc_or);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 ADC_clk CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME ADC_clk, ASSOCIATED_RESET ADC_reset, FREQ_HZ 100000000, PHASE 0.000" *) input ADC_clk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 ADC_reset RST" *) (* x_interface_parameter = "XIL_INTERFACENAME ADC_reset, POLARITY ACTIVE_LOW" *) input ADC_reset;
  input [15:0]ADC_Din;
  output [31:0]ADC_Dout;
  output data_valid;
  input [8:0]Ctrl_reg0_in;
  output [1:0]Ctrl_reg1_out;
  output to_adc_cal_fmc;
  output to_adc_caldly_nscs_fmc;
  output to_adc_fsr_ece_fmc;
  output to_adc_outv_slck_fmc;
  output to_adc_outedge_ddr_sdata_fmc;
  output to_adc_dclk_rst_fmc;
  output to_adc_pd_fmc;
  output to_adc_led_0;
  output to_adc_led_1;
  input from_adc_calrun_fmc;
  input from_adc_or;

  wire [15:0]ADC_Din;
  wire [31:0]ADC_Dout;
  wire ADC_clk;
  wire ADC_reset;
  wire [8:0]Ctrl_reg0_in;
  wire data_valid;
  wire from_adc_calrun_fmc;
  wire from_adc_or;

  assign Ctrl_reg1_out[1] = from_adc_or;
  assign Ctrl_reg1_out[0] = from_adc_calrun_fmc;
  assign to_adc_cal_fmc = Ctrl_reg0_in[0];
  assign to_adc_caldly_nscs_fmc = Ctrl_reg0_in[1];
  assign to_adc_dclk_rst_fmc = Ctrl_reg0_in[5];
  assign to_adc_fsr_ece_fmc = Ctrl_reg0_in[2];
  assign to_adc_led_0 = Ctrl_reg0_in[7];
  assign to_adc_led_1 = Ctrl_reg0_in[8];
  assign to_adc_outedge_ddr_sdata_fmc = Ctrl_reg0_in[4];
  assign to_adc_outv_slck_fmc = Ctrl_reg0_in[3];
  assign to_adc_pd_fmc = Ctrl_reg0_in[6];
  Histo1_ADC500MSPS_Controller_0_0_ADC500MHz_Controller_v1_0 U0
       (.ADC_Din(ADC_Din),
        .ADC_Dout(ADC_Dout),
        .ADC_clk(ADC_clk),
        .ADC_reset(ADC_reset),
        .data_valid(data_valid));
endmodule

(* ORIG_REF_NAME = "design_1_adc" *) 
module Histo1_ADC500MSPS_Controller_0_0_design_1_adc
   (ADC_Dout_for_PL,
    ADC_Dout_valid,
    Ctrl_reg1_out,
    to_adc_cal_fmc,
    to_adc_caldly_nscs_fmc,
    to_adc_fsr_ece_fmc,
    to_adc_outv_slck_fmc,
    to_adc_outedge_ddr_sdata_fmc,
    to_adc_dclk_rst_fmc,
    to_adc_pd_fmc,
    to_adc_led_0,
    to_adc_led_1,
    ADC_clk_for_PL,
    ADC_Dout_2Sample,
    clk_to_ADC_P,
    clk_to_ADC_N,
    Ctrl_reg0_in,
    from_adc_calrun_fmc,
    from_ADC_OR_P,
    from_ADC_OR_N,
    Data_from_ADC_P,
    Data_from_ADC_N,
    clk_from_ADC_P,
    clk_from_ADC_N,
    clock,
    reset);
  output [31:0]ADC_Dout_for_PL;
  output ADC_Dout_valid;
  output [1:0]Ctrl_reg1_out;
  output to_adc_cal_fmc;
  output to_adc_caldly_nscs_fmc;
  output to_adc_fsr_ece_fmc;
  output to_adc_outv_slck_fmc;
  output to_adc_outedge_ddr_sdata_fmc;
  output to_adc_dclk_rst_fmc;
  output to_adc_pd_fmc;
  output to_adc_led_0;
  output to_adc_led_1;
  output [0:0]ADC_clk_for_PL;
  output [15:0]ADC_Dout_2Sample;
  output [0:0]clk_to_ADC_P;
  output [0:0]clk_to_ADC_N;
  input [8:0]Ctrl_reg0_in;
  input from_adc_calrun_fmc;
  input [0:0]from_ADC_OR_P;
  input [0:0]from_ADC_OR_N;
  input [15:0]Data_from_ADC_P;
  input [15:0]Data_from_ADC_N;
  input [0:0]clk_from_ADC_P;
  input [0:0]clk_from_ADC_N;
  input clock;
  input reset;

  wire [15:0]ADC_Dout_2Sample;
  wire [31:0]ADC_Dout_for_PL;
  wire ADC_Dout_valid;
  wire [0:0]ADC_clk_for_PL;
  wire [8:0]Ctrl_reg0_in;
  wire [1:0]Ctrl_reg1_out;
  wire DS_ADC_OR_Buffer_IBUF_OUT;
  wire [15:0]Data_from_ADC_N;
  wire [15:0]Data_from_ADC_P;
  wire [0:0]clk_from_ADC_N;
  wire [0:0]clk_from_ADC_P;
  wire [0:0]clk_to_ADC_N;
  wire [0:0]clk_to_ADC_P;
  wire clk_wiz_0_clk_out1;
  wire clock;
  wire [0:0]from_ADC_OR_N;
  wire [0:0]from_ADC_OR_P;
  wire from_adc_calrun_fmc;
  wire reset;
  wire reset_generator_peripheral_aresetn_0;
  wire to_adc_cal_fmc;
  wire to_adc_caldly_nscs_fmc;
  wire to_adc_dclk_rst_fmc;
  wire to_adc_fsr_ece_fmc;
  wire to_adc_led_0;
  wire to_adc_led_1;
  wire to_adc_outedge_ddr_sdata_fmc;
  wire to_adc_outv_slck_fmc;
  wire to_adc_pd_fmc;
  wire util_ds_buf_1_IBUF_OUT1;
  wire NLW_reset_generator_mb_reset_UNCONNECTED;
  wire [0:0]NLW_reset_generator_bus_struct_reset_UNCONNECTED;
  wire [0:0]NLW_reset_generator_interconnect_aresetn_UNCONNECTED;
  wire [0:0]NLW_reset_generator_peripheral_reset_UNCONNECTED;

  (* CHECK_LICENSE_TYPE = "design_1_ADC500MHz_Controller_v1_0_0_0,ADC500MHz_Controller_v1_0,{}" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* x_core_info = "ADC500MHz_Controller_v1_0,Vivado 2017.4" *) 
  Histo1_ADC500MSPS_Controller_0_0_design_1_ADC500MHz_Controller_v1_0_0_0 ADC500MHz_Controller_v1_0_0
       (.ADC_Din(ADC_Dout_2Sample),
        .ADC_Dout(ADC_Dout_for_PL),
        .ADC_clk(ADC_clk_for_PL),
        .ADC_reset(reset_generator_peripheral_aresetn_0),
        .Ctrl_reg0_in(Ctrl_reg0_in),
        .Ctrl_reg1_out(Ctrl_reg1_out),
        .data_valid(ADC_Dout_valid),
        .from_adc_calrun_fmc(from_adc_calrun_fmc),
        .from_adc_or(DS_ADC_OR_Buffer_IBUF_OUT),
        .to_adc_cal_fmc(to_adc_cal_fmc),
        .to_adc_caldly_nscs_fmc(to_adc_caldly_nscs_fmc),
        .to_adc_dclk_rst_fmc(to_adc_dclk_rst_fmc),
        .to_adc_fsr_ece_fmc(to_adc_fsr_ece_fmc),
        .to_adc_led_0(to_adc_led_0),
        .to_adc_led_1(to_adc_led_1),
        .to_adc_outedge_ddr_sdata_fmc(to_adc_outedge_ddr_sdata_fmc),
        .to_adc_outv_slck_fmc(to_adc_outv_slck_fmc),
        .to_adc_pd_fmc(to_adc_pd_fmc));
  (* CHECK_LICENSE_TYPE = "design_1_util_ds_buf_2_0,util_ds_buf,{}" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* x_core_info = "util_ds_buf,Vivado 2017.4" *) 
  Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_2_0 BUFG_clk
       (.BUFG_I(util_ds_buf_1_IBUF_OUT1),
        .BUFG_O(ADC_clk_for_PL));
  (* CHECK_LICENSE_TYPE = "design_1_util_ds_buf_0_1,util_ds_buf,{}" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* x_core_info = "util_ds_buf,Vivado 2017.4" *) 
  Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_0_1 DS_ADC_OR_Buffer
       (.IBUF_DS_N(from_ADC_OR_N),
        .IBUF_DS_P(from_ADC_OR_P),
        .IBUF_OUT(DS_ADC_OR_Buffer_IBUF_OUT));
  (* CHECK_LICENSE_TYPE = "design_1_util_ds_buf_3_0,util_ds_buf,{}" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* x_core_info = "util_ds_buf,Vivado 2017.4" *) 
  Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_3_0 DS_Data_Buffer
       (.IBUF_DS_N(Data_from_ADC_N),
        .IBUF_DS_P(Data_from_ADC_P),
        .IBUF_OUT(ADC_Dout_2Sample));
  (* CHECK_LICENSE_TYPE = "design_1_util_ds_buf_1_0,util_ds_buf,{}" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* x_core_info = "util_ds_buf,Vivado 2017.4" *) 
  Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_1_0 DS_clk_Buffer
       (.IBUF_DS_N(clk_from_ADC_N),
        .IBUF_DS_P(clk_from_ADC_P),
        .IBUF_OUT(util_ds_buf_1_IBUF_OUT1));
  (* CHECK_LICENSE_TYPE = "design_1_util_ds_buf_0_0,util_ds_buf,{}" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* x_core_info = "util_ds_buf,Vivado 2017.4" *) 
  Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_0_0 SD_clk_Buffer
       (.OBUF_DS_N(clk_to_ADC_N),
        .OBUF_DS_P(clk_to_ADC_P),
        .OBUF_IN(clk_wiz_0_clk_out1));
  Histo1_ADC500MSPS_Controller_0_0_design_1_clk_wiz_0_0 clk_wiz_for_ADC
       (.clk_in1(clock),
        .clk_out1(clk_wiz_0_clk_out1),
        .resetn(reset_generator_peripheral_aresetn_0));
  (* CHECK_LICENSE_TYPE = "design_1_proc_sys_reset_0_0,proc_sys_reset,{}" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* x_core_info = "proc_sys_reset,Vivado 2017.4" *) 
  Histo1_ADC500MSPS_Controller_0_0_design_1_proc_sys_reset_0_0 reset_generator
       (.aux_reset_in(1'b1),
        .bus_struct_reset(NLW_reset_generator_bus_struct_reset_UNCONNECTED[0]),
        .dcm_locked(1'b1),
        .ext_reset_in(reset),
        .interconnect_aresetn(NLW_reset_generator_interconnect_aresetn_UNCONNECTED[0]),
        .mb_debug_sys_rst(1'b0),
        .mb_reset(NLW_reset_generator_mb_reset_UNCONNECTED),
        .peripheral_aresetn(reset_generator_peripheral_aresetn_0),
        .peripheral_reset(NLW_reset_generator_peripheral_reset_UNCONNECTED[0]),
        .slowest_sync_clk(ADC_clk_for_PL));
endmodule

(* ORIG_REF_NAME = "design_1_clk_wiz_0_0" *) 
module Histo1_ADC500MSPS_Controller_0_0_design_1_clk_wiz_0_0
   (clk_out1,
    resetn,
    clk_in1);
  output clk_out1;
  input resetn;
  input clk_in1;

  wire clk_in1;
  wire clk_out1;
  wire resetn;

  Histo1_ADC500MSPS_Controller_0_0_design_1_clk_wiz_0_0_clk_wiz inst
       (.clk_in1(clk_in1),
        .clk_out1(clk_out1),
        .resetn(resetn));
endmodule

(* ORIG_REF_NAME = "design_1_clk_wiz_0_0_clk_wiz" *) 
module Histo1_ADC500MSPS_Controller_0_0_design_1_clk_wiz_0_0_clk_wiz
   (clk_out1,
    resetn,
    clk_in1);
  output clk_out1;
  input resetn;
  input clk_in1;

  wire clk_in1;
  wire clk_in1_design_1_clk_wiz_0_0;
  wire clk_out1;
  wire clk_out1_design_1_clk_wiz_0_0;
  wire clkfbout_buf_design_1_clk_wiz_0_0;
  wire clkfbout_design_1_clk_wiz_0_0;
  wire reset_high;
  wire resetn;
  wire NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT1_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED;
  wire NLW_mmcm_adv_inst_DRDY_UNCONNECTED;
  wire NLW_mmcm_adv_inst_LOCKED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_PSDONE_UNCONNECTED;
  wire [15:0]NLW_mmcm_adv_inst_DO_UNCONNECTED;

  (* box_type = "PRIMITIVE" *) 
  BUFG clkf_buf
       (.I(clkfbout_design_1_clk_wiz_0_0),
        .O(clkfbout_buf_design_1_clk_wiz_0_0));
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  (* box_type = "PRIMITIVE" *) 
  IBUF #(
    .IOSTANDARD("DEFAULT")) 
    clkin1_ibufg
       (.I(clk_in1),
        .O(clk_in1_design_1_clk_wiz_0_0));
  (* box_type = "PRIMITIVE" *) 
  BUFG clkout1_buf
       (.I(clk_out1_design_1_clk_wiz_0_0),
        .O(clk_out1));
  (* box_type = "PRIMITIVE" *) 
  MMCME2_ADV #(
    .BANDWIDTH("OPTIMIZED"),
    .CLKFBOUT_MULT_F(10.125000),
    .CLKFBOUT_PHASE(0.000000),
    .CLKFBOUT_USE_FINE_PS("FALSE"),
    .CLKIN1_PERIOD(10.000000),
    .CLKIN2_PERIOD(0.000000),
    .CLKOUT0_DIVIDE_F(3.375000),
    .CLKOUT0_DUTY_CYCLE(0.500000),
    .CLKOUT0_PHASE(0.000000),
    .CLKOUT0_USE_FINE_PS("FALSE"),
    .CLKOUT1_DIVIDE(1),
    .CLKOUT1_DUTY_CYCLE(0.500000),
    .CLKOUT1_PHASE(0.000000),
    .CLKOUT1_USE_FINE_PS("FALSE"),
    .CLKOUT2_DIVIDE(1),
    .CLKOUT2_DUTY_CYCLE(0.500000),
    .CLKOUT2_PHASE(0.000000),
    .CLKOUT2_USE_FINE_PS("FALSE"),
    .CLKOUT3_DIVIDE(1),
    .CLKOUT3_DUTY_CYCLE(0.500000),
    .CLKOUT3_PHASE(0.000000),
    .CLKOUT3_USE_FINE_PS("FALSE"),
    .CLKOUT4_CASCADE("FALSE"),
    .CLKOUT4_DIVIDE(1),
    .CLKOUT4_DUTY_CYCLE(0.500000),
    .CLKOUT4_PHASE(0.000000),
    .CLKOUT4_USE_FINE_PS("FALSE"),
    .CLKOUT5_DIVIDE(1),
    .CLKOUT5_DUTY_CYCLE(0.500000),
    .CLKOUT5_PHASE(0.000000),
    .CLKOUT5_USE_FINE_PS("FALSE"),
    .CLKOUT6_DIVIDE(1),
    .CLKOUT6_DUTY_CYCLE(0.500000),
    .CLKOUT6_PHASE(0.000000),
    .CLKOUT6_USE_FINE_PS("FALSE"),
    .COMPENSATION("ZHOLD"),
    .DIVCLK_DIVIDE(1),
    .IS_CLKINSEL_INVERTED(1'b0),
    .IS_PSEN_INVERTED(1'b0),
    .IS_PSINCDEC_INVERTED(1'b0),
    .IS_PWRDWN_INVERTED(1'b0),
    .IS_RST_INVERTED(1'b0),
    .REF_JITTER1(0.010000),
    .REF_JITTER2(0.010000),
    .SS_EN("FALSE"),
    .SS_MODE("CENTER_HIGH"),
    .SS_MOD_PERIOD(10000),
    .STARTUP_WAIT("FALSE")) 
    mmcm_adv_inst
       (.CLKFBIN(clkfbout_buf_design_1_clk_wiz_0_0),
        .CLKFBOUT(clkfbout_design_1_clk_wiz_0_0),
        .CLKFBOUTB(NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED),
        .CLKFBSTOPPED(NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED),
        .CLKIN1(clk_in1_design_1_clk_wiz_0_0),
        .CLKIN2(1'b0),
        .CLKINSEL(1'b1),
        .CLKINSTOPPED(NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED),
        .CLKOUT0(clk_out1_design_1_clk_wiz_0_0),
        .CLKOUT0B(NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED),
        .CLKOUT1(NLW_mmcm_adv_inst_CLKOUT1_UNCONNECTED),
        .CLKOUT1B(NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED),
        .CLKOUT2(NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED),
        .CLKOUT2B(NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED),
        .CLKOUT3(NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED),
        .CLKOUT3B(NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED),
        .CLKOUT4(NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED),
        .CLKOUT5(NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED),
        .CLKOUT6(NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED),
        .DADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DCLK(1'b0),
        .DEN(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DO(NLW_mmcm_adv_inst_DO_UNCONNECTED[15:0]),
        .DRDY(NLW_mmcm_adv_inst_DRDY_UNCONNECTED),
        .DWE(1'b0),
        .LOCKED(NLW_mmcm_adv_inst_LOCKED_UNCONNECTED),
        .PSCLK(1'b0),
        .PSDONE(NLW_mmcm_adv_inst_PSDONE_UNCONNECTED),
        .PSEN(1'b0),
        .PSINCDEC(1'b0),
        .PWRDWN(1'b0),
        .RST(reset_high));
  LUT1 #(
    .INIT(2'h1)) 
    mmcm_adv_inst_i_1
       (.I0(resetn),
        .O(reset_high));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_proc_sys_reset_0_0,proc_sys_reset,{}" *) (* ORIG_REF_NAME = "design_1_proc_sys_reset_0_0" *) (* downgradeipidentifiedwarnings = "yes" *) 
(* x_core_info = "proc_sys_reset,Vivado 2017.4" *) 
module Histo1_ADC500MSPS_Controller_0_0_design_1_proc_sys_reset_0_0
   (slowest_sync_clk,
    ext_reset_in,
    aux_reset_in,
    mb_debug_sys_rst,
    dcm_locked,
    mb_reset,
    bus_struct_reset,
    peripheral_reset,
    interconnect_aresetn,
    peripheral_aresetn);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clock CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clock, ASSOCIATED_RESET mb_reset:bus_struct_reset:interconnect_aresetn:peripheral_aresetn:peripheral_reset, FREQ_HZ 100000000, PHASE 0.000" *) input slowest_sync_clk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 ext_reset RST" *) (* x_interface_parameter = "XIL_INTERFACENAME ext_reset, BOARD.ASSOCIATED_PARAM RESET_BOARD_INTERFACE, POLARITY ACTIVE_LOW" *) input ext_reset_in;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 aux_reset RST" *) (* x_interface_parameter = "XIL_INTERFACENAME aux_reset, POLARITY ACTIVE_LOW" *) input aux_reset_in;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 dbg_reset RST" *) (* x_interface_parameter = "XIL_INTERFACENAME dbg_reset, POLARITY ACTIVE_HIGH" *) input mb_debug_sys_rst;
  input dcm_locked;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 mb_rst RST" *) (* x_interface_parameter = "XIL_INTERFACENAME mb_rst, POLARITY ACTIVE_HIGH, TYPE PROCESSOR" *) output mb_reset;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 bus_struct_reset RST" *) (* x_interface_parameter = "XIL_INTERFACENAME bus_struct_reset, POLARITY ACTIVE_HIGH, TYPE INTERCONNECT" *) output [0:0]bus_struct_reset;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 peripheral_high_rst RST" *) (* x_interface_parameter = "XIL_INTERFACENAME peripheral_high_rst, POLARITY ACTIVE_HIGH, TYPE PERIPHERAL" *) output [0:0]peripheral_reset;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 interconnect_low_rst RST" *) (* x_interface_parameter = "XIL_INTERFACENAME interconnect_low_rst, POLARITY ACTIVE_LOW, TYPE INTERCONNECT" *) output [0:0]interconnect_aresetn;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 peripheral_low_rst RST" *) (* x_interface_parameter = "XIL_INTERFACENAME peripheral_low_rst, POLARITY ACTIVE_LOW, TYPE PERIPHERAL" *) output [0:0]peripheral_aresetn;

  wire aux_reset_in;
  wire [0:0]bus_struct_reset;
  wire dcm_locked;
  wire ext_reset_in;
  wire [0:0]interconnect_aresetn;
  wire mb_debug_sys_rst;
  wire mb_reset;
  wire [0:0]peripheral_aresetn;
  wire [0:0]peripheral_reset;
  wire slowest_sync_clk;

  (* C_AUX_RESET_HIGH = "1'b0" *) 
  (* C_AUX_RST_WIDTH = "4" *) 
  (* C_EXT_RESET_HIGH = "1'b0" *) 
  (* C_EXT_RST_WIDTH = "4" *) 
  (* C_FAMILY = "zynq" *) 
  (* C_NUM_BUS_RST = "1" *) 
  (* C_NUM_INTERCONNECT_ARESETN = "1" *) 
  (* C_NUM_PERP_ARESETN = "1" *) 
  (* C_NUM_PERP_RST = "1" *) 
  Histo1_ADC500MSPS_Controller_0_0_proc_sys_reset U0
       (.aux_reset_in(aux_reset_in),
        .bus_struct_reset(bus_struct_reset),
        .dcm_locked(dcm_locked),
        .ext_reset_in(ext_reset_in),
        .interconnect_aresetn(interconnect_aresetn),
        .mb_debug_sys_rst(mb_debug_sys_rst),
        .mb_reset(mb_reset),
        .peripheral_aresetn(peripheral_aresetn),
        .peripheral_reset(peripheral_reset),
        .slowest_sync_clk(slowest_sync_clk));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_util_ds_buf_0_0,util_ds_buf,{}" *) (* ORIG_REF_NAME = "design_1_util_ds_buf_0_0" *) (* downgradeipidentifiedwarnings = "yes" *) 
(* x_core_info = "util_ds_buf,Vivado 2017.4" *) 
module Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_0_0
   (OBUF_IN,
    OBUF_DS_P,
    OBUF_DS_N);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 OBUF_IN CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME OBUF_IN, FREQ_HZ 100000000, PHASE 0.000" *) input [0:0]OBUF_IN;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 OBUF_DS_P CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME OBUF_DS_P, FREQ_HZ 100000000, PHASE 0.000" *) output [0:0]OBUF_DS_P;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 OBUF_DS_N CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME OBUF_DS_N, FREQ_HZ 100000000, PHASE 0.000" *) output [0:0]OBUF_DS_N;

  wire [0:0]OBUF_DS_N;
  wire [0:0]OBUF_DS_P;
  wire [0:0]OBUF_IN;

  Histo1_ADC500MSPS_Controller_0_0_util_ds_buf__parameterized5 U0
       (.OBUF_DS_N(OBUF_DS_N),
        .OBUF_DS_P(OBUF_DS_P),
        .OBUF_IN(OBUF_IN));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_util_ds_buf_0_1,util_ds_buf,{}" *) (* ORIG_REF_NAME = "design_1_util_ds_buf_0_1" *) (* downgradeipidentifiedwarnings = "yes" *) 
(* x_core_info = "util_ds_buf,Vivado 2017.4" *) 
module Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_0_1
   (IBUF_DS_P,
    IBUF_DS_N,
    IBUF_OUT);
  (* x_interface_info = "xilinx.com:interface:diff_clock:1.0 CLK_IN_D CLK_P" *) (* x_interface_parameter = "XIL_INTERFACENAME CLK_IN_D, BOARD.ASSOCIATED_PARAM DIFF_CLK_IN_BOARD_INTERFACE, CAN_DEBUG false, FREQ_HZ 100000000" *) input [0:0]IBUF_DS_P;
  (* x_interface_info = "xilinx.com:interface:diff_clock:1.0 CLK_IN_D CLK_N" *) input [0:0]IBUF_DS_N;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 IBUF_OUT CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME IBUF_OUT, FREQ_HZ 100000000, PHASE 0.000" *) output [0:0]IBUF_OUT;

  wire [0:0]IBUF_DS_N;
  wire [0:0]IBUF_DS_P;
  wire [0:0]IBUF_OUT;

  Histo1_ADC500MSPS_Controller_0_0_util_ds_buf__parameterized1_1 U0
       (.IBUF_DS_N(IBUF_DS_N),
        .IBUF_DS_P(IBUF_DS_P),
        .IBUF_OUT(IBUF_OUT));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_util_ds_buf_1_0,util_ds_buf,{}" *) (* ORIG_REF_NAME = "design_1_util_ds_buf_1_0" *) (* downgradeipidentifiedwarnings = "yes" *) 
(* x_core_info = "util_ds_buf,Vivado 2017.4" *) 
module Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_1_0
   (IBUF_DS_P,
    IBUF_DS_N,
    IBUF_OUT);
  (* x_interface_info = "xilinx.com:interface:diff_clock:1.0 CLK_IN_D CLK_P" *) (* x_interface_parameter = "XIL_INTERFACENAME CLK_IN_D, BOARD.ASSOCIATED_PARAM DIFF_CLK_IN_BOARD_INTERFACE, CAN_DEBUG false, FREQ_HZ 100000000" *) input [0:0]IBUF_DS_P;
  (* x_interface_info = "xilinx.com:interface:diff_clock:1.0 CLK_IN_D CLK_N" *) input [0:0]IBUF_DS_N;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 IBUF_OUT CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME IBUF_OUT, FREQ_HZ 100000000, PHASE 0.000" *) output [0:0]IBUF_OUT;

  wire [0:0]IBUF_DS_N;
  wire [0:0]IBUF_DS_P;
  wire [0:0]IBUF_OUT;

  Histo1_ADC500MSPS_Controller_0_0_util_ds_buf__parameterized1 U0
       (.IBUF_DS_N(IBUF_DS_N),
        .IBUF_DS_P(IBUF_DS_P),
        .IBUF_OUT(IBUF_OUT));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_util_ds_buf_2_0,util_ds_buf,{}" *) (* ORIG_REF_NAME = "design_1_util_ds_buf_2_0" *) (* downgradeipidentifiedwarnings = "yes" *) 
(* x_core_info = "util_ds_buf,Vivado 2017.4" *) 
module Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_2_0
   (BUFG_I,
    BUFG_O);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 BUFG_I CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BUFG_I, FREQ_HZ 100000000, PHASE 0.000" *) input [0:0]BUFG_I;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 BUFG_O CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BUFG_O, FREQ_HZ 100000000, PHASE 0.000" *) output [0:0]BUFG_O;

  wire [0:0]BUFG_I;
  wire [0:0]BUFG_O;

  Histo1_ADC500MSPS_Controller_0_0_util_ds_buf U0
       (.BUFG_I(BUFG_I),
        .BUFG_O(BUFG_O));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_util_ds_buf_3_0,util_ds_buf,{}" *) (* ORIG_REF_NAME = "design_1_util_ds_buf_3_0" *) (* downgradeipidentifiedwarnings = "yes" *) 
(* x_core_info = "util_ds_buf,Vivado 2017.4" *) 
module Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_3_0
   (IBUF_DS_P,
    IBUF_DS_N,
    IBUF_OUT);
  (* x_interface_info = "xilinx.com:interface:diff_clock:1.0 CLK_IN_D CLK_P" *) (* x_interface_parameter = "XIL_INTERFACENAME CLK_IN_D, BOARD.ASSOCIATED_PARAM DIFF_CLK_IN_BOARD_INTERFACE, CAN_DEBUG false, FREQ_HZ 100000000" *) input [15:0]IBUF_DS_P;
  (* x_interface_info = "xilinx.com:interface:diff_clock:1.0 CLK_IN_D CLK_N" *) input [15:0]IBUF_DS_N;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 IBUF_OUT CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME IBUF_OUT, FREQ_HZ 100000000, PHASE 0.000" *) output [15:0]IBUF_OUT;

  wire [15:0]IBUF_DS_N;
  wire [15:0]IBUF_DS_P;
  wire [15:0]IBUF_OUT;

  Histo1_ADC500MSPS_Controller_0_0_util_ds_buf__parameterized3 U0
       (.IBUF_DS_N(IBUF_DS_N),
        .IBUF_DS_P(IBUF_DS_P),
        .IBUF_OUT(IBUF_OUT));
endmodule

(* ORIG_REF_NAME = "lpf" *) 
module Histo1_ADC500MSPS_Controller_0_0_lpf
   (lpf_int,
    slowest_sync_clk,
    dcm_locked,
    aux_reset_in,
    mb_debug_sys_rst,
    ext_reset_in);
  output lpf_int;
  input slowest_sync_clk;
  input dcm_locked;
  input aux_reset_in;
  input mb_debug_sys_rst;
  input ext_reset_in;

  wire \ACTIVE_LOW_AUX.ACT_LO_AUX_n_0 ;
  wire \ACTIVE_LOW_EXT.ACT_LO_EXT_n_0 ;
  wire Q;
  wire [0:0]asr_lpf;
  wire aux_reset_in;
  wire dcm_locked;
  wire ext_reset_in;
  wire lpf_asr;
  wire lpf_exr;
  wire lpf_int;
  wire lpf_int0__0;
  wire mb_debug_sys_rst;
  wire p_1_in;
  wire p_2_in;
  wire p_3_in1_in;
  wire [3:0]p_3_out;
  wire slowest_sync_clk;

  Histo1_ADC500MSPS_Controller_0_0_cdc_sync \ACTIVE_LOW_AUX.ACT_LO_AUX 
       (.asr_lpf(asr_lpf),
        .aux_reset_in(aux_reset_in),
        .lpf_asr(lpf_asr),
        .lpf_asr_reg(\ACTIVE_LOW_AUX.ACT_LO_AUX_n_0 ),
        .p_1_in(p_1_in),
        .p_2_in(p_2_in),
        .scndry_out(p_3_in1_in),
        .slowest_sync_clk(slowest_sync_clk));
  Histo1_ADC500MSPS_Controller_0_0_cdc_sync_0 \ACTIVE_LOW_EXT.ACT_LO_EXT 
       (.ext_reset_in(ext_reset_in),
        .lpf_exr(lpf_exr),
        .lpf_exr_reg(\ACTIVE_LOW_EXT.ACT_LO_EXT_n_0 ),
        .mb_debug_sys_rst(mb_debug_sys_rst),
        .p_3_out(p_3_out[2:0]),
        .scndry_out(p_3_out[3]),
        .slowest_sync_clk(slowest_sync_clk));
  FDRE #(
    .INIT(1'b0)) 
    \AUX_LPF[1].asr_lpf_reg[1] 
       (.C(slowest_sync_clk),
        .CE(1'b1),
        .D(p_3_in1_in),
        .Q(p_2_in),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \AUX_LPF[2].asr_lpf_reg[2] 
       (.C(slowest_sync_clk),
        .CE(1'b1),
        .D(p_2_in),
        .Q(p_1_in),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \AUX_LPF[3].asr_lpf_reg[3] 
       (.C(slowest_sync_clk),
        .CE(1'b1),
        .D(p_1_in),
        .Q(asr_lpf),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \EXT_LPF[1].exr_lpf_reg[1] 
       (.C(slowest_sync_clk),
        .CE(1'b1),
        .D(p_3_out[3]),
        .Q(p_3_out[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \EXT_LPF[2].exr_lpf_reg[2] 
       (.C(slowest_sync_clk),
        .CE(1'b1),
        .D(p_3_out[2]),
        .Q(p_3_out[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \EXT_LPF[3].exr_lpf_reg[3] 
       (.C(slowest_sync_clk),
        .CE(1'b1),
        .D(p_3_out[1]),
        .Q(p_3_out[0]),
        .R(1'b0));
  (* XILINX_LEGACY_PRIM = "SRL16" *) 
  (* box_type = "PRIMITIVE" *) 
  (* srl_name = "\U0/reset_generator /U0/\EXT_LPF/POR_SRL_I " *) 
  SRL16E #(
    .INIT(16'hFFFF)) 
    POR_SRL_I
       (.A0(1'b1),
        .A1(1'b1),
        .A2(1'b1),
        .A3(1'b1),
        .CE(1'b1),
        .CLK(slowest_sync_clk),
        .D(1'b0),
        .Q(Q));
  FDRE #(
    .INIT(1'b0)) 
    lpf_asr_reg
       (.C(slowest_sync_clk),
        .CE(1'b1),
        .D(\ACTIVE_LOW_AUX.ACT_LO_AUX_n_0 ),
        .Q(lpf_asr),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    lpf_exr_reg
       (.C(slowest_sync_clk),
        .CE(1'b1),
        .D(\ACTIVE_LOW_EXT.ACT_LO_EXT_n_0 ),
        .Q(lpf_exr),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hFFFD)) 
    lpf_int0
       (.I0(dcm_locked),
        .I1(Q),
        .I2(lpf_exr),
        .I3(lpf_asr),
        .O(lpf_int0__0));
  FDRE #(
    .INIT(1'b0)) 
    lpf_int_reg
       (.C(slowest_sync_clk),
        .CE(1'b1),
        .D(lpf_int0__0),
        .Q(lpf_int),
        .R(1'b0));
endmodule

(* C_AUX_RESET_HIGH = "1'b0" *) (* C_AUX_RST_WIDTH = "4" *) (* C_EXT_RESET_HIGH = "1'b0" *) 
(* C_EXT_RST_WIDTH = "4" *) (* C_FAMILY = "zynq" *) (* C_NUM_BUS_RST = "1" *) 
(* C_NUM_INTERCONNECT_ARESETN = "1" *) (* C_NUM_PERP_ARESETN = "1" *) (* C_NUM_PERP_RST = "1" *) 
(* ORIG_REF_NAME = "proc_sys_reset" *) 
module Histo1_ADC500MSPS_Controller_0_0_proc_sys_reset
   (slowest_sync_clk,
    ext_reset_in,
    aux_reset_in,
    mb_debug_sys_rst,
    dcm_locked,
    mb_reset,
    bus_struct_reset,
    peripheral_reset,
    interconnect_aresetn,
    peripheral_aresetn);
  input slowest_sync_clk;
  input ext_reset_in;
  input aux_reset_in;
  input mb_debug_sys_rst;
  input dcm_locked;
  output mb_reset;
  (* equivalent_register_removal = "no" *) output [0:0]bus_struct_reset;
  (* equivalent_register_removal = "no" *) output [0:0]peripheral_reset;
  (* equivalent_register_removal = "no" *) output [0:0]interconnect_aresetn;
  (* equivalent_register_removal = "no" *) output [0:0]peripheral_aresetn;

  wire Bsr_out;
  wire MB_out;
  wire Pr_out;
  wire SEQ_n_3;
  wire SEQ_n_4;
  wire aux_reset_in;
  wire [0:0]bus_struct_reset;
  wire dcm_locked;
  wire ext_reset_in;
  wire [0:0]interconnect_aresetn;
  wire lpf_int;
  wire mb_debug_sys_rst;
  wire mb_reset;
  wire [0:0]peripheral_aresetn;
  wire [0:0]peripheral_reset;
  wire slowest_sync_clk;

  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b0),
    .IS_D_INVERTED(1'b0),
    .IS_R_INVERTED(1'b0)) 
    \ACTIVE_LOW_BSR_OUT_DFF[0].FDRE_BSR_N 
       (.C(slowest_sync_clk),
        .CE(1'b1),
        .D(SEQ_n_3),
        .Q(interconnect_aresetn),
        .R(1'b0));
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b0),
    .IS_D_INVERTED(1'b0),
    .IS_R_INVERTED(1'b0)) 
    \ACTIVE_LOW_PR_OUT_DFF[0].FDRE_PER_N 
       (.C(slowest_sync_clk),
        .CE(1'b1),
        .D(SEQ_n_4),
        .Q(peripheral_aresetn),
        .R(1'b0));
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b1),
    .IS_C_INVERTED(1'b0),
    .IS_D_INVERTED(1'b0),
    .IS_R_INVERTED(1'b0)) 
    \BSR_OUT_DFF[0].FDRE_BSR 
       (.C(slowest_sync_clk),
        .CE(1'b1),
        .D(Bsr_out),
        .Q(bus_struct_reset),
        .R(1'b0));
  Histo1_ADC500MSPS_Controller_0_0_lpf EXT_LPF
       (.aux_reset_in(aux_reset_in),
        .dcm_locked(dcm_locked),
        .ext_reset_in(ext_reset_in),
        .lpf_int(lpf_int),
        .mb_debug_sys_rst(mb_debug_sys_rst),
        .slowest_sync_clk(slowest_sync_clk));
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b1),
    .IS_C_INVERTED(1'b0),
    .IS_D_INVERTED(1'b0),
    .IS_R_INVERTED(1'b0)) 
    FDRE_inst
       (.C(slowest_sync_clk),
        .CE(1'b1),
        .D(MB_out),
        .Q(mb_reset),
        .R(1'b0));
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b1),
    .IS_C_INVERTED(1'b0),
    .IS_D_INVERTED(1'b0),
    .IS_R_INVERTED(1'b0)) 
    \PR_OUT_DFF[0].FDRE_PER 
       (.C(slowest_sync_clk),
        .CE(1'b1),
        .D(Pr_out),
        .Q(peripheral_reset),
        .R(1'b0));
  Histo1_ADC500MSPS_Controller_0_0_sequence_psr SEQ
       (.\ACTIVE_LOW_BSR_OUT_DFF[0].FDRE_BSR_N (SEQ_n_3),
        .\ACTIVE_LOW_PR_OUT_DFF[0].FDRE_PER_N (SEQ_n_4),
        .Bsr_out(Bsr_out),
        .MB_out(MB_out),
        .Pr_out(Pr_out),
        .lpf_int(lpf_int),
        .slowest_sync_clk(slowest_sync_clk));
endmodule

(* ORIG_REF_NAME = "sequence_psr" *) 
module Histo1_ADC500MSPS_Controller_0_0_sequence_psr
   (MB_out,
    Bsr_out,
    Pr_out,
    \ACTIVE_LOW_BSR_OUT_DFF[0].FDRE_BSR_N ,
    \ACTIVE_LOW_PR_OUT_DFF[0].FDRE_PER_N ,
    lpf_int,
    slowest_sync_clk);
  output MB_out;
  output Bsr_out;
  output Pr_out;
  output \ACTIVE_LOW_BSR_OUT_DFF[0].FDRE_BSR_N ;
  output \ACTIVE_LOW_PR_OUT_DFF[0].FDRE_PER_N ;
  input lpf_int;
  input slowest_sync_clk;

  wire \ACTIVE_LOW_BSR_OUT_DFF[0].FDRE_BSR_N ;
  wire \ACTIVE_LOW_PR_OUT_DFF[0].FDRE_PER_N ;
  wire Bsr_out;
  wire Core_i_1_n_0;
  wire MB_out;
  wire Pr_out;
  wire \bsr_dec_reg_n_0_[0] ;
  wire \bsr_dec_reg_n_0_[2] ;
  wire bsr_i_1_n_0;
  wire \core_dec[0]_i_1_n_0 ;
  wire \core_dec[2]_i_1_n_0 ;
  wire \core_dec_reg_n_0_[0] ;
  wire \core_dec_reg_n_0_[1] ;
  wire from_sys_i_1_n_0;
  wire lpf_int;
  wire p_0_in;
  wire [2:0]p_3_out;
  wire [2:0]p_5_out;
  wire pr_dec0__0;
  wire \pr_dec_reg_n_0_[0] ;
  wire \pr_dec_reg_n_0_[2] ;
  wire pr_i_1_n_0;
  wire seq_clr;
  wire [5:0]seq_cnt;
  wire seq_cnt_en;
  wire slowest_sync_clk;

  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \ACTIVE_LOW_BSR_OUT_DFF[0].FDRE_BSR_N_i_1 
       (.I0(Bsr_out),
        .O(\ACTIVE_LOW_BSR_OUT_DFF[0].FDRE_BSR_N ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \ACTIVE_LOW_PR_OUT_DFF[0].FDRE_PER_N_i_1 
       (.I0(Pr_out),
        .O(\ACTIVE_LOW_PR_OUT_DFF[0].FDRE_PER_N ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT2 #(
    .INIT(4'h2)) 
    Core_i_1
       (.I0(MB_out),
        .I1(p_0_in),
        .O(Core_i_1_n_0));
  FDSE #(
    .INIT(1'b1)) 
    Core_reg
       (.C(slowest_sync_clk),
        .CE(1'b1),
        .D(Core_i_1_n_0),
        .Q(MB_out),
        .S(lpf_int));
  Histo1_ADC500MSPS_Controller_0_0_upcnt_n SEQ_COUNTER
       (.Q(seq_cnt),
        .seq_clr(seq_clr),
        .seq_cnt_en(seq_cnt_en),
        .slowest_sync_clk(slowest_sync_clk));
  LUT4 #(
    .INIT(16'h0804)) 
    \bsr_dec[0]_i_1 
       (.I0(seq_cnt_en),
        .I1(seq_cnt[3]),
        .I2(seq_cnt[5]),
        .I3(seq_cnt[4]),
        .O(p_5_out[0]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \bsr_dec[2]_i_1 
       (.I0(\core_dec_reg_n_0_[1] ),
        .I1(\bsr_dec_reg_n_0_[0] ),
        .O(p_5_out[2]));
  FDRE #(
    .INIT(1'b0)) 
    \bsr_dec_reg[0] 
       (.C(slowest_sync_clk),
        .CE(1'b1),
        .D(p_5_out[0]),
        .Q(\bsr_dec_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \bsr_dec_reg[2] 
       (.C(slowest_sync_clk),
        .CE(1'b1),
        .D(p_5_out[2]),
        .Q(\bsr_dec_reg_n_0_[2] ),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'h2)) 
    bsr_i_1
       (.I0(Bsr_out),
        .I1(\bsr_dec_reg_n_0_[2] ),
        .O(bsr_i_1_n_0));
  FDSE #(
    .INIT(1'b1)) 
    bsr_reg
       (.C(slowest_sync_clk),
        .CE(1'b1),
        .D(bsr_i_1_n_0),
        .Q(Bsr_out),
        .S(lpf_int));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'h8040)) 
    \core_dec[0]_i_1 
       (.I0(seq_cnt[4]),
        .I1(seq_cnt[3]),
        .I2(seq_cnt[5]),
        .I3(seq_cnt_en),
        .O(\core_dec[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \core_dec[2]_i_1 
       (.I0(\core_dec_reg_n_0_[1] ),
        .I1(\core_dec_reg_n_0_[0] ),
        .O(\core_dec[2]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \core_dec_reg[0] 
       (.C(slowest_sync_clk),
        .CE(1'b1),
        .D(\core_dec[0]_i_1_n_0 ),
        .Q(\core_dec_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \core_dec_reg[1] 
       (.C(slowest_sync_clk),
        .CE(1'b1),
        .D(pr_dec0__0),
        .Q(\core_dec_reg_n_0_[1] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \core_dec_reg[2] 
       (.C(slowest_sync_clk),
        .CE(1'b1),
        .D(\core_dec[2]_i_1_n_0 ),
        .Q(p_0_in),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT2 #(
    .INIT(4'h8)) 
    from_sys_i_1
       (.I0(MB_out),
        .I1(seq_cnt_en),
        .O(from_sys_i_1_n_0));
  FDSE #(
    .INIT(1'b0)) 
    from_sys_reg
       (.C(slowest_sync_clk),
        .CE(1'b1),
        .D(from_sys_i_1_n_0),
        .Q(seq_cnt_en),
        .S(lpf_int));
  LUT4 #(
    .INIT(16'h0210)) 
    pr_dec0
       (.I0(seq_cnt[0]),
        .I1(seq_cnt[1]),
        .I2(seq_cnt[2]),
        .I3(seq_cnt_en),
        .O(pr_dec0__0));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'h1080)) 
    \pr_dec[0]_i_1 
       (.I0(seq_cnt_en),
        .I1(seq_cnt[5]),
        .I2(seq_cnt[3]),
        .I3(seq_cnt[4]),
        .O(p_3_out[0]));
  LUT2 #(
    .INIT(4'h8)) 
    \pr_dec[2]_i_1 
       (.I0(\core_dec_reg_n_0_[1] ),
        .I1(\pr_dec_reg_n_0_[0] ),
        .O(p_3_out[2]));
  FDRE #(
    .INIT(1'b0)) 
    \pr_dec_reg[0] 
       (.C(slowest_sync_clk),
        .CE(1'b1),
        .D(p_3_out[0]),
        .Q(\pr_dec_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \pr_dec_reg[2] 
       (.C(slowest_sync_clk),
        .CE(1'b1),
        .D(p_3_out[2]),
        .Q(\pr_dec_reg_n_0_[2] ),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'h2)) 
    pr_i_1
       (.I0(Pr_out),
        .I1(\pr_dec_reg_n_0_[2] ),
        .O(pr_i_1_n_0));
  FDSE #(
    .INIT(1'b1)) 
    pr_reg
       (.C(slowest_sync_clk),
        .CE(1'b1),
        .D(pr_i_1_n_0),
        .Q(Pr_out),
        .S(lpf_int));
  FDRE #(
    .INIT(1'b0)) 
    seq_clr_reg
       (.C(slowest_sync_clk),
        .CE(1'b1),
        .D(1'b1),
        .Q(seq_clr),
        .R(lpf_int));
endmodule

(* ORIG_REF_NAME = "upcnt_n" *) 
module Histo1_ADC500MSPS_Controller_0_0_upcnt_n
   (Q,
    seq_clr,
    seq_cnt_en,
    slowest_sync_clk);
  output [5:0]Q;
  input seq_clr;
  input seq_cnt_en;
  input slowest_sync_clk;

  wire [5:0]Q;
  wire clear;
  wire [5:0]q_int0;
  wire seq_clr;
  wire seq_cnt_en;
  wire slowest_sync_clk;

  LUT1 #(
    .INIT(2'h1)) 
    \q_int[0]_i_1 
       (.I0(Q[0]),
        .O(q_int0[0]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \q_int[1]_i_1 
       (.I0(Q[0]),
        .I1(Q[1]),
        .O(q_int0[1]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \q_int[2]_i_1 
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(Q[2]),
        .O(q_int0[2]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \q_int[3]_i_1 
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(Q[2]),
        .I3(Q[3]),
        .O(q_int0[3]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \q_int[4]_i_1 
       (.I0(Q[2]),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(Q[3]),
        .I4(Q[4]),
        .O(q_int0[4]));
  LUT1 #(
    .INIT(2'h1)) 
    \q_int[5]_i_1 
       (.I0(seq_clr),
        .O(clear));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \q_int[5]_i_2 
       (.I0(Q[3]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(Q[2]),
        .I4(Q[4]),
        .I5(Q[5]),
        .O(q_int0[5]));
  FDRE #(
    .INIT(1'b1)) 
    \q_int_reg[0] 
       (.C(slowest_sync_clk),
        .CE(seq_cnt_en),
        .D(q_int0[0]),
        .Q(Q[0]),
        .R(clear));
  FDRE #(
    .INIT(1'b1)) 
    \q_int_reg[1] 
       (.C(slowest_sync_clk),
        .CE(seq_cnt_en),
        .D(q_int0[1]),
        .Q(Q[1]),
        .R(clear));
  FDRE #(
    .INIT(1'b1)) 
    \q_int_reg[2] 
       (.C(slowest_sync_clk),
        .CE(seq_cnt_en),
        .D(q_int0[2]),
        .Q(Q[2]),
        .R(clear));
  FDRE #(
    .INIT(1'b1)) 
    \q_int_reg[3] 
       (.C(slowest_sync_clk),
        .CE(seq_cnt_en),
        .D(q_int0[3]),
        .Q(Q[3]),
        .R(clear));
  FDRE #(
    .INIT(1'b1)) 
    \q_int_reg[4] 
       (.C(slowest_sync_clk),
        .CE(seq_cnt_en),
        .D(q_int0[4]),
        .Q(Q[4]),
        .R(clear));
  FDRE #(
    .INIT(1'b1)) 
    \q_int_reg[5] 
       (.C(slowest_sync_clk),
        .CE(seq_cnt_en),
        .D(q_int0[5]),
        .Q(Q[5]),
        .R(clear));
endmodule

(* ORIG_REF_NAME = "util_ds_buf" *) 
module Histo1_ADC500MSPS_Controller_0_0_util_ds_buf
   (BUFG_O,
    BUFG_I);
  output [0:0]BUFG_O;
  input [0:0]BUFG_I;

  wire [0:0]BUFG_I;
  wire [0:0]BUFG_O;

  (* box_type = "PRIMITIVE" *) 
  BUFG \USE_BUFG.GEN_BUFG[0].BUFG_U 
       (.I(BUFG_I),
        .O(BUFG_O));
endmodule

(* ORIG_REF_NAME = "util_ds_buf" *) 
module Histo1_ADC500MSPS_Controller_0_0_util_ds_buf__parameterized1
   (IBUF_OUT,
    IBUF_DS_P,
    IBUF_DS_N);
  output [0:0]IBUF_OUT;
  input [0:0]IBUF_DS_P;
  input [0:0]IBUF_DS_N;

  wire [0:0]IBUF_DS_N;
  wire [0:0]IBUF_DS_P;
  wire [0:0]IBUF_OUT;

  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  (* box_type = "PRIMITIVE" *) 
  IBUFDS #(
    .IOSTANDARD("DEFAULT")) 
    \USE_IBUFDS.GEN_IBUFDS[0].IBUFDS_I 
       (.I(IBUF_DS_P),
        .IB(IBUF_DS_N),
        .O(IBUF_OUT));
endmodule

(* ORIG_REF_NAME = "util_ds_buf" *) 
module Histo1_ADC500MSPS_Controller_0_0_util_ds_buf__parameterized1_1
   (IBUF_OUT,
    IBUF_DS_P,
    IBUF_DS_N);
  output [0:0]IBUF_OUT;
  input [0:0]IBUF_DS_P;
  input [0:0]IBUF_DS_N;

  wire [0:0]IBUF_DS_N;
  wire [0:0]IBUF_DS_P;
  wire [0:0]IBUF_OUT;

  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  (* box_type = "PRIMITIVE" *) 
  IBUFDS #(
    .IOSTANDARD("DEFAULT")) 
    \USE_IBUFDS.GEN_IBUFDS[0].IBUFDS_I 
       (.I(IBUF_DS_P),
        .IB(IBUF_DS_N),
        .O(IBUF_OUT));
endmodule

(* ORIG_REF_NAME = "util_ds_buf" *) 
module Histo1_ADC500MSPS_Controller_0_0_util_ds_buf__parameterized3
   (IBUF_OUT,
    IBUF_DS_P,
    IBUF_DS_N);
  output [15:0]IBUF_OUT;
  input [15:0]IBUF_DS_P;
  input [15:0]IBUF_DS_N;

  wire [15:0]IBUF_DS_N;
  wire [15:0]IBUF_DS_P;
  wire [15:0]IBUF_OUT;

  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  (* box_type = "PRIMITIVE" *) 
  IBUFDS #(
    .IOSTANDARD("DEFAULT")) 
    \USE_IBUFDS.GEN_IBUFDS[0].IBUFDS_I 
       (.I(IBUF_DS_P[0]),
        .IB(IBUF_DS_N[0]),
        .O(IBUF_OUT[0]));
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  (* box_type = "PRIMITIVE" *) 
  IBUFDS #(
    .IOSTANDARD("DEFAULT")) 
    \USE_IBUFDS.GEN_IBUFDS[10].IBUFDS_I 
       (.I(IBUF_DS_P[10]),
        .IB(IBUF_DS_N[10]),
        .O(IBUF_OUT[10]));
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  (* box_type = "PRIMITIVE" *) 
  IBUFDS #(
    .IOSTANDARD("DEFAULT")) 
    \USE_IBUFDS.GEN_IBUFDS[11].IBUFDS_I 
       (.I(IBUF_DS_P[11]),
        .IB(IBUF_DS_N[11]),
        .O(IBUF_OUT[11]));
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  (* box_type = "PRIMITIVE" *) 
  IBUFDS #(
    .IOSTANDARD("DEFAULT")) 
    \USE_IBUFDS.GEN_IBUFDS[12].IBUFDS_I 
       (.I(IBUF_DS_P[12]),
        .IB(IBUF_DS_N[12]),
        .O(IBUF_OUT[12]));
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  (* box_type = "PRIMITIVE" *) 
  IBUFDS #(
    .IOSTANDARD("DEFAULT")) 
    \USE_IBUFDS.GEN_IBUFDS[13].IBUFDS_I 
       (.I(IBUF_DS_P[13]),
        .IB(IBUF_DS_N[13]),
        .O(IBUF_OUT[13]));
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  (* box_type = "PRIMITIVE" *) 
  IBUFDS #(
    .IOSTANDARD("DEFAULT")) 
    \USE_IBUFDS.GEN_IBUFDS[14].IBUFDS_I 
       (.I(IBUF_DS_P[14]),
        .IB(IBUF_DS_N[14]),
        .O(IBUF_OUT[14]));
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  (* box_type = "PRIMITIVE" *) 
  IBUFDS #(
    .IOSTANDARD("DEFAULT")) 
    \USE_IBUFDS.GEN_IBUFDS[15].IBUFDS_I 
       (.I(IBUF_DS_P[15]),
        .IB(IBUF_DS_N[15]),
        .O(IBUF_OUT[15]));
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  (* box_type = "PRIMITIVE" *) 
  IBUFDS #(
    .IOSTANDARD("DEFAULT")) 
    \USE_IBUFDS.GEN_IBUFDS[1].IBUFDS_I 
       (.I(IBUF_DS_P[1]),
        .IB(IBUF_DS_N[1]),
        .O(IBUF_OUT[1]));
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  (* box_type = "PRIMITIVE" *) 
  IBUFDS #(
    .IOSTANDARD("DEFAULT")) 
    \USE_IBUFDS.GEN_IBUFDS[2].IBUFDS_I 
       (.I(IBUF_DS_P[2]),
        .IB(IBUF_DS_N[2]),
        .O(IBUF_OUT[2]));
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  (* box_type = "PRIMITIVE" *) 
  IBUFDS #(
    .IOSTANDARD("DEFAULT")) 
    \USE_IBUFDS.GEN_IBUFDS[3].IBUFDS_I 
       (.I(IBUF_DS_P[3]),
        .IB(IBUF_DS_N[3]),
        .O(IBUF_OUT[3]));
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  (* box_type = "PRIMITIVE" *) 
  IBUFDS #(
    .IOSTANDARD("DEFAULT")) 
    \USE_IBUFDS.GEN_IBUFDS[4].IBUFDS_I 
       (.I(IBUF_DS_P[4]),
        .IB(IBUF_DS_N[4]),
        .O(IBUF_OUT[4]));
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  (* box_type = "PRIMITIVE" *) 
  IBUFDS #(
    .IOSTANDARD("DEFAULT")) 
    \USE_IBUFDS.GEN_IBUFDS[5].IBUFDS_I 
       (.I(IBUF_DS_P[5]),
        .IB(IBUF_DS_N[5]),
        .O(IBUF_OUT[5]));
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  (* box_type = "PRIMITIVE" *) 
  IBUFDS #(
    .IOSTANDARD("DEFAULT")) 
    \USE_IBUFDS.GEN_IBUFDS[6].IBUFDS_I 
       (.I(IBUF_DS_P[6]),
        .IB(IBUF_DS_N[6]),
        .O(IBUF_OUT[6]));
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  (* box_type = "PRIMITIVE" *) 
  IBUFDS #(
    .IOSTANDARD("DEFAULT")) 
    \USE_IBUFDS.GEN_IBUFDS[7].IBUFDS_I 
       (.I(IBUF_DS_P[7]),
        .IB(IBUF_DS_N[7]),
        .O(IBUF_OUT[7]));
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  (* box_type = "PRIMITIVE" *) 
  IBUFDS #(
    .IOSTANDARD("DEFAULT")) 
    \USE_IBUFDS.GEN_IBUFDS[8].IBUFDS_I 
       (.I(IBUF_DS_P[8]),
        .IB(IBUF_DS_N[8]),
        .O(IBUF_OUT[8]));
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  (* box_type = "PRIMITIVE" *) 
  IBUFDS #(
    .IOSTANDARD("DEFAULT")) 
    \USE_IBUFDS.GEN_IBUFDS[9].IBUFDS_I 
       (.I(IBUF_DS_P[9]),
        .IB(IBUF_DS_N[9]),
        .O(IBUF_OUT[9]));
endmodule

(* ORIG_REF_NAME = "util_ds_buf" *) 
module Histo1_ADC500MSPS_Controller_0_0_util_ds_buf__parameterized5
   (OBUF_DS_P,
    OBUF_DS_N,
    OBUF_IN);
  output [0:0]OBUF_DS_P;
  output [0:0]OBUF_DS_N;
  input [0:0]OBUF_IN;

  wire [0:0]OBUF_DS_N;
  wire [0:0]OBUF_DS_P;
  wire [0:0]OBUF_IN;

  (* CAPACITANCE = "DONT_CARE" *) 
  (* XILINX_LEGACY_PRIM = "OBUFDS" *) 
  (* box_type = "PRIMITIVE" *) 
  OBUFDS #(
    .IOSTANDARD("DEFAULT")) 
    \USE_OBUFDS.GEN_OBUFDS[0].OBUFDS_I 
       (.I(OBUF_IN),
        .O(OBUF_DS_P),
        .OB(OBUF_DS_N));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
