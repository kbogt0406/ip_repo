-- Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2017.4 (win64) Build 2086221 Fri Dec 15 20:55:39 MST 2017
-- Date        : Wed Feb  7 13:10:30 2018
-- Host        : HP6-MLAB-9 running 64-bit Service Pack 1  (build 7601)
-- Command     : write_vhdl -force -mode synth_stub
--               c:/Users/infolab/Desktop/my_verilog/histo_w_comblock/project_1/project_1.srcs/sources_1/bd/Histo1/ip/Histo1_reg_block_0_0/Histo1_reg_block_0_0_stub.vhdl
-- Design      : Histo1_reg_block_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Histo1_reg_block_0_0 is
  Port ( 
    in_reg0 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    in_reg1 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    in_reg2 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    in_reg3 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    in_reg4 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    in_reg5 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    in_reg6 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    in_reg7 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    in_reg8 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    in_reg9 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    out_reg0 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    out_reg1 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    out_reg2 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    out_reg3 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    out_reg4 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    out_reg5 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    out_reg6 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    out_reg7 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    out_reg8 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    out_reg9 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 6 downto 0 );
    s00_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_awready : out STD_LOGIC;
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_wready : out STD_LOGIC;
    s00_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 6 downto 0 );
    s00_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_arready : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_rvalid : out STD_LOGIC;
    s00_axi_rready : in STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC
  );

end Histo1_reg_block_0_0;

architecture stub of Histo1_reg_block_0_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "in_reg0[31:0],in_reg1[31:0],in_reg2[31:0],in_reg3[31:0],in_reg4[31:0],in_reg5[31:0],in_reg6[31:0],in_reg7[31:0],in_reg8[31:0],in_reg9[31:0],out_reg0[31:0],out_reg1[31:0],out_reg2[31:0],out_reg3[31:0],out_reg4[31:0],out_reg5[31:0],out_reg6[31:0],out_reg7[31:0],out_reg8[31:0],out_reg9[31:0],s00_axi_awaddr[6:0],s00_axi_awprot[2:0],s00_axi_awvalid,s00_axi_awready,s00_axi_wdata[31:0],s00_axi_wstrb[3:0],s00_axi_wvalid,s00_axi_wready,s00_axi_bresp[1:0],s00_axi_bvalid,s00_axi_bready,s00_axi_araddr[6:0],s00_axi_arprot[2:0],s00_axi_arvalid,s00_axi_arready,s00_axi_rdata[31:0],s00_axi_rresp[1:0],s00_axi_rvalid,s00_axi_rready,s00_axi_aclk,s00_axi_aresetn";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "reg_block_v1_0,Vivado 2017.4";
begin
end;
