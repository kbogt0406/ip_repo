#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"
#include "xil_io.h"

int main()
{
    int i, val;
    init_platform();

    print("Testing Regs inverted interconnection\n");

    for (i=0; i < 10; i++) {
        Xil_Out32(XPAR_COMBLOCK_0_AXIL_REGS_BASEADDR+i*4,i);
    }

    for (i=0; i < 10; i++) {
        val = Xil_In32(XPAR_COMBLOCK_0_AXIL_REGS_BASEADDR+i*4);
        if ( val == 9-i )
           xil_printf("SUCCESS -> Reg %d = %d\n",i,val);
        else
           xil_printf("ERROR:  -> Reg %d = %d\n",i,val);
    }

    print("Testing RAM interconnection\n");

    for (i=0; i < 400; i++) {
        Xil_Out32(XPAR_COMBLOCK_0_AXIF_DRAM_BASEADDR+i*4,i);
    }

    for (i=0; i < 400; i++) {
        val = Xil_In32(XPAR_COMBLOCK_0_AXIF_DRAM_BASEADDR+i*4);
        if ( val == i )
           xil_printf("SUCCESS -> Mem %d = %d\n",i,val);
        else
           xil_printf("ERROR:  -> Mem %d = %d\n",i,val);
    }


    print("Testing FIFO interconnection\n");

    for (i=0; i <15; i++) {
        xil_printf("FIFO %d = %d\n",i,Xil_In32(XPAR_COMBLOCK_0_AXIF_FIFO_BASEADDR));
    }

    cleanup_platform();
    return 0;
}
